# Lama Studio

Lama Studio is a software to quickly design knitting charts. First, you choose a set of color, and the symbols you want to use. Then basic tools let you draw some patterns you want to use. 
The interest of this software is to be able to mix the pattern used. Create a bigger pattern: with the stamp tool you can quickly create a complexe pattern. You want two color versions of a pattern? Duplicate it, and use the remaping tool.

- You're done? Export your chart as an image.
- You're not happy with the color? Modify them.
- You feel like something is still missing? See below.

Here is a screenshot of the application.

![](Preview.png)

## Using this software

This software is licensed as GNU GPL v3 (you can find a copy of this licence included in the repository).
Lama Studio relies on Qt5 for the user interface and LUA for saving and loading the file.
Please cite me (Raphaël Charrondière) when you write about the software.

**Warning!** This is a personnal project, first intended for my use. I know it is poorly documented. Do not request any additionnal feature, I will not add it. 
However, feel free to get involved and extend the capabilities of the software. That's why it's opensource.

