#include "GrilleEditorToolRect.hpp"
#include "Actions/ActionRect.hpp"

GrilleEditorToolRect::GrilleEditorToolRect ( ActionMng* mgr, int style ) :
  mgr ( mgr ),
  style ( style )
{
}

void GrilleEditorToolRect::selectStyle ( int style )
{
  this->style = style;
}

Action* GrilleEditorToolRect::toAction ( Action* prev )
{
  int sx = std::min ( start.X(),end.X() );
  int ex = std::max ( start.X(),end.X() );
  int sy = std::min ( start.Y(),end.Y() );
  int ey = std::max ( start.Y(),end.Y() );

  Dimensions rstart ( sx,sy );
  Dimensions rsize ( ex-sx+1,ey-sy+1 );
  return new ActionRect ( mgr,prev,rstart,rsize,style );
}

std::vector< std::pair< Dimensions, int > > GrilleEditorToolRect::mask()
{
  std::vector< std::pair< Dimensions, int > > res;
  int sx = std::min ( start.X(),end.X() );
  int ex = std::max ( start.X(),end.X() );
  int sy = std::min ( start.Y(),end.Y() );
  int ey = std::max ( start.Y(),end.Y() );
  for ( int x=sx; x<=ex; x++ )
    for ( int y=sy; y<=ey; y++ )
      res.push_back ( std::pair< Dimensions, int > ( Dimensions ( x,y ),style ) );
  return res;
}

std::vector<std::pair<Dimensions, int> > GrilleEditorToolRect::toolOverviewmask(Dimensions cell)
{
  std::vector<std::pair<Dimensions, int> > res;
  res.push_back(std::pair<Dimensions, int>(cell, style));
  return res;
}

void GrilleEditorToolRect::cursorEnter ( Dimensions cell )
{
  end = cell;
}

void GrilleEditorToolRect::startAt ( Dimensions cell )
{
  start = cell;
}

void GrilleEditorToolRect::clear()
{
}
