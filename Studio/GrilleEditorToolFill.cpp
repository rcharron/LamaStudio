#include "GrilleEditorToolFill.hpp"
#include <Actions/ActionFill.hpp>

GrilleEditorToolFill::GrilleEditorToolFill(ActionMng* mgr, int style):
  mgr(mgr),
  style(style)
{
}


void GrilleEditorToolFill::selectStyle(int style)
{
  this->style=style;
}

Action* GrilleEditorToolFill::toAction(Action* prev)
{
  return new ActionFill(mgr,prev,from,style);
}

std::vector< std::pair< Dimensions, int > > GrilleEditorToolFill::mask()
{
  return std::vector< std::pair< Dimensions, int > > ();
}

std::vector<std::pair<Dimensions, int> > GrilleEditorToolFill::toolOverviewmask(Dimensions cell)
{
  std::vector<std::pair<Dimensions, int> > res;
  res.push_back(std::pair<Dimensions, int>(cell, style));
  return res;
}

void GrilleEditorToolFill::cursorEnter(Dimensions cell)
{
  from = cell;
}

void GrilleEditorToolFill::startAt(Dimensions cell)
{
  from = cell;
}

void GrilleEditorToolFill::clear()
{

}
