#include "ChooseMotifDlg.hpp"
#include <QVBoxLayout>

ChooseMotifDlg::ChooseMotifDlg(QWidget* parent):
  QDialog(parent)
{
  QVBoxLayout* layout = new QVBoxLayout(this);
  liste = new QComboBox(this);
  buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok
                                     | QDialogButtonBox::Cancel);
  layout->addWidget(liste);
  layout->addWidget(buttonBox);
  
  connect(buttonBox, &QDialogButtonBox::accepted, this, &QDialog::accept);
  connect(buttonBox, &QDialogButtonBox::rejected, this, &QDialog::reject);
}

void ChooseMotifDlg::addMotif(QString m)
{
  liste->addItem(m);
}

QString ChooseMotifDlg::getMotif()
{
  return liste->currentText();
}
