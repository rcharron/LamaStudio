#include "ModuleResizeDlg.hpp"
#include <QLabel>
#include <QGridLayout>


ModuleResizeDlg::ModuleResizeDlg(Dimensions orSize, QWidget* parent):
  QDialog(parent),
  orSize(orSize)
{
  QLabel* l_orSize = new QLabel(QString::number(orSize.X())+"x"+QString::number(orSize.Y()),this);
  l_orSize->setFrameStyle(QFrame::Box);
  l_orSize->setAlignment(Qt::AlignCenter);
  
  right = new QLineEdit(this);
  left = new QLineEdit(this);
  top = new QLineEdit(this);
  bottom = new QLineEdit(this);
  
  rightv=new QIntValidator(-999,999,right);
  rightv->setBottom(-orSize.X()+1);
  leftv=new QIntValidator(-999,999,left);
  leftv->setBottom(-orSize.X()+1);
  topv=new QIntValidator(-999,999,top);
  topv->setBottom(-orSize.Y()+1);
  bottomv=new QIntValidator(-999,999,bottom);
  bottomv->setBottom(-orSize.Y()+1);
  
  right->setValidator(rightv);
  left->setValidator(leftv);
  top->setValidator(topv);
  bottom->setValidator(bottomv);
  
  right->setAlignment(Qt::AlignCenter);
  left->setAlignment(Qt::AlignCenter);
  top->setAlignment(Qt::AlignCenter);
  bottom->setAlignment(Qt::AlignCenter);
  
  buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok
                                     | QDialogButtonBox::Cancel);
  
  QGridLayout* layout = new QGridLayout(this);
  layout->addWidget(top,0,1);
  layout->addWidget(left,1,0);
  layout->addWidget(l_orSize,1,1);
  layout->addWidget(right,1,2);
  layout->addWidget(bottom,2,1);
  layout->addWidget(buttonBox,3,0,1,3);
  
  connect(right,&QLineEdit::textChanged,this,&ModuleResizeDlg::changeRight);
  connect(left,&QLineEdit::textChanged,this,&ModuleResizeDlg::changeLeft);
  connect(top,&QLineEdit::textChanged,this,&ModuleResizeDlg::changeTop);
  connect(bottom,&QLineEdit::textChanged,this,&ModuleResizeDlg::changeBottom);
  connect(buttonBox, &QDialogButtonBox::accepted, this, &QDialog::accept);
  connect(buttonBox, &QDialogButtonBox::rejected, this, &QDialog::reject);
}

Dimensions ModuleResizeDlg::getLeftTop()
{
  return Dimensions(left->text().toInt(),top->text().toInt());
}

Dimensions ModuleResizeDlg::getRightBottom()
{
  return Dimensions(right->text().toInt(),bottom->text().toInt());
}

void ModuleResizeDlg::changeRight(QString t)
{
  addRight = t.toInt();
  leftv->setBottom(-orSize.X()-addRight+1);
}

void ModuleResizeDlg::changeLeft(QString t)
{
  addLeft = t.toInt();
  rightv->setBottom(-orSize.X()-addLeft+1);
}

void ModuleResizeDlg::changeTop(QString t)
{
  addTop = t.toInt();
  bottomv->setBottom(-orSize.Y()-addTop+1);
}

void ModuleResizeDlg::changeBottom(QString t)
{
  addBottom = t.toInt();
  topv->setBottom(-orSize.Y()-addBottom+1);
}

