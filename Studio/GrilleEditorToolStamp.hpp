#ifndef GRILLEEDITORTOOLSTAMP_H
#define GRILLEEDITORTOOLSTAMP_H

#include "GrilleEditorTool.hpp"
#include "Factory.hpp"

class GrilleEditorToolStamp : public GrilleEditorTool
{
public:
  GrilleEditorToolStamp ( ActionMng *mgr, Action *stamp, Factory* factory );
  virtual void selectStyle ( int style );
  virtual Action *toAction ( Action *prev );
  virtual std::vector< std::pair< Dimensions, int > > toolOverviewmask ( Dimensions cell );
  virtual std::vector< std::pair< Dimensions, int > > mask();
  virtual void cursorEnter ( Dimensions cell );
  virtual void startAt ( Dimensions cell );
  virtual void clear();
private:
  ActionMng *mgr;
  Action *stamp;
  Factory* factory;
  Dimensions where;
  std::vector< std::pair< Dimensions, int > > maskOr;
};

#endif // GRILLEEDITORTOOLSTAMP_H
