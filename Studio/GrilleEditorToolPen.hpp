#ifndef GRILLEEDITORTOOLPEN_H
#define GRILLEEDITORTOOLPEN_H

#include "GrilleEditorTool.hpp"




class GrilleEditorToolPen : public GrilleEditorTool
{
public:
  GrilleEditorToolPen(ActionMng* mgr, int style);
  virtual void selectStyle ( int style );
  virtual Action* toAction(Action* prev);
  virtual std::vector< std::pair< Dimensions, int > > mask();
  virtual std::vector<std::pair<Dimensions, int> > toolOverviewmask(Dimensions cell);
  virtual void cursorEnter ( Dimensions cell );
  virtual void startAt ( Dimensions cell );
  virtual void clear();
private:
  ActionMng* mgr;
  int style;
  std::vector< std::pair< Dimensions, int > > path;
};

#endif // GRILLEEDITORTOOLPEN_H
