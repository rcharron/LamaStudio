#ifndef CHOOSESYMBOLDLG_HPP
#define CHOOSESYMBOLDLG_HPP

#include <QDialog>
#include <QDialogButtonBox>
#include <QGridLayout>
#include "SymbolsDrawer.hpp"

class ChooseSymbolDlg : public QDialog
{
    Q_OBJECT
public:
    ChooseSymbolDlg(QWidget* parent = nullptr);
    SymbolsDrawer::Symbol getSelected();
private slots:
    void selSym(SymbolsDrawer::Symbol s);
private:
    void fillSym(SymbolsDrawer::Symbol s, int i, int j);
    SymbolsDrawer sdraw;
    QDialogButtonBox* buttonBox;
    SymbolsDrawer::Symbol symb;
    QGridLayout* symbGrid;
};

#endif // CHOOSESYMBOLDLG_HPP
