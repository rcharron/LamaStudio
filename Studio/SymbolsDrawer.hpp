#ifndef SYMBOLS_DRAWER_HPP
#define SYMBOLS_DRAWER_HPP

#include <QBrush>
#include <QPen>
#include <QPixmap>
#include <QColor>
#include <QSize>
#include <QPainter>
#include <QPointF>
#include <map>

#include <Style.hpp>
#include <dimensions.hpp>

class SymbolsDrawer
{
public:
  enum Symbol{
	  None = 0,
	  knit = 1,
	  purl = 2,
	  yarnOver = 3,
	  k2tog = 4,
	  ssk = 5,
	  k3tog = 6,
	  sk2po = 7,
	  k2skpo = 8,
	  k1tbl = 9,
      bobble = 10,
      torseNeedleBackKnit = 11,
      torseNeedleBackPurl = 12,
      torseNeedleBackKnitTrLeft = 13,
      torseNeedleBackPurlTrLeft = 14,
      torseNeedleBackKnitTrRight = 15,
      torseNeedleBackPurlTrRight = 16,
      torseNeedleFrontKnit = 17,
      torseNeedleFrontPurl = 18,
      torseNeedleFrontKnitTrLeft = 19,
      torseNeedleFrontPurlTrLeft = 20,
      torseNeedleFrontKnitTrRight = 21,
      torseNeedleFrontPurlTrRight = 22,
      torseNeedleBackKnitLeftEnd = 23,
      torseNeedleBackPurlLeftEnd = 24,
      torseNeedleBackKnitRightEnd = 25,
      torseNeedleBackPurlRightEnd = 26,
      torseNeedleFrontKnitLeftEnd = 27,
      torseNeedleFrontPurlLeftEnd = 28,
      torseNeedleFrontKnitRightEnd = 29,
      torseNeedleFrontPurlRightEnd = 30,
      k2togWS = 31,
      sskWS = 32,
      k3togWS = 33,
      sk2poWS = 34,
      k2skpoWS = 35,
      k1tblWS = 36
  };
  static const int NbSymbols = 37;
  SymbolsDrawer();
  void setColor(QColor col);
  void setBackgroundColor(QColor col);
  void setSize(Dimensions dim);
  void setSize(QSize dim);
  void setPenWidth(int width);
  QPixmap draw(Symbol sym);
  QPixmap drawNone();
  QPixmap drawKnit();
  QPixmap drawPurl();
  QPixmap drawYarnOver();
  QPixmap drawK2tog(bool RS=true);
  QPixmap drawSsk(bool RS=true);
  QPixmap drawK3tog(bool RS=true);
  QPixmap drawSk2po(bool RS=true);
  QPixmap drawK2skpo(bool RS=true);
  QPixmap drawK1tbl(bool RS=true);
  QPixmap drawBobble();
  QPixmap drawTorsade(Symbol sym);
  QPixmap drawTorsadeEnd(Symbol sym);
  QPixmap drawTorsade(bool needleFront, bool knit, bool tr, bool trRight = false);
  QPixmap drawTorsadeEnd(bool needleFront, bool knit, bool endRight);
  void draw(QPainter &painter, const QPointF& offset, Symbol sym);
  void drawNone(QPainter &painter, const QPointF& offset);
  void drawKnit(QPainter &painter, const QPointF& offset);
  void drawPurl(QPainter &painter, const QPointF& offset);
  void drawYarnOver(QPainter &painter, const QPointF& offset);
  void drawK2tog(QPainter &painter, const QPointF& offset, bool RS=true);
  void drawSsk(QPainter &painter, const QPointF& offset, bool RS=true);
  void drawK3tog(QPainter &painter, const QPointF& offset, bool RS=true);
  void drawSk2po(QPainter &painter, const QPointF& offset, bool RS=true);
  void drawK2skpo(QPainter &painter, const QPointF& offset, bool RS=true);
  void drawK1tbl(QPainter &painter, const QPointF& offset, bool RS=true);
  void drawBobble(QPainter &painter, const QPointF& offset);
  void drawTorsade(QPainter &painter, const QPointF& offset, Symbol sym);
  void drawTorsadeEnd(QPainter &painter, const QPointF& offset, Symbol sym);
  void drawTorsade(QPainter &painter, const QPointF& offset, bool needleFront, bool knit, bool tr, bool trRight = false);
  void drawTorsadeEnd(QPainter &painter, const QPointF& offset, bool needleFront, bool knit, bool endRight);
private:
  QColor color;
  QColor backgroundColor;
  QSize size;
  QPen pen;
};


#endif // !SYMBOLS_DRAWER_HPP
