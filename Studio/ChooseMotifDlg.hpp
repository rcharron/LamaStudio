#ifndef CHOOSEMOTIFDLG_H
#define CHOOSEMOTIFDLG_H

#include <QDialog>
#include <QComboBox>
#include <QDialogButtonBox>

class ChooseMotifDlg : public QDialog
{
  Q_OBJECT
public:
  ChooseMotifDlg(QWidget* parent = nullptr);
  void addMotif(QString);
  QString getMotif(); 
private:
  QComboBox* liste;
  QDialogButtonBox* buttonBox;
};

#endif // CHOOSEMOTIFDLG_H
