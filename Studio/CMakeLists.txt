project(Studio)

set(CMAKE_CXX_STANDARD 17)

# Find includes in corresponding build directories
set(CMAKE_INCLUDE_CURRENT_DIR ON)
# Instruct CMake to run moc automatically when needed.
set(CMAKE_AUTOMOC ON)

# Find the QtWidgets library
find_package(Qt5 REQUIRED COMPONENTS Core Gui Widgets Svg)

qt5_add_resources(UI_RESOURCES ../res/res.qrc)

add_executable(LamaStudio
    main.cpp 
    ${UI_RESOURCES}
    Grille.hpp
    Grille.cpp
    GrilleEditor.hpp
    GrilleEditor.cpp
	SymbolsDrawer.hpp
    SymbolsDrawer.cpp
    GrilleEditorTool.hpp
#    MotifRenderer.hpp
#    MotifRenderer.cpp
    Studio.hpp
    Studio.cpp
    ToolStyles.hpp
    ToolStyles.cpp
    ModulesInterface.hpp
    ModulesInterface.cpp
    NewModuleDlg.hpp
    NewModuleDlg.cpp
    ModuleResizeDlg.hpp
    ModuleResizeDlg.cpp
    ChooseMotifDlg.hpp
    ChooseMotifDlg.cpp
    RemapStyleDlg.hpp
    RemapStyleDlg.cpp
    RemapColorsDlg.hpp
    RemapColorsDlg.cpp
    ChooseSymbolDlg.hpp
    ChooseSymbolDlg.cpp
    GridStyle.hpp
    GridStyle.cpp

    GrilleEditorToolNone.hpp
    GrilleEditorToolNone.cpp
    GrilleEditorToolRect.hpp
    GrilleEditorToolRect.cpp
    GrilleEditorToolPen.hpp
    GrilleEditorToolPen.cpp
    GrilleEditorToolFill.hpp
    GrilleEditorToolFill.cpp
    GrilleEditorToolStamp.hpp
    GrilleEditorToolStamp.cpp
    )
    
target_link_libraries(LamaStudio 
  LamaCore
	Qt5::Core
	Qt5::Gui
	Qt5::Widgets
	Qt5::Svg)

