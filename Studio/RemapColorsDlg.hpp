#ifndef REMAPCOLORSDLG_HPP
#define REMAPCOLORSDLG_HPP

#include <QDialog>
#include <QComboBox>
#include <QDialogButtonBox>
#include <QMap>
#include <map>
#include "Style.hpp"
#include "SymbolsDrawer.hpp"

class RemapColorsDlg : public QDialog
{
    Q_OBJECT
public:
    RemapColorsDlg(QVector<QPair<Style::Color,Style::Color>> colors,
            QMap<QPair<int,int>,int> grid, QWidget* parent = nullptr);
    std::map<int,int> get();
  private:
    QMap<QPair<int,int>,int> grid;
    std::map<int,QPixmap> colorPx;
    std::map<int,QComboBox*> listes;
    QDialogButtonBox* buttonBox;
};

#endif // REMAPCOLORSDLG_HPP
