#include "GridStyle.hpp"
#include "SymbolsDrawer.hpp"
#include "ChooseSymbolDlg.hpp"
#include <QColorDialog>

GridStyle::GridStyle(QWidget *parent) : QWidget(parent)
{
    grid = new QGridLayout();
    setLayout(grid);
    addRowBtn = new QPushButton("+", this);
    addColBtn = new QPushButton("+", this);
    noStyleBtn = new QPushButton("x",this);

    addRowBtn->setMaximumSize(30,30);
    addColBtn->setMaximumSize(30,30);
    noStyleBtn->setMaximumSize(30,30);
    addRowBtn->setMinimumSize(30,30);
    addColBtn->setMinimumSize(30,30);
    noStyleBtn->setMinimumSize(30,30);
    addRowBtn->setFlat(true);
    addColBtn->setFlat(true);
    noStyleBtn->setFlat(true);
    noStyleBtn->setCheckable(true);

    connect(addRowBtn,SIGNAL(clicked()),this,SLOT(clickAddRow()));
    connect(addColBtn,SIGNAL(clicked()),this,SLOT(clickAddCol()));
    connect(noStyleBtn,&QPushButton::clicked,this,[=](){emit select(-1,-1);});

    grid->addWidget(noStyleBtn,0,0);
    grid->addWidget(addColBtn,0,1);
    grid->addWidget(addRowBtn,1,0);

    selectedID=0;
    counterID=1;
    addRow(0);
    addColumn(Style::Color(255,255,255));
    select(-1,-1);
}

void GridStyle::clear()
{
    for ( auto* swidget : rowBtn )
    {
        grid->removeWidget ( swidget );
        swidget->close();
        swidget->deleteLater();
    }
    for ( auto* swidget : columnBgCBtn )
    {
        grid->removeWidget ( swidget );
        swidget->close();
        swidget->deleteLater();
    }
    for ( auto* swidget : columnSyCBtn )
    {
        grid->removeWidget ( swidget );
        swidget->close();
        swidget->deleteLater();
    }
    for ( auto* swidget : stylesSamples )
    {
        grid->removeWidget ( swidget );
        swidget->close();
        swidget->deleteLater();
    }

    grid->addWidget(addColBtn,0,1);
    grid->addWidget(addRowBtn,1,0);
    rows.clear();
    rowBtn.clear();
    columns.clear();
    columnBgCBtn.clear();
    columnSyCBtn.clear();
    stylesSamples.clear();
    stylesID.clear();
    IDToStyle.clear();
    lastSelected=QPair<int,int>(-1,-1);
    select(-1,-1);
}

int GridStyle::rowOf(Style sty)
{
    return rows.indexOf(sty.symbol);
}

int GridStyle::columnOf(Style sty, int extraOffset)
{
    return columns.indexOf(QPair<Style::Color,Style::Color>(sty.backgroundColor,sty.foregroundColor), extraOffset);
}

void GridStyle::addRow(int symbol, bool addSty)
{
    int i=rows.size();
    rows.push_back(symbol);
    QPushButton* newRBt=new QPushButton(this);
    SymbolsDrawer sdraw;
    sdraw.setPenWidth(2);
    sdraw.setSize(Dimensions(30,30));
    QIcon ButtonIcon(sdraw.draw(static_cast<SymbolsDrawer::Symbol>(symbol)));
    newRBt->setFlat(true);
    newRBt->setIcon(ButtonIcon);
    newRBt->setIconSize(QSize(30,30));
    rowBtn.push_back(newRBt);
    grid->addWidget(newRBt, rows.size(), 0);
    connect(newRBt, &QPushButton::clicked,this,[=](){ this->clickChangeRow(i); });
    int trow=rows.size();
    grid->addWidget(addRowBtn,1+trow,0);
    for(int j=0;j<columns.size();j++)
    {
        grid->addWidget(columnSyCBtn.at(j),1+trow,j+1);
    }
    for(int j=0;j<columns.size();j++)
    {
        QPushButton* stybtn=new QPushButton;
        Style::Color bg=columns[j].first, sy=columns[j].second;
        sdraw.setBackgroundColor(QColor(bg.r,bg.g,bg.b));
        sdraw.setColor(QColor(sy.r,sy.g,sy.b));
        QIcon ButtonIcon(sdraw.draw(static_cast<SymbolsDrawer::Symbol>(symbol)));
        stybtn->setFlat(true);
        stybtn->setIcon(ButtonIcon);
        stybtn->setIconSize(QSize(30,30));
        stybtn->setCheckable(true);
        grid->addWidget(stybtn, trow, j+1);
        stylesSamples[QPair<int,int>(i,j)]=stybtn;
        connect(stybtn,&QPushButton::clicked,this,[=](){this->select(i,j);});
        if(!addSty)continue;
        int newID=counterID++;
        stylesID[QPair<int,int>(i,j)]=newID;
        IDToStyle[newID]=QPair<int,int>(i,j);
        emit styleChanged(newID);
    }
}

void GridStyle::addColumn(Style::Color backgroundColor, Style::Color symbColor, bool addSty)
{
    int i=columns.size();
    columns.push_back(QPair<Style::Color,Style::Color>(backgroundColor, symbColor));
    QPushButton* newBgC=new QPushButton(this);
    QPushButton* newSyC=new QPushButton(this);
    QPixmap pxb(30, 30);
    QPixmap pxs(30, 30);
    pxb.fill(QColor(backgroundColor.r, backgroundColor.g, backgroundColor.b));
    pxs.fill(QColor(symbColor.r, symbColor.g, symbColor.b));
    newBgC->setIcon(pxb);
    newSyC->setIcon(pxs);
    newBgC->setFlat(true);
    newSyC->setFlat(true);
    newBgC->setIconSize(QSize(30,30));
    newSyC->setIconSize(QSize(30,30));
    columnBgCBtn.push_back(newBgC);
    columnSyCBtn.push_back(newSyC);
    int tcol=columns.size();
    grid->addWidget(newBgC, 0, tcol);
    grid->addWidget(newSyC, rows.size()+1, tcol);
    connect(newBgC, &QPushButton::clicked,this,[=](){ this->clickChangeColBgC(i); });
    connect(newSyC, &QPushButton::clicked,this,[=](){ this->clickChangeColSyC(i); });
    grid->addWidget(addColBtn,0,1+tcol);
    SymbolsDrawer sdraw;
    sdraw.setPenWidth(2);
    sdraw.setSize(Dimensions(30,30));
    Style::Color bg=columns[i].first, sy=columns[i].second;
    sdraw.setBackgroundColor(QColor(bg.r,bg.g,bg.b));
    sdraw.setColor(QColor(sy.r,sy.g,sy.b));
    for(int j=0;j<rows.size();j++)
    {
        QPushButton* stybtn=new QPushButton;
        QIcon ButtonIcon(sdraw.draw(static_cast<SymbolsDrawer::Symbol>(rows[j])));
        stybtn->setFlat(true);
        stybtn->setIcon(ButtonIcon);
        stybtn->setIconSize(QSize(30,30));
        stybtn->setCheckable(true);
        grid->addWidget(stybtn, j+1, tcol);
        stylesSamples[QPair<int,int>(j,i)]=stybtn;
        connect(stybtn,&QPushButton::clicked,this,[=](){this->select(j,i);});
        if(!addSty)continue;
        int newID=counterID++;
        stylesID[QPair<int,int>(j,i)]=newID;
        IDToStyle[newID]=QPair<int,int>(j,i);
        emit styleChanged(newID);
    }
}

void GridStyle::clickAddRow()
{
    ChooseSymbolDlg *dlg = new ChooseSymbolDlg(this);
    if ( dlg->exec() == QDialog::Accepted )
    {
        int symbol = dlg->getSelected();
        addRow(symbol);
    }
}

void GridStyle::clickAddCol()
{
    QColor color = QColorDialog::getColor(Qt::white, this, tr("Choose symbol color"));
    if (!color.isValid()) return;
    addColumn(Style::Color(color.red(), color.green(), color.blue()));
}

void GridStyle::clickChangeRow(int index)
{
    ChooseSymbolDlg *dlg = new ChooseSymbolDlg(this);
    if ( dlg->exec() == QDialog::Accepted )
    {
        int symbol = dlg->getSelected();
        SymbolsDrawer sdraw;
        sdraw.setPenWidth(2);
        sdraw.setSize(Dimensions(30,30));
        QIcon ButtonIcon(sdraw.draw(static_cast<SymbolsDrawer::Symbol>(symbol)));
        rowBtn.at(index)->setIcon(ButtonIcon);
        rows[index]=symbol;
        for(int j=0;j<columns.size();j++)
        {
            Style::Color bg=columns[j].first, sy=columns[j].second;
            sdraw.setBackgroundColor(QColor(bg.r,bg.g,bg.b));
            sdraw.setColor(QColor(sy.r,sy.g,sy.b));
            QIcon ButtonIcon(sdraw.draw(static_cast<SymbolsDrawer::Symbol>(symbol)));
            stylesSamples[QPair<int,int>(index,j)]->setIcon(ButtonIcon);
            int sid=stylesID[QPair<int,int>(index,j)];
            if(sid>0)emit styleChanged(sid);
        }
    }
}

void GridStyle::clickChangeColBgC(int index)
{
    Style::Color c=columns.at(index).first;
    QColor col = QColor::fromRgb(c.r, c.g, c.b);
    QColor color = QColorDialog::getColor(col, this, tr("Choose symbol color"));
    if (!color.isValid()) return;
    QPixmap px(30, 30);
    px.fill(color);
    columnBgCBtn.at(index)->setIcon(px);
    c=Style::Color( color.red(), color.green(), color.blue());
    columns[index].first=c;
    SymbolsDrawer sdraw;
    sdraw.setPenWidth(2);
    sdraw.setSize(Dimensions(30,30));
    Style::Color bg=columns[index].first, sy=columns[index].second;
    sdraw.setBackgroundColor(QColor(bg.r,bg.g,bg.b));
    sdraw.setColor(QColor(sy.r,sy.g,sy.b));
    for(int j=0;j<rows.size();j++)
    {
        QIcon ButtonIcon(sdraw.draw(static_cast<SymbolsDrawer::Symbol>(rows[j])));
        stylesSamples[QPair<int,int>(j,index)]->setIcon(ButtonIcon);
        int sid=stylesID[QPair<int,int>(j,index)];
        if(sid>0)emit styleChanged(sid);
    }
}

void GridStyle::clickChangeColSyC(int index)
{
    Style::Color c=columns.at(index).second;
    QColor col = QColor::fromRgb(c.r, c.g, c.b);
    QColor color = QColorDialog::getColor(col, this, tr("Choose symbol color"));
    if (!color.isValid()) return;
    c=Style::Color( color.red(), color.green(), color.blue());
    QPixmap px(30, 30);
    px.fill(color);
    columnSyCBtn.at(index)->setIcon(px);
    columns[index].second=c;
    SymbolsDrawer sdraw;
    sdraw.setPenWidth(2);
    sdraw.setSize(Dimensions(30,30));
    Style::Color bg=columns[index].first, sy=columns[index].second;
    sdraw.setBackgroundColor(QColor(bg.r,bg.g,bg.b));
    sdraw.setColor(QColor(sy.r,sy.g,sy.b));
    for(int j=0;j<rows.size();j++)
    {
        QIcon ButtonIcon(sdraw.draw(static_cast<SymbolsDrawer::Symbol>(rows[j])));
        stylesSamples[QPair<int,int>(j,index)]->setIcon(ButtonIcon);
        int sid=stylesID[QPair<int,int>(j,index)];
        if(sid>0)emit styleChanged(sid);
    }
}

void GridStyle::select(int row, int col)
{
    if(lastSelected.first<0||lastSelected.second<0)
        noStyleBtn->setChecked(false);
    else
        stylesSamples[lastSelected]->setChecked(false);
    if(row<0||col<0)
    {
        noStyleBtn->setChecked(true);
        selectedID=0;
    }
    else
    {
        selectedID=stylesID[QPair<int,int>(row,col)];
        if(selectedID==0)
        {
            selectedID=counterID++;
            stylesID[QPair<int,int>(row,col)]=selectedID;
            IDToStyle[selectedID]=QPair<int,int>(row,col);
        }
        stylesSamples[QPair<int,int>(row,col)]->setChecked(true);
    }
    lastSelected=QPair<int,int>(row,col);
    emit styleSelected(selectedID);
}

Style GridStyle::get(int styleId)
{
    if (styleId<=0)return Style();
    Style sty;
    sty.symbol=rows.at(IDToStyle[styleId].first);
    sty.backgroundColor=columns.at(IDToStyle[styleId].second).first;
    sty.foregroundColor=columns.at(IDToStyle[styleId].second).second;
    return sty;
}

int GridStyle::getSelected()
{
    return selectedID;
}

void GridStyle::setStyles(std::map<int, Style> styles)
{
    clear();
    for(auto e:styles)
        set(e.first, e.second);
}

std::map<int, Style> GridStyle::getStyles()
{
    std::map<int, Style> res;
    for(auto e:IDToStyle.keys())
    {
        res[e]=get(e);
    }
    return res;
}

void GridStyle::set(int styleId, Style style)
{
    int row=rowOf(style);
    if(row<0)
    {
        addRow(style.symbol, false);
        row=rowOf(style);
    }
    int col=columnOf(style);
    if(col<0)
    {
        addColumn(style.backgroundColor, style.foregroundColor, false);
        col=columnOf(style);
    }
    while(true)
    {
        int id=stylesID[QPair<int,int>(row,col)];
        if(id==0)//new style
        {
            stylesID[QPair<int,int>(row,col)]=styleId;
            IDToStyle[styleId]=QPair<int,int>(row,col);
            break;
        }
        else if(id==styleId)return ;//already in
        else //wrong correspondance (two matching ids)
        {
            int newcol=columnOf(style,col+1);
            if(newcol<0)
            {
                addColumn(style.backgroundColor, style.foregroundColor, false);
                newcol=columnOf(style,col+1);
            }
            col=newcol;
        }

    }
    counterID=std::max(counterID, styleId+1);
}

QMap<QPair<int, int>, int> GridStyle::getStylesGrid()
{
    return stylesID;
}

QVector<QPair<Style::Color, Style::Color> > GridStyle::getColors()
{
    return columns;
}
