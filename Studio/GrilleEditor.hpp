#ifndef GRILLEEDITOR_H
#define GRILLEEDITOR_H

#include "Grille.hpp"
#include "GrilleEditor.hpp"
#include <map>

class GrilleEditorTool;
class GrilleEditorToolStamp;

class GrilleEditor : public Grille
{
public:
  GrilleEditor(ActionMng* mgr, Factory* motifFactory, QWidget* parent = nullptr );
  enum class Tool{None, Pen, Rect, Fill};
  void selectTool(Tool t);
  void selectStampTool(GrilleEditorToolStamp* stampTool);
  void clearStyles();
public slots:
  void selecteStyle(int style);
protected:
  virtual void paintMask(QPainter& painter) override;
  virtual void cursorEnterCell(QPoint cellid, bool isPressed) override;
  virtual void mousePressCell(QPoint cellid, bool leftButton) override;
  virtual void mouseReleaseCell(QPoint cellid) override;
private:
  void finalizeTool();
  ActionMng* mgr;
  GrilleEditorTool* selectedTool;
  int selectedStyle;
  bool mouseWasPressed;
};

#endif // GRILLEEDITOR_H
