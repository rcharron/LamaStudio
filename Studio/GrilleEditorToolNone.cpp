#include "GrilleEditorToolNone.hpp"

void GrilleEditorToolNone::selectStyle(int style)
{
}

Action* GrilleEditorToolNone::toAction(Action* prev)
{
  return nullptr;
}

std::vector< std::pair< Dimensions, int > > GrilleEditorToolNone::mask()
{
  return std::vector< std::pair< Dimensions, int > >();
}

std::vector<std::pair<Dimensions, int> > GrilleEditorToolNone::toolOverviewmask(Dimensions cell)
{
  return std::vector< std::pair< Dimensions, int > >();
}


void GrilleEditorToolNone::cursorEnter(Dimensions cell)
{
}

void GrilleEditorToolNone::startAt(Dimensions cell)
{
}

void GrilleEditorToolNone::clear()
{
}
