#ifndef GRILLEEDITORTOOLFILL_H
#define GRILLEEDITORTOOLFILL_H

#include "GrilleEditorTool.hpp"

class GrilleEditorToolFill : public GrilleEditorTool
{
public:
  GrilleEditorToolFill ( ActionMng* mgr, int style );
  virtual void selectStyle ( int style );
  virtual Action* toAction ( Action* prev );
  virtual std::vector< std::pair< Dimensions, int > > mask();
  virtual std::vector<std::pair<Dimensions, int> > toolOverviewmask(Dimensions cell);
  virtual void cursorEnter ( Dimensions cell );
  virtual void startAt ( Dimensions cell );
  virtual void clear();
private:
  ActionMng* mgr;
  int style;
  Dimensions from;
};

#endif // GRILLEEDITORTOOLFILL_H
