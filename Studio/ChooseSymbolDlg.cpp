#include "ChooseSymbolDlg.hpp"
#include <QVBoxLayout>
#include <QPushButton>

ChooseSymbolDlg::ChooseSymbolDlg(QWidget* parent) :
    QDialog(parent),
    symb(SymbolsDrawer::Symbol::None)
{
    QVBoxLayout* layout = new QVBoxLayout(this);
    symbGrid = new QGridLayout();
    buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok
                                       | QDialogButtonBox::Cancel);

    sdraw.setSize(Dimensions(40,40));
    sdraw.setPenWidth(2);

    int rowStd=0;
    fillSym(SymbolsDrawer::Symbol::None, rowStd, 0);
    fillSym(SymbolsDrawer::Symbol::knit, rowStd, 1);
    fillSym(SymbolsDrawer::Symbol::yarnOver, rowStd, 2);
    fillSym(SymbolsDrawer::Symbol::k1tbl, rowStd, 3);
    fillSym(SymbolsDrawer::Symbol::bobble, rowStd, 4);
    fillSym(SymbolsDrawer::Symbol::k2tog, rowStd+1, 0);
    fillSym(SymbolsDrawer::Symbol::ssk, rowStd+1, 1);
    fillSym(SymbolsDrawer::Symbol::sk2po, rowStd+1, 2);
    fillSym(SymbolsDrawer::Symbol::k2skpo, rowStd+1, 3);
    fillSym(SymbolsDrawer::Symbol::k3tog, rowStd+1, 4);
    int rowWS=2;
    fillSym(SymbolsDrawer::Symbol::purl, rowWS, 0);
    fillSym(SymbolsDrawer::Symbol::k1tblWS, rowWS, 1);
    fillSym(SymbolsDrawer::Symbol::k2togWS, rowWS+1, 0);
    fillSym(SymbolsDrawer::Symbol::sskWS, rowWS+1, 1);
    fillSym(SymbolsDrawer::Symbol::sk2poWS, rowWS+1, 2);
    fillSym(SymbolsDrawer::Symbol::k2skpoWS, rowWS+1, 3);
    fillSym(SymbolsDrawer::Symbol::k3togWS, rowWS+1, 4);

    //Torsades
    int rowTorse=4;
    fillSym(SymbolsDrawer::Symbol::torseNeedleBackKnitLeftEnd, rowTorse, 0);
    fillSym(SymbolsDrawer::Symbol::torseNeedleBackKnit, rowTorse, 1);
    fillSym(SymbolsDrawer::Symbol::torseNeedleBackKnitTrRight, rowTorse, 2);
    fillSym(SymbolsDrawer::Symbol::torseNeedleFrontKnitTrLeft, rowTorse, 3);
    fillSym(SymbolsDrawer::Symbol::torseNeedleFrontKnitRightEnd, rowTorse, 4);
    fillSym(SymbolsDrawer::Symbol::torseNeedleBackPurlLeftEnd, rowTorse+1, 0);
    fillSym(SymbolsDrawer::Symbol::torseNeedleBackPurl, rowTorse+1, 1);
    fillSym(SymbolsDrawer::Symbol::torseNeedleBackPurlTrRight, rowTorse+1, 2);
    fillSym(SymbolsDrawer::Symbol::torseNeedleFrontPurlTrLeft, rowTorse+1, 3);
    fillSym(SymbolsDrawer::Symbol::torseNeedleFrontPurlRightEnd, rowTorse+1, 4);

    fillSym(SymbolsDrawer::Symbol::torseNeedleFrontKnitLeftEnd, rowTorse+2, 0);
    fillSym(SymbolsDrawer::Symbol::torseNeedleFrontKnit, rowTorse+2, 1);
    fillSym(SymbolsDrawer::Symbol::torseNeedleFrontKnitTrRight, rowTorse+2, 2);
    fillSym(SymbolsDrawer::Symbol::torseNeedleBackKnitTrLeft, rowTorse+2, 3);
    fillSym(SymbolsDrawer::Symbol::torseNeedleBackKnitRightEnd, rowTorse+2, 4);
    fillSym(SymbolsDrawer::Symbol::torseNeedleFrontPurlLeftEnd, rowTorse+3, 0);
    fillSym(SymbolsDrawer::Symbol::torseNeedleFrontPurl, rowTorse+3, 1);
    fillSym(SymbolsDrawer::Symbol::torseNeedleFrontPurlTrRight, rowTorse+3, 2);
    fillSym(SymbolsDrawer::Symbol::torseNeedleBackPurlTrLeft, rowTorse+3, 3);
    fillSym(SymbolsDrawer::Symbol::torseNeedleBackPurlRightEnd, rowTorse+3, 4);
    /*QLabel* lNone=new QLabel(this);
    lNone->setPixmap(sdraw.draw(SymbolsDrawer::Symbol::None));
    symbGrid->addWidget(lNone, 0, 0);

    QLabel* lKnit=new QLabel(this);
    lKnit->setPixmap(sdraw.draw(SymbolsDrawer::Symbol::knit));
    symbGrid->addWidget(lKnit, 0, 1);*/


    layout->addLayout(symbGrid);
    layout->addWidget(buttonBox);

    connect(buttonBox, &QDialogButtonBox::accepted, this, &QDialog::accept);
    connect(buttonBox, &QDialogButtonBox::rejected, this, &QDialog::reject);
}

void ChooseSymbolDlg::fillSym(SymbolsDrawer::Symbol s, int i, int j)
{
    QPushButton* llabel=new QPushButton();
    QIcon ButtonIcon(sdraw.draw(s));
    llabel->setFlat(true);
    llabel->setIcon(ButtonIcon);
    llabel->setIconSize(QSize(40,40));
    //llabel->setPixmap(sdraw.draw(s));
    symbGrid->addWidget(llabel, i, j);
    connect(llabel, &QPushButton::clicked,this,[=](){ this->selSym(s); });
}

void ChooseSymbolDlg::selSym(SymbolsDrawer::Symbol s)
{
    symb=s;
    emit QDialog::accept();
}

SymbolsDrawer::Symbol ChooseSymbolDlg::getSelected()
{
    return symb;
}
