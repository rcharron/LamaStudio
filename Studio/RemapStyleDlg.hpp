#ifndef REMAPSTYLEDLG_H
#define REMAPSTYLEDLG_H

#include <QDialog>
#include <QPixmap>
#include <QComboBox>
#include <QDialogButtonBox>
#include <QMap>
#include <map>
#include <Style.hpp>


class RemapStyleDlg : public QDialog
{
  Q_OBJECT
public:
  RemapStyleDlg(std::map<int, Style> stys, QWidget* parent = nullptr);
  std::map<int,int> get();
private:
  std::map<int,QPixmap> styles;
  std::map<int,QComboBox*> listes;
  QDialogButtonBox* buttonBox;
};

#endif // REMAPSTYLEDLG_H
