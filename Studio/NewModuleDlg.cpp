#include "NewModuleDlg.hpp"
#include <QLabel>
#include <QGridLayout>
#include <QIntValidator>

NewModuleDlg::NewModuleDlg ( QWidget* parent ) :
  QDialog ( parent )
{
  name = new QLineEdit ( this );
  sizeX = new QLineEdit ( this );
  sizeY = new QLineEdit ( this );
  sizeX->setValidator ( new QIntValidator ( 1,9999,sizeX ) );
  sizeY->setValidator ( new QIntValidator ( 1,9999,sizeY ) );
  QLabel* l_name = new QLabel ( tr ( "Name:" ),this );
  QLabel* l_size = new QLabel ( tr ( "Size:" ),this );
  QLabel* l_x = new QLabel ( "x",this );
  
  buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok
                                     | QDialogButtonBox::Cancel);

  QGridLayout* layout = new QGridLayout ( this );
  layout->addWidget ( l_name,0,0 );
  layout->addWidget ( name,0,1,1,3 );
  layout->addWidget ( l_size,1,0 );
  layout->addWidget ( sizeX,1,1 );
  layout->addWidget ( l_x,1,2 );
  layout->addWidget ( sizeY,1,3 );
  layout->addWidget(buttonBox,2,0,1,4);
  
  connect(buttonBox, &QDialogButtonBox::accepted, this, &QDialog::accept);
  connect(buttonBox, &QDialogButtonBox::rejected, this, &QDialog::reject);
}

QString NewModuleDlg::getName()
{
  return name->text();
}

Dimensions NewModuleDlg::getSize()
{
  return Dimensions ( sizeX->text().toInt(),sizeY->text().toInt() );
}
