#ifndef GRILLE_HPP
#define GRILLE_HPP

#include <QBrush>
#include <QPen>
#include <QPixmap>
#include <QWidget>
#include <map>

#include <Style.hpp>
#include <ActionMng.hpp>
#include <Actions/Action.hpp>
#include <Module.hpp>
#include <Motif.hpp>
#include <dimensions.hpp>
#include <Factory.hpp>

#include "SymbolsDrawer.hpp"

class Grille : public QWidget
{
  Q_OBJECT
public:
  Grille (Factory* motifFactory, QWidget* parent = nullptr );
  QSize minimumSizeHint() const override;
  QSize sizeHint() const override;
  void setModule(Module* mod);
  void setStyles(std::map<int, Style> styles);
  void addStyle(int id, Style);
  QPoint toCoordinate(QPoint Cursor);
 public slots:
  QPixmap drawOnPixmap();
  void drawSVG(QString filename);
protected:
  virtual void cursorEnterCell(QPoint cellid, bool isPressed);
  virtual void mousePressCell(QPoint cellid, bool leftButton);
  virtual void mouseReleaseCell(QPoint cellid);
protected:
  void paintEvent ( QPaintEvent *event ) override;
  void mouseMoveEvent ( QMouseEvent *event ) override;
  void mousePressEvent ( QMouseEvent *event ) override;
  void mouseReleaseEvent ( QMouseEvent *event ) override;
  void wheelEvent ( QWheelEvent *event ) override;
  Dimensions getCursorCell();
protected:
  virtual void paintMask(QPainter& painter);
  Factory* motifFactory;
  Module* module;
  std::map<int, Style> styles;
  void draw(Dimensions at, QPainter& painter, Motif& motif);
  void draw(Dimensions at, QPainter& painter, int styleId);
  void draw(Style sty, QPainter& painter);

  QPoint drawGrilleOffset;
  QSize drawGrilleCellSize;

  QPoint cursorCell;
  bool moving = false;
  QPoint drawGrilleOffsetOr;
  QPoint movingCursorOr;
  Motif renderMotif;
  size_t lastMotif;

  SymbolsDrawer symbolDrawer;
  std::map<Style, QPixmap> pixOfStyles;
};


#endif // !GRILLE_HPP
