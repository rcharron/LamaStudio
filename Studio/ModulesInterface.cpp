#include "ModulesInterface.hpp"
#include <QLabel>
#include <QGridLayout>
#include <QMessageBox>
#include <QInputDialog>
#include <QAbstractItemView>
#include <Actions/ActionResize.hpp>
#include "NewModuleDlg.hpp"
#include "ModuleResizeDlg.hpp"

ModulesInterface::ModulesInterface ( ActionMng* mgr, Factory* factory, QWidget* parent )
  : QWidget ( parent ),
    mgr ( mgr ),
    factory(factory)
{
  QGridLayout* layout = new QGridLayout ( this );
  listeModules = new QComboBox ( this );
  listeModules->view()->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
  QLabel* t_name= new QLabel ( tr ( "Name:" ),this );
  nameModule = new QLineEdit ( this );
  addModule = new QPushButton ( tr ( "Add new" ), this );
  deleteModule = new QPushButton ( tr ( "Delete this" ), this );
  resizeModule = new QPushButton ( tr ( "Resize" ), this );
  duplicateModule = new QPushButton ( tr ( "Duplicate" ), this );

  layout->addWidget ( listeModules,0,0,1,2 );
  layout->addWidget ( t_name,1,0 );
  layout->addWidget ( nameModule,1,1 );
  layout->addWidget ( resizeModule,2,0);
  layout->addWidget ( duplicateModule,2,1);
  layout->addWidget ( addModule,3,0 );
  layout->addWidget ( deleteModule,3,1 );

  QString defaultModuleName ( tr ( "Default" ) );
  listeModules->addItem ( defaultModuleName );
  theModules[defaultModuleName.toStdString()] = new Module ( defaultModuleName.toStdString() );
  {
    Module* moduleDemo = theModules[defaultModuleName.toStdString()];
    Action* act = new ActionResize ( mgr,nullptr,Dimensions ( 10,10 ),true,true );
    moduleDemo->addAction ( act );
  }
  moduleChanged ( defaultModuleName );

  connect ( listeModules, QOverload<int >::of ( &QComboBox::currentIndexChanged ), this, QOverload<int >::of (&ModulesInterface::moduleChanged) );
  connect ( nameModule, &QLineEdit::editingFinished,this,&ModulesInterface::renameModule );
  connect ( addModule, &QPushButton::clicked,this, &ModulesInterface::doAddModule );
  connect ( deleteModule, &QPushButton::clicked,this, &ModulesInterface::doRemoveModule );
  connect ( resizeModule,&QPushButton::clicked,this,&ModulesInterface::doResizeModule );
  connect ( duplicateModule,&QPushButton::clicked,this,&ModulesInterface::doDuplicateModule);
}

Module * ModulesInterface::module()
{
  std::string mod=listeModules->currentText().toStdString();
  Q_ASSERT ( theModules[mod]!=nullptr );
  return theModules[mod];
}

std::vector<Module *> ModulesInterface::listOfModules()
{
  std::vector<Module *> res;
  for(auto m:theModules)
    if(m.second) res.push_back(m.second);
  return res;
}

void ModulesInterface::resetWith(std::vector<Module *> mods)
{
  listeModules->blockSignals(true);
  Q_ASSERT(!mods.empty());
  for(auto oldm:theModules)if(oldm.second)delete oldm.second;
  theModules.clear();
  listeModules->clear();
  for(Module* m:mods)
  {
    theModules[m->getName()]=m;
    listeModules->addItem(QString::fromStdString(m->getName()));
  }
  listeModules->setCurrentIndex(0);
  moduleChanged(listeModules->currentText());
  listeModules->blockSignals(false);
}

void ModulesInterface::clear()
{
  listeModules->blockSignals(true);
  for(auto oldm:theModules)if(oldm.second)delete oldm.second;
  theModules.clear();
  listeModules->clear();
  QString defaultModuleName ( tr ( "Default" ) );
  listeModules->addItem ( defaultModuleName );
  theModules[defaultModuleName.toStdString()] = new Module ( defaultModuleName.toStdString() );
  {
    Module* moduleDemo = theModules[defaultModuleName.toStdString()];
    Action* act = new ActionResize ( mgr,nullptr,Dimensions ( 10,10 ),true,true );
    moduleDemo->addAction ( act );
  }
  moduleChanged ( defaultModuleName );
  listeModules->blockSignals(false);
}

void ModulesInterface::moduleChanged ( int idx )
{
    moduleChanged(listeModules->itemText(idx));
}
void ModulesInterface::moduleChanged ( QString text )
{
  std::string mod=text.toStdString();
  nameModule->setText ( text );
  Q_ASSERT ( theModules[text.toStdString()]!=nullptr );
  emit moduleSelected ( theModules[text.toStdString()] );
}

void ModulesInterface::renameModule()
{
  QString text=nameModule->text();
  if ( listeModules->currentText().compare ( text,Qt::CaseInsensitive ) ==0 )
    {
      //self change
      std::string modrename=text.toStdString();
      std::string mod=listeModules->currentText().toStdString();
      if ( mod==modrename ) return;
      if ( theModules[mod]!=nullptr )
        {
          Q_ASSERT ( theModules[modrename]==nullptr );
          theModules[modrename]=theModules[mod];
          theModules[modrename]->rename ( modrename );
          theModules[mod]=nullptr;
        }
      listeModules->setItemText ( listeModules->currentIndex(),text );
    }
  else if ( listeModules->findText ( text,Qt::MatchFixedString ) >=0 )
    {
      //already existing
      QMessageBox::warning ( this, tr ( "Error" ), tr ( "The module name %1 already exist" ).arg ( text ),QMessageBox::Ok );
      nameModule->setText ( listeModules->currentText() );
    }
  else
    {
      //Fine
      std::string modrename=text.toStdString();
      std::string mod=listeModules->currentText().toStdString();
      if ( theModules[mod]!=nullptr )
        {
          Q_ASSERT ( theModules[modrename]==nullptr );
          theModules[modrename]=theModules[mod];
          theModules[modrename]->rename ( modrename );
          theModules[mod]=nullptr;
        }
      listeModules->setItemText ( listeModules->currentIndex(),text );
    }
}

void ModulesInterface::doRemoveModule()
{
  if ( listeModules->count() <=1 )
    {
      QMessageBox::warning ( this, tr ( "Impossible" ),tr ( "There is only one module left, you can't remove it" ),QMessageBox::Ok );
      return ;
    }
  QString mod = listeModules->currentText();
  if ( QMessageBox::warning ( this, tr ( "Definitiv" ),tr ( "Are you sure you want to dele %1 forever ?" ).arg ( mod ),QMessageBox::Yes|QMessageBox::No ) ==QMessageBox::No ) return ;
  listeModules->removeItem ( listeModules->currentIndex() );
  if ( theModules[mod.toStdString()]!=nullptr )
    {
      delete theModules[mod.toStdString()];
      theModules[mod.toStdString()]=nullptr;
    }
}

void ModulesInterface::doAddModule()
{
  NewModuleDlg* dlg = new NewModuleDlg ( this );
  if ( dlg->exec() ==QDialog::Accepted )
    {
      QString newMod = dlg->getName();
      if ( newMod.isEmpty() ) return;
      if ( listeModules->findText ( newMod,Qt::MatchFixedString ) >=0 )
        QMessageBox::warning ( this, tr ( "Error" ), tr ( "The module name %1 already exist" ).arg ( newMod ),QMessageBox::Ok );
      else
        {
          listeModules->addItem ( newMod );
          theModules[newMod.toStdString()]=new Module ( newMod.toStdString() );
          theModules[newMod.toStdString()]->addAction ( new ActionResize ( mgr,nullptr,dlg->getSize(),true,true ) );
        }
    }
}

void ModulesInterface::doDuplicateModule()
{
  bool ok;
  QString text = QInputDialog::getText(this, tr("Name of duplicate"),
                                       tr("Name:"), QLineEdit::Normal,"", &ok);
  if (ok && !text.isEmpty())
  {    
    if ( listeModules->findText ( text,Qt::MatchFixedString ) >=0 )
        QMessageBox::warning ( this, tr ( "Error" ), tr ( "The module name %1 already exist" ).arg ( text ),QMessageBox::Ok );
    else
    {
      theModules[text.toStdString()]=new Module ( *module() );
      theModules[text.toStdString()]->rename(text.toStdString());
      listeModules->addItem(text);
    }
  }
}

void ModulesInterface::doResizeModule()
{
  std::string mod=listeModules->currentText().toStdString();
  Q_ASSERT ( theModules[mod]!=nullptr );
  Motif m = factory->computeMotif(theModules[mod]->getLastAction()->ID());
  
  ModuleResizeDlg* dlg = new ModuleResizeDlg(m.size(),this);
  if ( dlg->exec() ==QDialog::Accepted )
  {
    Dimensions ori=m.size();
    Dimensions lt=dlg->getLeftTop();
    Dimensions rb=dlg->getRightBottom();
    if(ori.X()+lt.X()+rb.X()<1)
    {
      QMessageBox::warning ( this, tr ( "Error" ), tr ( "The new width %1 is less than zero" ).arg ( ori.X()+lt.X()+rb.X() ),QMessageBox::Ok );
      return;
    }
    if(ori.Y()+lt.Y()+rb.Y()<1)
    {
      QMessageBox::warning ( this, tr ( "Error" ), tr ( "The new height %1 is less than zero" ).arg ( ori.Y()+lt.Y()+rb.Y() ),QMessageBox::Ok );
      return;
    }
    if(lt!=Dimensions(0,0))
    {
      theModules[mod]->addAction(new ActionResize(mgr,theModules[mod]->getLastAction(),lt+ori,false,false));
    }
    if(rb!=Dimensions(0,0))
    {
      theModules[mod]->addAction(new ActionResize(mgr,theModules[mod]->getLastAction(),rb+lt+ori,true,true));
    }
    emit pleaseUpdateEditor();
  }
}
