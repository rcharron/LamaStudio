#ifndef GRILLEEDITORTOOLNONE_H
#define GRILLEEDITORTOOLNONE_H

#include "GrilleEditorTool.hpp"



class GrilleEditorToolNone : public GrilleEditorTool
{
public:
  virtual void selectStyle ( int style );
  virtual Action* toAction(Action* prev);
  virtual std::vector< std::pair< Dimensions, int > > mask();
  virtual std::vector<std::pair<Dimensions, int> > toolOverviewmask(Dimensions cell);
  virtual void cursorEnter ( Dimensions cell );
  virtual void startAt ( Dimensions cell );
  virtual void clear();

};

#endif // GRILLEEDITORTOOLNONE_H
