#include "RemapStyleDlg.hpp"
#include "SymbolsDrawer.hpp"
#include <QGridLayout>
#include <QColor>
#include <QLabel>
#include <QIcon>


RemapStyleDlg::RemapStyleDlg(std::map<int, Style> stys, QWidget* parent):
  QDialog(parent)
{
  SymbolsDrawer drawer;
  drawer.setSize(QSize(20, 20));
  for(auto st:stys)
  {
	drawer.setBackgroundColor(QColor::fromRgb ( st.second.backgroundColor.r,st.second.backgroundColor.g,st.second.backgroundColor.b ));
	drawer.setColor(QColor::fromRgb(st.second.foregroundColor.r, st.second.foregroundColor.g, st.second.foregroundColor.b));
	QPixmap px = drawer.draw(static_cast<SymbolsDrawer::Symbol>(st.second.symbol));
    styles[st.first] = px;
  }
  QGridLayout* layout = new QGridLayout(this);
  buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok
                                     | QDialogButtonBox::Cancel);
  
  
  int line=0;
  for(auto st:stys)
  {
    QLabel* labeicon = new QLabel(this);
    labeicon->setPixmap(styles[st.first]);
    layout->addWidget(labeicon,line,0);
    QLabel* maps = new QLabel("->",this);
    layout->addWidget(maps,line,1);
    QComboBox* cbox = new QComboBox(this);
    for(auto stpx:styles)
    {
      cbox->addItem(QIcon(stpx.second),"",QVariant(stpx.first));
    }
    listes[st.first]=cbox;
    layout->addWidget(cbox,line,2);
	cbox->setCurrentIndex(line);
    line++;
  }
  
  layout->addWidget(buttonBox,line,0,1,3);
  
  connect(buttonBox, &QDialogButtonBox::accepted, this, &QDialog::accept);
  connect(buttonBox, &QDialogButtonBox::rejected, this, &QDialog::reject);
}

std::map<int, int> RemapStyleDlg::get()
{
  std::map<int, int> res;
  for(auto st:styles)
  {
    res[st.first]=listes[st.first]->currentData().toInt();
  }
  return res;
}
