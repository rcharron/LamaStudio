#ifndef MODULERESIZEDLG_H
#define MODULERESIZEDLG_H

#include <QDialog>
#include <dimensions.hpp>
#include <QLineEdit>
#include <QDialogButtonBox>
#include <QIntValidator>

class ModuleResizeDlg : public QDialog
{
  Q_OBJECT
public:
  ModuleResizeDlg(Dimensions orSize, QWidget* parent = nullptr);
  Dimensions getRightBottom();
  Dimensions getLeftTop();
private slots:
  void changeRight(QString t);
  void changeLeft(QString t);
  void changeTop(QString t);
  void changeBottom(QString t);
private:
  Dimensions orSize;
  QLineEdit* right;
  QLineEdit* left;
  QLineEdit* top;
  QLineEdit* bottom;
  QIntValidator* rightv;
  QIntValidator* leftv;
  QIntValidator* topv;
  QIntValidator* bottomv;
  QDialogButtonBox* buttonBox;
  int addRight = 0;
  int addLeft = 0;
  int addTop = 0;
  int addBottom = 0;
};

#endif // MODULERESIZEDLG_H
