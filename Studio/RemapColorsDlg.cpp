#include "RemapColorsDlg.hpp"
#include <QGridLayout>
#include <QLabel>

RemapColorsDlg::RemapColorsDlg(QVector<QPair<Style::Color,Style::Color>> colors,
                               QMap<QPair<int,int>,int> grid, QWidget* parent):
    QDialog(parent),
    grid(grid)
{
    SymbolsDrawer drawer;
    drawer.setPenWidth(2);
    drawer.setSize(QSize(30, 30));
    int t=colors.size();
    for(int i=0; i<t; i++)
    {
        auto cbg=colors.at(i).first;
        auto csy=colors.at(i).second;
        drawer.setBackgroundColor(QColor::fromRgb ( cbg.r, cbg.g, cbg.b ));
        drawer.setColor(QColor::fromRgb(csy.r, csy.g, csy.b));
        QPixmap px = drawer.draw(SymbolsDrawer::Symbol::bobble);
        colorPx[i] = px;
    }
    QGridLayout* layout = new QGridLayout(this);
    buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok
                                     | QDialogButtonBox::Cancel);


    int line=0;
    for(int i=0; i<t; i++)
    {
        QLabel* labeicon = new QLabel(this);
        labeicon->setPixmap(colorPx[i]);
        layout->addWidget(labeicon,line,0);
        QLabel* maps = new QLabel("->",this);
        layout->addWidget(maps,line,1);
        QComboBox* cbox = new QComboBox(this);
        for(int j=0; j<t; j++)
        {
            cbox->addItem(QIcon(colorPx[j]),"",QVariant(j));
        }
        listes[i]=cbox;
        layout->addWidget(cbox,line,2);
        cbox->setCurrentIndex(line);
        line++;
    }

    layout->addWidget(buttonBox,line,0,1,3);

    connect(buttonBox, &QDialogButtonBox::accepted, this, &QDialog::accept);
    connect(buttonBox, &QDialogButtonBox::rejected, this, &QDialog::reject);
}

std::map<int, int> RemapColorsDlg::get()
{
  std::map<int, int> res;
  std::map<int, int> mapC;
  int t=listes.size();
  for(int i=0; i<t; i++)
  {
      mapC[i]=listes[i]->currentData().toInt();
  }
  QMap<QPair<int,int>,int>::iterator it;
  for(it=grid.begin();it!=grid.end();++it)
  {
      int old=it.value();
      QPair<int,int > c=it.key();
      c.second=mapC[c.second];
      QMap<QPair<int,int>,int>::const_iterator fit=grid.find(c);
      if(fit==grid.end())continue;
      int newst=fit.value();
      if(newst==0)continue;
      res[old]=newst;
  }
  return res;
}
