#include <iostream>
#include <QApplication>
#include <QPushButton>
#include <QString>
#include "Studio.hpp"

int main ( int argc, char **argv )
{
  QApplication app ( argc, argv );
  QCoreApplication::setOrganizationName ( "The lama corporation" );
  QCoreApplication::setApplicationName ( "Lama studio" );
  QCoreApplication::setApplicationVersion ( "0.1" );
  Studio mainWin;
  //Workspace mainWin;
  mainWin.show();
  return app.exec();
}
