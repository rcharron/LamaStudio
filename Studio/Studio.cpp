#include "Studio.hpp"
#include <QHBoxLayout>
#include <QAction>
#include <QMenuBar>
#include <QToolBar>
#include <QMessageBox>
#include <QFileDialog>
#include <QFile>
#include <QJsonDocument>
#include <IO/LamaFile.hpp>
#include <IO/ScriptFile.hpp>
#include <Actions/ActionRemapStyle.hpp>
#include <Actions/ActionIsoTransform.hpp>
#include "ChooseMotifDlg.hpp"
#include "GrilleEditorToolStamp.hpp"
#include "RemapStyleDlg.hpp"
#include "RemapColorsDlg.hpp"

Studio::Studio() :
  QMainWindow(),
  actionMng ( new ActionMng() ),
  motifFactory ( new Factory ( actionMng ) )
{
  createActions();
  createMenus();

  scene = new GrilleEditor ( actionMng, motifFactory, this );
  createToolbars();

  setCentralWidget ( scene );

  createDockWidgets();

  setWindowIcon ( QIcon ( ":/images/icone.png" ) );

}

Studio::~Studio()
{
  delete motifFactory;
  delete actionMng;
}

QSize Studio::minimumSizeHint() const
{
  return QSize ( 500,300 );
}

void Studio::openFile()
{
  QString fileNameDlg = QFileDialog::getOpenFileName ( this,
                        tr ( "Save Knit chart" ), "",
                        tr ( "LamaScript (*.lua);;Lama diagramm (*.Lama)" ) );
  if ( fileNameDlg.isNull() ) return;
  filename = fileNameDlg;
  
  if(filename.endsWith(".Lama"))
  {
    QFile loadFile ( filename );

    if ( !loadFile.open ( QIODevice::ReadOnly ) )
      {
        QMessageBox::critical ( nullptr, tr ( "File reading error" ), tr ( "The file %1 couldn't be read" ).arg ( filename ) );
        filename = QString();
        return;
      }
    loadFile.close();

    LamaFile file ( actionMng, motifFactory );
    file.load ( filename.toStdString() );
    auto mods=file.getModules();
    modulesWidget->resetWith ( mods );
    gridStyleWidget->setStyles ( file.getStyles() );
    scene->setStyles ( file.getStyles() );
  }
  else if(filename.endsWith(".lua"))
  {
    ScriptFile file ( actionMng, motifFactory );
    if(!file.load ( filename.toStdString() )){
      QMessageBox::critical ( nullptr, tr ( "File error" ), tr ( "The file %1 couldn't be read or contains errors" ).arg ( filename ) );
        filename = QString();
        newFile();
        return;
    }
    auto mods=file.getModules();
    modulesWidget->resetWith ( mods );
    gridStyleWidget->setStyles ( file.getStyles() );
    scene->setStyles ( file.getStyles() );
  }
  update();
}

void Studio::newFile()
{
  filename.clear();
  actionMng->clear();
  modulesWidget->clear();
  gridStyleWidget->clear();
  motifFactory->clear();
  update();
}

void Studio::saveFile()
{
  if ( filename.isNull() )
    saveFileAs();
  QFile saveFile ( filename );
  if ( !saveFile.open ( QIODevice::WriteOnly ) )
    {
      QMessageBox::critical ( nullptr, tr ( "File writing error" ), tr ( "The file couldn't be written" ) );
      filename = QString();
      return;
    }
  saveFile.close();
  if(filename.endsWith(".Lama"))
  {
    LamaFile file ( actionMng, motifFactory );
    file.setModules ( modulesWidget->listOfModules() );
    file.setStyles ( gridStyleWidget->getStyles() );
    file.save ( filename.toStdString() );
  }
  else if(filename.endsWith(".lua"))
  {
    ScriptFile file ( actionMng,motifFactory );
    file.setModules ( modulesWidget->listOfModules() );
    file.setStyles ( gridStyleWidget->getStyles() );
    file.save ( filename.toStdString() );
  }
}

void Studio::saveFileAs()
{
  QString fileNameDlg = QFileDialog::getSaveFileName ( this,
                        tr ( "Save Knit chart" ), "",
                        tr ( "LamaScript (*.lua);;Lama diagramm (*.Lama)" ) );
  if ( fileNameDlg.isNull() ) return;
  filename = fileNameDlg;
  if ( !filename.endsWith ( ".Lama" ) && !filename.endsWith(".lua") ) filename+=".lua";
  saveFile();
}

void Studio::exportChart()
{
	QString fileNameDlg = QFileDialog::getSaveFileName(this,
		tr("Export chart"), "",
		tr("Image jpeg (*.jpg);;Image SVG (*.svg)"));
	if (fileNameDlg.isNull()) return;
  if(fileNameDlg.endsWith(".svg"))
  {
    scene->drawSVG(fileNameDlg);
  }
  else
  {
    QPixmap pix = scene->drawOnPixmap();
    pix.save(fileNameDlg,"JPG",100);
  }
}



void Studio::about()
{
  QMessageBox::about ( this, tr ( "About Lama" ),
                       tr ( "The <b>Lama</b> application "
                            "is intended to make knitting charts.<br />"
                            "Lama use icons from icons8.com under CC BY-ND 3.0 licence" ) );
}

void Studio::uncheckAllToolActions()
{
  toolSelectAction->setChecked ( false );
  toolPenAction->setChecked ( false );
  toolRectAction->setChecked ( false );
  toolFillAction->setChecked ( false );
  toolStampAction->setChecked ( false );
}

void Studio::toolSelect()
{
  scene->selectTool ( GrilleEditor::Tool::None );
  uncheckAllToolActions();
  toolSelectAction->setChecked ( true );
}

void Studio::toolPen()
{
  scene->selectTool ( GrilleEditor::Tool::Pen );
  uncheckAllToolActions();
  toolPenAction->setChecked ( true );
}

void Studio::toolRect()
{
  scene->selectTool ( GrilleEditor::Tool::Rect );
  uncheckAllToolActions();
  toolRectAction->setChecked ( true );
}

void Studio::toolFill()
{
  scene->selectTool ( GrilleEditor::Tool::Fill );
  uncheckAllToolActions();
  toolFillAction->setChecked ( true );
}

void Studio::toolStamp()
{
  uncheckAllToolActions();
  ChooseMotifDlg *dlg = new ChooseMotifDlg ( this );
  std::vector<Module *> mods = modulesWidget->listOfModules();
  for ( auto m:mods )
    dlg->addMotif ( QString::fromStdString ( m->getName() ) );
  if ( dlg->exec() == QDialog::Accepted )
    {
      Action *act = nullptr;
      std::string mod = dlg->getMotif().toStdString();
      for ( auto m:mods )
        {
          if ( m->getName() ==mod )
            {
              act = m->getLastAction();
            }
        }
      scene->selectStampTool ( new GrilleEditorToolStamp ( actionMng,act,motifFactory ) );
      toolStampAction->setChecked ( true );
    }
  else
    {
      scene->selectTool(GrilleEditor::Tool::None);
    }
}

void Studio::toolRemapStyles()
{
  RemapStyleDlg* dlg = new RemapStyleDlg(gridStyleWidget->getStyles(),this);
  if ( dlg->exec() == QDialog::Accepted )
  {
    modulesWidget->module()->addAction(new ActionRemapStyle(actionMng,modulesWidget->module()->getLastAction(),dlg->get()));
    scene->update();
  }
}

void Studio::toolRemapColors()
{
    RemapColorsDlg* dlg = new RemapColorsDlg(gridStyleWidget->getColors(), gridStyleWidget->getStylesGrid(), this);
    if ( dlg->exec() == QDialog::Accepted )
    {
      modulesWidget->module()->addAction(new ActionRemapStyle(actionMng,modulesWidget->module()->getLastAction(),dlg->get()));
      scene->update();
    }
}

void Studio::toolMirrorV()
{
    modulesWidget->module()->addAction(new ActionIsoTransform(actionMng,modulesWidget->module()->getLastAction(),ActionIsoTransform::Type::SymV));
    scene->update();
}

void Studio::toolMirrorH()
{
    modulesWidget->module()->addAction(new ActionIsoTransform(actionMng,modulesWidget->module()->getLastAction(),ActionIsoTransform::Type::SymH));
    scene->update();
}

void Studio::toolRotateLeft()
{
    modulesWidget->module()->addAction(new ActionIsoTransform(actionMng,modulesWidget->module()->getLastAction(),ActionIsoTransform::Type::RotateLeft));
    scene->update();
}

void Studio::toolRotateRight()
{
    modulesWidget->module()->addAction(new ActionIsoTransform(actionMng,modulesWidget->module()->getLastAction(),ActionIsoTransform::Type::RotateRight));
    scene->update();
}

void Studio::updateStyle ( int sty )
{
  scene->addStyle ( sty, gridStyleWidget->get ( sty ) );
  scene->update();
}

void Studio::undo()
{
  modulesWidget->module()->undo();
  scene->update();
}

void Studio::redo()
{
  modulesWidget->module()->redo();
  scene->update();
}

void Studio::createActions()
{
  //Files Actions

  openFileAction = new QAction ( QIcon ( ":/images/open.png" ),
                                 tr ( "&Open file" ), this );
  openFileAction->setShortcut ( tr ( "Ctrl+O" ) );
  openFileAction->setStatusTip ( tr ( "Open a file" ) );
  connect ( openFileAction, SIGNAL ( triggered() ), this, SLOT ( openFile() ) );

  newFileAction = new QAction ( QIcon ( ":/images/new.png" ), tr ( "&New file" ), this );
  newFileAction->setShortcut ( tr ( "Ctrl+N" ) );
  newFileAction->setStatusTip ( tr ( "Create a new file" ) );
  connect ( newFileAction, SIGNAL ( triggered() ), this, SLOT ( newFile() ) );

  saveFileAction = new QAction ( QIcon ( ":/images/save.png" ), tr ( "&Save file" ), this );
  saveFileAction->setShortcut ( tr ( "Ctrl+S" ) );
  saveFileAction->setStatusTip ( tr ( "Save a file" ) );
  connect ( saveFileAction, SIGNAL ( triggered() ), this, SLOT ( saveFile() ) );

  saveFileAsAction = new QAction ( QIcon ( ":/images/save-as.png" ), tr ( "&Save file as..." ), this );
  saveFileAsAction->setShortcut ( tr ( "Ctrl+Maj+S" ) );
  saveFileAsAction->setStatusTip ( tr ( "Save a file as ..." ) );
  connect ( saveFileAsAction, SIGNAL ( triggered() ), this, SLOT ( saveFileAs() ) );

  exportChartAction = new QAction(QIcon(":/images/grid.png"), tr("&Export chart"), this);
  exportChartAction->setShortcut(tr("Ctrl+Maj+E"));
  exportChartAction->setStatusTip(tr("Export chart in image format"));
  connect( exportChartAction, SIGNAL( triggered() ), this, SLOT( exportChart() ) );

  //Tools actions

  toolSelectAction = new QAction ( QIcon ( ":/images/cursor.png" ), tr ( "&cursor" ), this );
  toolSelectAction->setShortcut ( tr ( "C" ) );
  toolSelectAction->setStatusTip ( tr ( "Navigate safely in the grid" ) );
  toolSelectAction->setCheckable ( true );
  connect ( toolSelectAction, &QAction::triggered,this,&Studio::toolSelect );

  toolPenAction = new QAction ( QIcon ( ":/images/pen.png" ), tr ( "&pen" ), this );
  toolPenAction->setShortcut ( tr ( "P" ) );
  toolPenAction->setStatusTip ( tr ( "Draw on the grid" ) );
  toolPenAction->setCheckable ( true );
  connect ( toolPenAction, &QAction::triggered,this,&Studio::toolPen );

  toolRectAction = new QAction ( QIcon ( ":/images/rectangular.png" ), tr ( "&rectangle" ), this );
  toolRectAction->setShortcut ( tr ( "R" ) );
  toolRectAction->setStatusTip ( tr ( "Draw a rectangle on the grid" ) );
  toolRectAction->setCheckable ( true );
  connect ( toolRectAction, &QAction::triggered,this,&Studio::toolRect );

  toolFillAction = new QAction ( QIcon ( ":/images/fill.png" ), tr ( "&fill" ), this );
  toolFillAction->setShortcut ( tr ( "F" ) );
  toolFillAction->setStatusTip ( tr ( "Fill a color zone" ) );
  toolFillAction->setCheckable ( true );
  connect ( toolFillAction, &QAction::triggered,this,&Studio::toolFill );

  toolStampAction = new QAction ( QIcon ( ":/images/stamp.png" ), tr ( "&stamp" ), this );
  toolStampAction->setShortcut ( tr ( "S" ) );
  toolStampAction->setStatusTip ( tr ( "Stamp a motif" ) );
  toolStampAction->setCheckable ( true );
  connect ( toolStampAction, &QAction::triggered,this,&Studio::toolStamp );
  
  toolRemapStylesAction = new QAction ( QIcon ( ":/images/palette.png" ), tr ( "remap styles" ), this );
  toolRemapStylesAction->setStatusTip ( tr ( "Remap the styles" ) );
  connect ( toolRemapStylesAction, &QAction::triggered,this,&Studio::toolRemapStyles );

  toolRemapColorsAction = new QAction ( QIcon ( ":/images/recolor.png" ), tr ( "remap styles" ), this );
  toolRemapColorsAction->setStatusTip ( tr ( "Remap the colors" ) );
  connect ( toolRemapColorsAction, &QAction::triggered,this,&Studio::toolRemapColors );

  toolMirrorHAction = new QAction ( QIcon ( ":/images/mirror-h.png" ), tr ( "mirror horizontal" ), this );
  toolMirrorHAction->setStatusTip ( tr ( "Horizontal mirror image" ) );
  connect ( toolMirrorHAction, &QAction::triggered,this,&Studio::toolMirrorH );

  toolMirrorVAction = new QAction ( QIcon ( ":/images/mirror-v.png" ), tr ( "mirror vertical" ), this );
  toolMirrorVAction->setStatusTip ( tr ( "Vertical mirror image" ) );
  connect ( toolMirrorVAction, &QAction::triggered,this,&Studio::toolMirrorV );

  toolRotateLeftAction = new QAction ( QIcon ( ":/images/rotate-left.png" ), tr ( "rotate left" ), this );
  toolRotateLeftAction->setStatusTip ( tr ( "Rotate image left" ) );
  connect ( toolRotateLeftAction, &QAction::triggered,this,&Studio::toolRotateLeft );

  toolRotateRightAction = new QAction ( QIcon ( ":/images/rotate-right.png" ), tr ( "rotate right" ), this );
  toolRotateRightAction->setStatusTip ( tr ( "Rotate right left" ) );
  connect ( toolRotateRightAction, &QAction::triggered,this,&Studio::toolRotateRight );

  //History actions
  undoAction = new QAction ( QIcon ( ":/images/undo.png" ), tr ( "&undo" ), this );
  undoAction->setShortcut ( tr ( "Ctrl+Z" ) );
  undoAction->setStatusTip ( tr ( "Undo" ) );
  connect ( undoAction, &QAction::triggered,this,&Studio::undo );

  redoAction = new QAction ( QIcon ( ":/images/redo.png" ), tr ( "&redo" ), this );
  redoAction->setShortcut ( tr ( "Ctrl+Maj+Z" ) );
  redoAction->setStatusTip ( tr ( "Redo" ) );
  connect ( redoAction, &QAction::triggered,this,&Studio::redo );

  //About actions

  aboutAction = new QAction ( QIcon ( ":/images/icone.png" ), tr ( "&About" ), this );
  aboutAction->setStatusTip ( tr ( "Show application informations" ) );
  connect ( aboutAction, SIGNAL ( triggered() ), this, SLOT ( about() ) );
}

void Studio::createMenus()
{
  fileMenu = menuBar()->addMenu ( tr ( "&File" ) );
  fileMenu->addAction ( newFileAction );
  fileMenu->addAction ( openFileAction );
  fileMenu->addAction ( saveFileAction );
  fileMenu->addAction ( saveFileAsAction );
  fileMenu->addSeparator();
  fileMenu->addAction ( exportChartAction );
  fileMenu->addSeparator();

  historyMenu = menuBar()->addMenu ( tr ( "&History" ) );
  historyMenu->addAction ( undoAction );
  historyMenu->addAction ( redoAction );

  toolMenu = menuBar()->addMenu ( tr ( "&Tools" ) );
  toolMenu->addAction ( toolSelectAction );
  toolMenu->addAction ( toolPenAction );
  toolMenu->addAction ( toolRectAction );
  toolMenu->addAction ( toolFillAction );
  toolMenu->addAction ( toolStampAction );
  toolMenu->addAction ( toolRemapStylesAction );
  toolMenu->addAction ( toolRemapColorsAction );
  toolMenu->addSeparator();
  toolMenu->addAction( toolMirrorHAction );
  toolMenu->addAction( toolMirrorVAction );
  toolMenu->addAction( toolRotateLeftAction );
  toolMenu->addAction( toolRotateRightAction );

  aboutMenu = menuBar()->addMenu ( tr ( "&Help" ) );
  aboutMenu->addAction ( aboutAction );
}

void Studio::createToolbars()
{
  fileToolBar = addToolBar ( tr ( "File" ) );
  fileToolBar->addAction ( newFileAction );
  fileToolBar->addAction ( openFileAction );
  fileToolBar->addAction ( saveFileAction );
  fileToolBar->addAction ( saveFileAsAction );

  historyBar = addToolBar ( tr ( "History" ) );
  historyBar->addAction ( undoAction );
  historyBar->addAction ( redoAction );

  toolToolBar = addToolBar ( tr ( "Tools" ) );
  toolToolBar->addAction ( toolSelectAction );
  toolToolBar->addAction ( toolPenAction );
  toolToolBar->addAction ( toolRectAction );
  toolToolBar->addAction ( toolFillAction );
  toolToolBar->addAction ( toolStampAction );
  toolToolBar->addAction ( toolRemapStylesAction );
  toolToolBar->addAction ( toolRemapColorsAction );
  toolToolBar->addSeparator();
  toolToolBar->addAction( toolMirrorHAction );
  toolToolBar->addAction( toolMirrorVAction );
  toolToolBar->addAction( toolRotateLeftAction );
  toolToolBar->addAction( toolRotateRightAction );
}

void Studio::createDockWidgets()
{
  gridStyleDockWidget = new QDockWidget ( tr ( "Styles" ), this );
  gridStyleDockWidget->setAllowedAreas ( Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea | Qt::BottomDockWidgetArea | Qt::TopDockWidgetArea );
  gridStyleWidget = new GridStyle ( gridStyleDockWidget );
  gridStyleDockWidget->setWidget ( gridStyleWidget );
  addDockWidget ( Qt::LeftDockWidgetArea, gridStyleDockWidget );
  fileMenu->addAction ( gridStyleDockWidget->toggleViewAction() );

  connect ( gridStyleWidget, &GridStyle::styleChanged, this, &Studio::updateStyle );
  connect ( gridStyleWidget, &GridStyle::styleSelected, scene, &GrilleEditor::selecteStyle );


  modulesDockWidget = new QDockWidget ( tr ( "Modules" ), this );
  modulesDockWidget->setAllowedAreas ( Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea | Qt::BottomDockWidgetArea | Qt::TopDockWidgetArea );
  modulesWidget = new ModulesInterface ( actionMng, motifFactory, modulesDockWidget );
  modulesDockWidget->setWidget ( modulesWidget );
  addDockWidget ( Qt::LeftDockWidgetArea, modulesDockWidget );
  fileMenu->addAction ( modulesDockWidget->toggleViewAction() );
  connect ( modulesWidget, &ModulesInterface::moduleSelected, scene, &Grille::setModule );
  connect ( modulesWidget, &ModulesInterface::pleaseUpdateEditor, [&]()
  {
    scene->update();
  } );
  scene->setModule ( modulesWidget->module() );
}
