#ifndef GRILLEEDITORTOOL_H
#define GRILLEEDITORTOOL_H

#include <dimensions.hpp>
#include <vector>
#include <Actions/Action.hpp>

class GrilleEditorTool
{
public:
  virtual void clear() = 0;
  virtual void startAt(Dimensions cell) = 0;
  virtual void cursorEnter(Dimensions cell) = 0;
  virtual std::vector<std::pair<Dimensions, int> > mask() = 0;
  virtual std::vector<std::pair<Dimensions, int> > toolOverviewmask(Dimensions cell) = 0;
  virtual Action* toAction(Action* prev) = 0;
  virtual void selectStyle(int style) = 0;
protected:
};

#endif // GRILLEEDITORTOOL_H
