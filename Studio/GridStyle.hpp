#ifndef GRIDSTYLE_HPP
#define GRIDSTYLE_HPP

#include <QWidget>
#include <QScrollArea>
#include <QGridLayout>
#include <QToolButton>
#include <QVector>
#include <QMap>
#include <QLabel>
#include <QPushButton>
#include <QComboBox>
#include <QGridLayout>
#include <map>
#include <Style.hpp>

class GridStyle : public QWidget
{
    Q_OBJECT
public:
    explicit GridStyle(QWidget *parent = nullptr);
    Style get(int styleId);
    void set(int styleId, Style style);
    int getSelected();
    void setStyles(std::map<int, Style>);
    std::map<int, Style> getStyles();
    void clear();
    QMap<QPair<int,int>,int> getStylesGrid();
    QVector<QPair<Style::Color,Style::Color>> getColors();
protected slots:
  void clickAddRow();
  void clickAddCol();
  void clickChangeRow(int index);
  void clickChangeColBgC(int index);
  void clickChangeColSyC(int index);
  void select(int row, int col);
Q_SIGNALS:
  void styleChanged(int);
  void styleSelected(int);
private:
  QGridLayout* grid;
  int rowOf(Style sty);
  int columnOf(Style sty,int extraOffset=0);
  void addRow(int symbol, bool addSty=true);
  void addColumn(Style::Color backgroundColor, Style::Color symbColor= Style::Color(0,0,0), bool addSty=true);
  QVector<int> rows;
  QVector<QPair<Style::Color,Style::Color>> columns;
  QVector<QPushButton*> rowBtn;
  QVector<QPushButton*> columnBgCBtn;
  QVector<QPushButton*> columnSyCBtn;
  QMap<QPair<int,int>,QPushButton*> stylesSamples;
  QMap<QPair<int,int>,int> stylesID;
  QMap<int,QPair<int,int>> IDToStyle;
  QPushButton* addRowBtn;
  QPushButton* addColBtn;
  QPushButton* noStyleBtn;

  QPair<int,int> lastSelected;
  int selectedID;
  int counterID;
};

#endif // GRIDSTYLE_HPP
