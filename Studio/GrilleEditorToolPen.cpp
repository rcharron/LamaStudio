#include "GrilleEditorToolPen.hpp"
#include <Actions/ActionPen.hpp>

GrilleEditorToolPen::GrilleEditorToolPen ( ActionMng* mgr,int style ) :
  mgr ( mgr ),
  style ( style )
{
}

void GrilleEditorToolPen::selectStyle ( int style )
{
  this->style=style;
}

Action* GrilleEditorToolPen::toAction ( Action* prev )
{
  if ( path.empty() ) return nullptr;
  std::vector< Dimensions> apath;
  for ( auto p:path )
    apath.push_back ( p.first );
  return new ActionPen ( mgr, prev, apath,style );
}

std::vector< std::pair< Dimensions, int > > GrilleEditorToolPen::mask()
{
  return path;
}

std::vector<std::pair<Dimensions, int> > GrilleEditorToolPen::toolOverviewmask(Dimensions cell)
{
  std::vector<std::pair<Dimensions, int> > res;
  res.push_back(std::pair<Dimensions, int>(cell, style));
  return res;
}


void GrilleEditorToolPen::cursorEnter ( Dimensions cell )
{
  path.push_back ( std::pair<Dimensions,int> ( cell, style ) );
}

void GrilleEditorToolPen::startAt ( Dimensions cell )
{
  path.clear();
  path.push_back ( std::pair<Dimensions,int> ( cell, style ) );
}

void GrilleEditorToolPen::clear()
{
  path.clear();
}
