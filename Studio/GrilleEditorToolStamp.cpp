#include "GrilleEditorToolStamp.hpp"
#include "Actions/ActionStamp.hpp"

GrilleEditorToolStamp::GrilleEditorToolStamp(ActionMng* mgr, Action* stamp, Factory* factory):
  mgr(mgr),stamp(stamp),factory(factory)
{
  Motif m = factory->computeMotif(stamp->ID());
  Dimensions ms=m.size();
  for(int i=0;i<ms.X();i++)
    for(int j=0;j<ms.Y();j++)
      if(m(i,j)>0)maskOr.push_back(std::pair< Dimensions, int >(Dimensions(i,j),m(i,j)));
}

void GrilleEditorToolStamp::selectStyle(int style){}

Action* GrilleEditorToolStamp::toAction(Action* prev)
{
  return new ActionStamp(mgr,prev,factory,where,stamp->ID());
}

std::vector< std::pair< Dimensions, int > > GrilleEditorToolStamp::toolOverviewmask(Dimensions cell)
{
  std::vector< std::pair< Dimensions, int > > res;
  for(auto c:maskOr)
    res.push_back(std::pair< Dimensions, int >(c.first+cell,c.second));
  return res;
}

std::vector< std::pair< Dimensions, int > > GrilleEditorToolStamp::mask()
{
  std::vector< std::pair< Dimensions, int > > res;
  for(auto c:maskOr)
    res.push_back(std::pair< Dimensions, int >(c.first+where,c.second));
  return res;
}

void GrilleEditorToolStamp::cursorEnter(Dimensions cell)
{
  where = cell;
}

void GrilleEditorToolStamp::startAt(Dimensions cell)
{
  where = cell;
}

void GrilleEditorToolStamp::clear(){}
