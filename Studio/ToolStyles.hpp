#ifndef TOOL_STYLES_HPP
#define TOOL_STYLES_HPP

#include <QWidget>
#include <QScrollArea>
#include <QGridLayout>
#include <QToolButton>
#include <QVector>
#include <QLabel>
#include <QPushButton>
#include <QComboBox>
#include <map>
#include <Style.hpp>

class ToolStylesLine : public QWidget
{
  Q_OBJECT
public:
  ToolStylesLine(int id, QWidget* parent = nullptr);
  void syncFrom(Style source);
  Style getStyle();
  void setSelected(bool val);
protected:
  void mousePressEvent(QMouseEvent *event) override;
private slots:
  void changeColor();
  void changeSymbolColor();
  void changeSymbol();
private:
  QToolButton* bColor = nullptr;
  Style style;
  QLabel* tSel = nullptr;
  QPushButton* bSymb = nullptr;
  QToolButton* bsColor = nullptr;
  int id;
Q_SIGNALS:
  void styleChanged(int);
  void clicked(int);
};

class ToolStylesLineBackground : public QWidget
{
  Q_OBJECT
public:
  ToolStylesLineBackground(QWidget* parent = nullptr);;
  void setSelected(bool val);
protected:
  void mousePressEvent(QMouseEvent *event) override;
private:
  QLabel* tSel = nullptr;
Q_SIGNALS:
  void clicked(int);
};

class ToolStyles : public QScrollArea
{
  Q_OBJECT
public:
  ToolStyles(QWidget* parent = nullptr);
  Style get(int styleId);
  int numberStyles();
  void set(int styleId, Style style);
  int getSelected();
  void setStyles(std::map<int, Style>);
  std::map<int, Style> getStyles();
  void clear();
public slots:
  void select(int id);

Q_SIGNALS:
  void styleChanged(int);
  void styleSelected(int);
private:
  void addLine();
  QGridLayout* layout = nullptr;
  QVector<ToolStylesLine*> styles;
  ToolStylesLineBackground * styleDefault;
  QPushButton* bAddStyle = nullptr;
  int lastStyle = 0;
  int selection = 0;
};

#endif // !TOOL_STYLES_HPP
