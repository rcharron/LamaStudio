#include "SymbolsDrawer.hpp"
#include <QPainter>
#include <QPainterPath>

SymbolsDrawer::SymbolsDrawer():
  color(Qt::black),
  backgroundColor(Qt::white),
  size(20,20)
{
}

void SymbolsDrawer::setColor(QColor col)
{
  color = col;
}

void SymbolsDrawer::setBackgroundColor(QColor col)
{
  backgroundColor = col;
}

void SymbolsDrawer::setSize(Dimensions dim)
{
  size.setWidth(dim.X());
  size.setHeight(dim.Y());
}

void SymbolsDrawer::setSize(QSize dim)
{
  size = dim;
}

void SymbolsDrawer::setPenWidth(int width)
{
  pen.setWidth(width);
}

QPixmap SymbolsDrawer::draw(Symbol sym)
{
  switch (sym)
  {
  case Symbol::None:
    return drawNone();
  case Symbol::knit:
    return drawKnit();
  case Symbol::purl:
    return drawPurl();
  case Symbol::yarnOver:
    return drawYarnOver();
  case Symbol::k2tog:
    return drawK2tog();
  case Symbol::ssk:
    return drawSsk();
  case Symbol::k3tog:
    return drawK3tog();
  case Symbol::sk2po:
    return drawSk2po();
  case Symbol::k2skpo:
    return drawK2skpo();
    case Symbol::k2togWS:
        return drawK2tog(false);
    case Symbol::sskWS:
        return drawSsk(false);
    case Symbol::k3togWS:
        return drawK3tog(false);
    case Symbol::sk2poWS:
        return drawSk2po(false);
    case Symbol::k2skpoWS:
        return drawK2skpo(false);
  case Symbol::k1tbl:
    return drawK1tbl();
    case Symbol::k1tblWS:
        return drawK1tbl(false);
  case Symbol::bobble:
    return drawBobble();
  case Symbol::torseNeedleBackKnit:
  case Symbol::torseNeedleBackPurl:
  case Symbol::torseNeedleBackKnitTrLeft:
  case Symbol::torseNeedleBackPurlTrLeft:
  case Symbol::torseNeedleBackKnitTrRight:
  case Symbol::torseNeedleBackPurlTrRight:
  case Symbol::torseNeedleFrontKnit:
  case Symbol::torseNeedleFrontPurl:
  case Symbol::torseNeedleFrontKnitTrLeft:
  case Symbol::torseNeedleFrontPurlTrLeft:
  case Symbol::torseNeedleFrontKnitTrRight:
  case Symbol::torseNeedleFrontPurlTrRight:
    return drawTorsade(sym);
  case Symbol::torseNeedleBackKnitLeftEnd:
  case Symbol::torseNeedleBackPurlLeftEnd:
  case Symbol::torseNeedleBackKnitRightEnd:
  case Symbol::torseNeedleBackPurlRightEnd:
  case Symbol::torseNeedleFrontKnitLeftEnd:
  case Symbol::torseNeedleFrontPurlLeftEnd:
  case Symbol::torseNeedleFrontKnitRightEnd:
  case Symbol::torseNeedleFrontPurlRightEnd:
    return drawTorsadeEnd(sym);
  }
  return QPixmap();
}

void SymbolsDrawer::draw(QPainter &painter, const QPointF& offset, Symbol sym)
{
  switch (sym)
  {
  case Symbol::None:
    drawNone(painter, offset);
    break;
  case Symbol::knit:
    drawKnit(painter, offset);
    break;
  case Symbol::purl:
    drawPurl(painter, offset);
    break;
  case Symbol::yarnOver:
    drawYarnOver(painter, offset);
    break;
  case Symbol::k2tog:
    drawK2tog(painter, offset);
    break;
  case Symbol::ssk:
    drawSsk(painter, offset);
    break;
  case Symbol::k3tog:
    drawK3tog(painter, offset);
    break;
  case Symbol::sk2po:
    drawSk2po(painter, offset);
    break;
  case Symbol::k2skpo:
    drawK2skpo(painter, offset);
    break;
  case Symbol::k2togWS:
    drawK2tog(painter, offset, false);
    break;
  case Symbol::sskWS:
    drawSsk(painter, offset, false);
    break;
  case Symbol::k3togWS:
    drawK3tog(painter, offset, false);
    break;
  case Symbol::sk2poWS:
    drawSk2po(painter, offset, false);
    break;
  case Symbol::k2skpoWS:
    drawK2skpo(painter, offset, false);
    break;
  case Symbol::k1tbl:
    drawK1tbl(painter, offset);
    break;
  case Symbol::k1tblWS:
    drawK1tbl(painter, offset, false);
    break;
  case Symbol::bobble:
    drawBobble(painter, offset);
    break;
  case Symbol::torseNeedleBackKnit:
  case Symbol::torseNeedleBackPurl:
  case Symbol::torseNeedleBackKnitTrLeft:
  case Symbol::torseNeedleBackPurlTrLeft:
  case Symbol::torseNeedleBackKnitTrRight:
  case Symbol::torseNeedleBackPurlTrRight:
  case Symbol::torseNeedleFrontKnit:
  case Symbol::torseNeedleFrontPurl:
  case Symbol::torseNeedleFrontKnitTrLeft:
  case Symbol::torseNeedleFrontPurlTrLeft:
  case Symbol::torseNeedleFrontKnitTrRight:
  case Symbol::torseNeedleFrontPurlTrRight:
    drawTorsade(painter, offset, sym);
    break;
  case Symbol::torseNeedleBackKnitLeftEnd:
  case Symbol::torseNeedleBackPurlLeftEnd:
  case Symbol::torseNeedleBackKnitRightEnd:
  case Symbol::torseNeedleBackPurlRightEnd:
  case Symbol::torseNeedleFrontKnitLeftEnd:
  case Symbol::torseNeedleFrontPurlLeftEnd:
  case Symbol::torseNeedleFrontKnitRightEnd:
  case Symbol::torseNeedleFrontPurlRightEnd:
    drawTorsadeEnd(painter, offset, sym);
    break;
  }
}

QPixmap SymbolsDrawer::drawTorsade(SymbolsDrawer::Symbol sym)
{
  bool NeedleFront=sym>=Symbol::torseNeedleFrontKnit;
  bool knit=(sym%2)==(Symbol::torseNeedleBackKnit%2); 
  bool tr= ((sym>=Symbol::torseNeedleBackKnitTrLeft)&&(sym<=Symbol::torseNeedleBackPurlTrRight))
          ||((sym>=Symbol::torseNeedleFrontKnitTrLeft)&&(sym<=Symbol::torseNeedleFrontPurlTrRight));
  bool trRight = (sym==Symbol::torseNeedleBackKnitTrRight)||(sym==Symbol::torseNeedleBackPurlTrRight)
          ||(sym==Symbol::torseNeedleFrontKnitTrRight)||(sym==Symbol::torseNeedleFrontPurlTrRight);
  return drawTorsade(NeedleFront, knit, tr, trRight);
}

void SymbolsDrawer::drawTorsade(QPainter &painter, const QPointF& offset, SymbolsDrawer::Symbol sym)
{
  bool NeedleFront=sym>=Symbol::torseNeedleFrontKnit;
  bool knit=(sym%2)==(Symbol::torseNeedleBackKnit%2); 
  bool tr= ((sym>=Symbol::torseNeedleBackKnitTrLeft)&&(sym<=Symbol::torseNeedleBackPurlTrRight))
          ||((sym>=Symbol::torseNeedleFrontKnitTrLeft)&&(sym<=Symbol::torseNeedleFrontPurlTrRight));
  bool trRight = (sym==Symbol::torseNeedleBackKnitTrRight)||(sym==Symbol::torseNeedleBackPurlTrRight)
          ||(sym==Symbol::torseNeedleFrontKnitTrRight)||(sym==Symbol::torseNeedleFrontPurlTrRight);
  drawTorsade(painter, offset, NeedleFront, knit, tr, trRight);
}

QPixmap SymbolsDrawer::drawTorsadeEnd ( SymbolsDrawer::Symbol sym )
{
  bool needleFront = sym>= torseNeedleFrontKnitLeftEnd;
  bool knit= (sym%2)== (torseNeedleBackKnitLeftEnd%2);
  bool right = (sym==Symbol::torseNeedleBackKnitRightEnd)||(sym==Symbol::torseNeedleBackPurlRightEnd)
  ||(sym==Symbol::torseNeedleFrontKnitRightEnd)||(sym==Symbol::torseNeedleFrontPurlRightEnd);
  return drawTorsadeEnd(needleFront,knit,right);
}

void SymbolsDrawer::drawTorsadeEnd (QPainter &painter, const QPointF& offset,  SymbolsDrawer::Symbol sym )
{
  bool needleFront = sym>= torseNeedleFrontKnitLeftEnd;
  bool knit= (sym%2)== (torseNeedleBackKnitLeftEnd%2);
  bool right = (sym==Symbol::torseNeedleBackKnitRightEnd)||(sym==Symbol::torseNeedleBackPurlRightEnd)
  ||(sym==Symbol::torseNeedleFrontKnitRightEnd)||(sym==Symbol::torseNeedleFrontPurlRightEnd);
  drawTorsadeEnd(painter, offset, needleFront,knit,right);
}


QPixmap SymbolsDrawer::drawTorsade(bool needleFront, bool knit, bool tr, bool trRight)
{
  QPixmap pix(size);
  QPainter painter(&pix);
  pix.fill(backgroundColor);
  int xMid = size.width() / 2;
  int yMid = size.height() / 2;
  int height = size.height();
  int width = size.width();
  pen.setColor(color);
  painter.setPen(pen);
  if(knit)
    painter.drawLine(xMid, needleFront?0.9*height:0.75*height, xMid, needleFront? 0.25*height:0.1*height);
  else
    painter.drawLine(tr&&trRight?0.8*width:0.9*width, yMid, tr&&!trRight?0.2*width:0.1*width, yMid);
  
  double needley=needleFront?0.1*height:0.9*height;
  double needlex0=(tr&&(!trRight))?0.2*width:0.;
  double needlex1=(tr&&trRight)?0.8*width:width;
  painter.drawLine(needlex0,needley,needlex1,needley);
  if(tr)
  {
    if(trRight)
      painter.drawLine(needlex1,needley,width,0.5*height);
    else
      painter.drawLine(0.,0.5*height,needlex0,needley);
      
  }
  return pix;
}

void SymbolsDrawer::drawTorsade(QPainter &painter, const QPointF& offset,  bool needleFront, bool knit, bool tr, bool trRight)
{
  painter.save();
  painter.translate(offset);
  painter.fillRect(0, 0, size.width(), size.height(), backgroundColor);
  float xMid = size.width() / 2;
  float yMid = size.height() / 2;
  float height = size.height();
  float width = size.width();
  pen.setColor(color);
  painter.setPen(pen);
  if(knit)
    painter.drawLine(xMid, needleFront?0.9*height:0.75*height, xMid, needleFront? 0.25*height:0.1*height);
  else
    painter.drawLine(tr&&trRight?0.8*width:0.9*width, yMid, tr&&!trRight?0.2*width:0.1*width, yMid);
  
  float needley=needleFront?0.1*height:0.9*height;
  float needlex0=(tr&&(!trRight))?0.2*width:0.;
  float needlex1=(tr&&trRight)?0.8*width:width;
  painter.drawLine(needlex0,needley,needlex1,needley);
  if(tr)
  {
    if(trRight)
      painter.drawLine(needlex1,needley,width,0.5*height);
    else
      painter.drawLine(0.,0.5*height,needlex0,needley);
      
  }
  painter.restore();
}

QPixmap SymbolsDrawer::drawTorsadeEnd ( bool needleFront, bool knit, bool endRight )
{
  QPixmap pix(size);
  QPainter painter(&pix);
  pix.fill(backgroundColor);
  int xMid = size.width() / 2;
  int yMid = size.height() / 2;
  int height = size.height();
  int width = size.width();
  pen.setColor(color);
  painter.setPen(pen);
  if(knit)
    painter.drawLine(xMid, needleFront?0.9*height:0.75*height, xMid, needleFront? 0.25*height:0.1*height);
  else
    painter.drawLine(endRight?0.8*width:0.9*width, yMid, !endRight?0.2*width:0.1*width, yMid);
  
  double needley=needleFront?0.1*height:0.9*height;
  double needlex0=(!endRight)?0.2*width:0.;
  double needlex1=(endRight)?0.8*width:width;
  painter.drawLine(needlex0,needley,needlex1,needley);
  return pix;
}

void SymbolsDrawer::drawTorsadeEnd (QPainter &painter, const QPointF& offset, bool needleFront, bool knit, bool endRight )
{
  painter.save();
  painter.translate(offset);
  painter.fillRect(0, 0, size.width(), size.height(), backgroundColor);
  float xMid = size.width() / 2;
  float yMid = size.height() / 2;
  float height = size.height();
  float width = size.width();
  pen.setColor(color);
  painter.setPen(pen);
  if(knit)
    painter.drawLine(xMid, needleFront?0.9*height:0.75*height, xMid, needleFront? 0.25*height:0.1*height);
  else
    painter.drawLine(endRight?0.8*width:0.9*width, yMid, !endRight?0.2*width:0.1*width, yMid);
  
  float needley=needleFront?0.1*height:0.9*height;
  float needlex0=(!endRight)?0.2*width:0.;
  float needlex1=(endRight)?0.8*width:width;
  painter.drawLine(needlex0,needley,needlex1,needley);
  painter.restore();
}


QPixmap SymbolsDrawer::drawNone()
{
  QPixmap pix(size);
  pix.fill(backgroundColor);
  return pix;
}

void SymbolsDrawer::drawNone(QPainter &painter, const QPointF& offset)
{
  painter.save();
  painter.translate(offset);
  painter.fillRect(0, 0, size.width(), size.height(), backgroundColor);
  painter.restore();
}

QPixmap SymbolsDrawer::drawKnit()
{
  QPixmap pix(size);
  QPainter painter(&pix);
  pix.fill(backgroundColor);
  int xMid = size.width() / 2;
  int height = size.height();
  pen.setColor(color);
  painter.setPen(pen);
  painter.drawLine(xMid, 0.9*height, xMid, 0.1*height);
  return pix;
}

void SymbolsDrawer::drawKnit(QPainter &painter, const QPointF& offset)
{
  painter.save();
  painter.translate(offset);
  painter.fillRect(0, 0, size.width(), size.height(), backgroundColor);
  int xMid = size.width() / 2;
  int height = size.height();
  pen.setColor(color);
  painter.setPen(pen);
  painter.drawLine(xMid, 0.9*height, xMid, 0.1*height);
  painter.restore();
}

QPixmap SymbolsDrawer::drawPurl()
{
  QPixmap pix(size);
  QPainter painter(&pix);
  pix.fill(backgroundColor);
  int yMid = size.height() / 2;
  int width = size.width();
  pen.setColor(color);
  painter.setPen(pen);
  painter.drawLine(0.9*width, yMid, 0.1*width, yMid);
  return pix;
}

void SymbolsDrawer::drawPurl(QPainter &painter, const QPointF& offset)
{
  painter.save();
  painter.translate(offset);
  painter.fillRect(0, 0, size.width(), size.height(), backgroundColor);
  int yMid = size.height() / 2;
  int width = size.width();
  pen.setColor(color);
  painter.setPen(pen);
  painter.drawLine(0.9*width, yMid, 0.1*width, yMid);
  painter.restore();
}

QPixmap SymbolsDrawer::drawYarnOver()
{
  QPixmap pix(size);
  QPainter painter(&pix);
  painter.setRenderHint(QPainter::Antialiasing, true);
  pix.fill(backgroundColor);
  int height = size.height();
  int width = size.width();
  pen.setColor(color);
  painter.setPen(pen);
  painter.drawEllipse(0.1*width, 0.1*height, 0.8*width, 0.8*height);
  return pix;
}

void SymbolsDrawer::drawYarnOver(QPainter &painter, const QPointF& offset)
{
  painter.save();
  painter.translate(offset);
  painter.fillRect(0, 0, size.width(), size.height(), backgroundColor);
  int height = size.height();
  int width = size.width();
  pen.setColor(color);
  painter.setPen(pen);
  painter.drawEllipse(0.1*width, 0.1*height, 0.8*width, 0.8*height);
  painter.restore();
}

QPixmap SymbolsDrawer::drawBobble()
{
  QPixmap pix(size);
  QPainter painter(&pix);
  painter.setRenderHint(QPainter::Antialiasing, true);
  pix.fill(backgroundColor);
  int height = size.height();
  int width = size.width();
  pen.setColor(color);
  painter.setPen(pen);
  QPainterPath path;
  QBrush brush(Qt::black);
  path.addEllipse(0.2*width, 0.2*height, 0.6*width, 0.6*height);
  painter.fillPath(path,brush);
  painter.drawEllipse(0.2*width, 0.2*height, 0.6*width, 0.6*height);
  return pix;
}

void SymbolsDrawer::drawBobble(QPainter &painter, const QPointF& offset)
{
  painter.save();
  painter.translate(offset);
  painter.fillRect(0, 0, size.width(), size.height(), backgroundColor);
  int height = size.height();
  int width = size.width();
  pen.setColor(color);
  painter.setPen(pen);
  QPainterPath path;
  QBrush brush(Qt::black);
  path.addEllipse(0.2*width, 0.2*height, 0.6*width, 0.6*height);
  painter.fillPath(path,brush);
  painter.drawEllipse(0.2*width, 0.2*height, 0.6*width, 0.6*height);
  painter.restore();
}

QPixmap SymbolsDrawer::drawK2tog(bool RS)
{
  QPixmap pix(size);
  QPainter painter(&pix);
  painter.setRenderHint(QPainter::Antialiasing, true);
  pix.fill(backgroundColor);
  int height = size.height();
  int width = size.width();
  pen.setColor(color);
  painter.setPen(pen);
  double baseFactor=RS?0.9*height:0.8*height;
  double midFactor=RS?0.5*height:0.45*height;
  painter.drawLine(0.1*width, baseFactor, 0.9*width, 0.1*height);
  painter.drawLine(0.5*width, midFactor, 0.9*width, baseFactor);
  if(!RS)
    painter.drawLine(0.1*width, 0.9*height, 0.9*width, 0.9*height);
  return pix;
}

void SymbolsDrawer::drawK2tog(QPainter &painter, const QPointF& offset, bool RS)
{
  painter.save();
  painter.translate(offset);
  painter.fillRect(0, 0, size.width(), size.height(), backgroundColor);
  int height = size.height();
  int width = size.width();
  pen.setColor(color);
  painter.setPen(pen);
  double baseFactor=RS?0.9*height:0.8*height;
  double midFactor=RS?0.5*height:0.45*height;
  painter.drawLine(0.1*width, baseFactor, 0.9*width, 0.1*height);
  painter.drawLine(0.5*width, midFactor, 0.9*width, baseFactor);
  if(!RS)
    painter.drawLine(0.1*width, 0.9*height, 0.9*width, 0.9*height);
  painter.restore();
}

QPixmap SymbolsDrawer::drawSsk(bool RS)
{
  QPixmap pix(size);
  QPainter painter(&pix);
  painter.setRenderHint(QPainter::Antialiasing, true);
  pix.fill(backgroundColor);
  int height = size.height();
  int width = size.width();
  pen.setColor(color);
  painter.setPen(pen);
  double baseFactor=RS?0.9*height:0.8*height;
  double midFactor=RS?0.5*height:0.45*height;
  painter.drawLine(0.1*width, 0.1*height, 0.9*width, baseFactor);
  painter.drawLine(0.5*width, midFactor, 0.1*width, baseFactor);
  if(!RS)
    painter.drawLine(0.1*width, 0.9*height, 0.9*width, 0.9*height);
  return pix;
}

void SymbolsDrawer::drawSsk(QPainter &painter, const QPointF& offset, bool RS)
{
  painter.save();
  painter.translate(offset);
  painter.fillRect(0, 0, size.width(), size.height(), backgroundColor);
  int height = size.height();
  int width = size.width();
  pen.setColor(color);
  painter.setPen(pen);
  double baseFactor=RS?0.9*height:0.8*height;
  double midFactor=RS?0.5*height:0.45*height;
  painter.drawLine(0.1*width, 0.1*height, 0.9*width, baseFactor);
  painter.drawLine(0.5*width, midFactor, 0.1*width, baseFactor);
  if(!RS)
    painter.drawLine(0.1*width, 0.9*height, 0.9*width, 0.9*height);
  painter.restore();
}

QPixmap SymbolsDrawer::drawK3tog(bool RS)
{
  QPixmap pix(size);
  QPainter painter(&pix);
  painter.setRenderHint(QPainter::Antialiasing, true);
  pix.fill(backgroundColor);
  int height = size.height();
  int width = size.width();
  pen.setColor(color);
  painter.setPen(pen);
  double baseFactor=RS?0.9*height:0.8*height;
  double midFactor=RS?0.5*height:0.45*height;
  painter.drawLine(0.1*width, baseFactor, 0.9*width, 0.1*height);
  painter.drawLine(0.5*width, midFactor, 0.9*width, baseFactor);
  painter.drawLine(0.5*width, midFactor, 0.5*width, baseFactor);
  if(!RS)
    painter.drawLine(0.1*width, 0.9*height, 0.9*width, 0.9*height);
  return pix;
}

void SymbolsDrawer::drawK3tog(QPainter &painter, const QPointF& offset, bool RS)
{
  painter.save();
  painter.translate(offset);
  painter.fillRect(0, 0, size.width(), size.height(), backgroundColor);
  int height = size.height();
  int width = size.width();
  pen.setColor(color);
  painter.setPen(pen);
  double baseFactor=RS?0.9*height:0.8*height;
  double midFactor=RS?0.5*height:0.45*height;
  painter.drawLine(0.1*width, baseFactor, 0.9*width, 0.1*height);
  painter.drawLine(0.5*width, midFactor, 0.9*width, baseFactor);
  painter.drawLine(0.5*width, midFactor, 0.5*width, baseFactor);
  if(!RS)
    painter.drawLine(0.1*width, 0.9*height, 0.9*width, 0.9*height);
  painter.restore();
}

QPixmap SymbolsDrawer::drawSk2po(bool RS)
{
  QPixmap pix(size);
  QPainter painter(&pix);
  painter.setRenderHint(QPainter::Antialiasing, true);
  pix.fill(backgroundColor);
  int height = size.height();
  int width = size.width();
  pen.setColor(color);
  painter.setPen(pen);
  double baseFactor=RS?0.9*height:0.8*height;
  double midFactor=RS?0.5*height:0.45*height;
  painter.drawLine(0.1*width, 0.1*height, 0.9*width, baseFactor);
  painter.drawLine(0.5*width, midFactor, 0.1*width, baseFactor);
  painter.drawLine(0.5*width, midFactor, 0.5*width, baseFactor);
  if(!RS)
    painter.drawLine(0.1*width, 0.9*height, 0.9*width, 0.9*height);
  return pix;
}

void SymbolsDrawer::drawSk2po(QPainter &painter, const QPointF& offset, bool RS)
{
  painter.save();
  painter.translate(offset);
  painter.fillRect(0, 0, size.width(), size.height(), backgroundColor);
  int height = size.height();
  int width = size.width();
  pen.setColor(color);
  painter.setPen(pen);
  double baseFactor=RS?0.9*height:0.8*height;
  double midFactor=RS?0.5*height:0.45*height;
  painter.drawLine(0.1*width, 0.1*height, 0.9*width, baseFactor);
  painter.drawLine(0.5*width, midFactor, 0.1*width, baseFactor);
  painter.drawLine(0.5*width, midFactor, 0.5*width, baseFactor);
  if(!RS)
    painter.drawLine(0.1*width, 0.9*height, 0.9*width, 0.9*height);
  painter.restore();
}

QPixmap SymbolsDrawer::drawK2skpo(bool RS)
{
  QPixmap pix(size);
  QPainter painter(&pix);
  painter.setRenderHint(QPainter::Antialiasing, true);
  pix.fill(backgroundColor);
  int height = size.height();
  int width = size.width();
  pen.setColor(color);
  painter.setPen(pen);
  double baseFactor=RS?0.9*height:0.8*height;
  double midFactor=RS?0.5*height:0.45*height;
  painter.drawLine(0.5*width, midFactor, 0.9*width, baseFactor);
  painter.drawLine(0.5*width, midFactor, 0.1*width, baseFactor);
  painter.drawLine(0.5*width, 0.1*height, 0.5*width, baseFactor);
  if(!RS)
    painter.drawLine(0.1*width, 0.9*height, 0.9*width, 0.9*height);
  return pix;
}

void SymbolsDrawer::drawK2skpo(QPainter &painter, const QPointF& offset, bool RS)
{
  painter.save();
  painter.translate(offset);
  painter.fillRect(0, 0, size.width(), size.height(), backgroundColor);
  int height = size.height();
  int width = size.width();
  pen.setColor(color);
  painter.setPen(pen);
  double baseFactor=RS?0.9*height:0.8*height;
  double midFactor=RS?0.5*height:0.45*height;
  painter.drawLine(0.5*width, midFactor, 0.9*width, baseFactor);
  painter.drawLine(0.5*width, midFactor, 0.1*width, baseFactor);
  painter.drawLine(0.5*width, 0.1*height, 0.5*width, baseFactor);
  if(!RS)
    painter.drawLine(0.1*width, 0.9*height, 0.9*width, 0.9*height);
  painter.restore();
}

QPixmap SymbolsDrawer::drawK1tbl(bool RS)
{
  QPixmap pix(size);
  QPainter painter(&pix);
  painter.setRenderHint(QPainter::Antialiasing, true);
  pix.fill(backgroundColor);
  int height = size.height();
  int width = size.width();
  pen.setColor(color);
  painter.setPen(pen);

  QPainterPath path;
  path.moveTo(0.2*width, 0.9*height);
  path.arcTo(-0.3*width, -0.1*height, width, height, -90, 90);
  path.arcTo(0.3*width, 0.2*height, 0.4*width, 0.4*height, 0, 180);
  path.arcTo(0.3*width, -0.1*height, width, height, 180, 90);
  if(RS)
  {
      painter.drawPath(path);
  }
  else
  {
    painter.drawLine(0.1*width, 0.9*height, 0.9*width, 0.9*height);
    QTransform transform;
    transform.translate(0.05*width,0);
    transform.scale(0.9,0.9);
    painter.setTransform(transform);
    painter.drawPath(path);
  }
  return pix;
}

void SymbolsDrawer::drawK1tbl(QPainter &painter, const QPointF& offset, bool RS)
{
  painter.save();
  painter.translate(offset);
  painter.fillRect(0, 0, size.width(), size.height(), backgroundColor);
  int height = size.height();
  int width = size.width();
  pen.setColor(color);
  painter.setPen(pen);

  QPainterPath path;
  path.moveTo(0.2*width, 0.9*height);
  path.arcTo(-0.3*width, -0.1*height, width, height, -90, 90);
  path.arcTo(0.3*width, 0.2*height, 0.4*width, 0.4*height, 0, 180);
  path.arcTo(0.3*width, -0.1*height, width, height, 180, 90);
  if(RS)
  {
      painter.drawPath(path);
  }
  else
  {
    painter.drawLine(0.1*width, 0.9*height, 0.9*width, 0.9*height);
    QTransform transform;
    transform.translate(0.05*width,0);
    transform.scale(0.9,0.9);
    painter.setTransform(transform);
    painter.drawPath(path);
  }
  painter.restore();
}
