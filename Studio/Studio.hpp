#ifndef STUDIO_HPP
#define STUDIO_HPP

#include <QMainWindow>
#include <QDockWidget>
#include <ActionMng.hpp>
#include <Factory.hpp>
#include "GrilleEditor.hpp"
#include "GridStyle.hpp"
#include "ModulesInterface.hpp"

class Studio : public QMainWindow
{
  Q_OBJECT
public:
  Studio();
  ~Studio();
protected:
  QSize minimumSizeHint() const override;
private slots:
  void openFile();
  void newFile();
  void saveFile();
  void saveFileAs();
  void exportChart();

  void toolSelect();
  void toolPen();
  void toolRect();
  void toolFill();
  void toolStamp();
  void toolRemapStyles();
  void toolRemapColors();
  void toolMirrorV();
  void toolMirrorH();
  void toolRotateLeft();
  void toolRotateRight();
  void undo();
  void redo();
  
  void updateStyle(int sty);

  void about();
private:
  void uncheckAllToolActions();
  void createActions();
  void createMenus();
  void createToolbars();
  void createDockWidgets();

  GrilleEditor * scene = nullptr;
  QString filename;

  QAction *openFileAction;
  QAction *saveFileAction;
  QAction *saveFileAsAction;
  QAction *newFileAction;
  QAction *exportChartAction;
  QAction *aboutAction;
  
  QAction *undoAction;
  QAction *redoAction;
  
  QAction *toolSelectAction;
  QAction *toolPenAction;
  QAction *toolRectAction;
  QAction *toolFillAction;
  QAction *toolStampAction;
  QAction *toolRemapStylesAction;
  QAction *toolRemapColorsAction;

  QAction *toolMirrorVAction;
  QAction *toolMirrorHAction;
  QAction *toolRotateLeftAction;
  QAction *toolRotateRightAction;
  
  QMenu *fileMenu;
  QMenu *toolMenu;
  QMenu *historyMenu;
  QMenu *aboutMenu;
  QToolBar* fileToolBar;
  QToolBar* toolToolBar;
  QToolBar* historyBar;
  
  ActionMng* actionMng;
  Factory* motifFactory;

  GridStyle* gridStyleWidget;
  QDockWidget* gridStyleDockWidget;
  ModulesInterface* modulesWidget;
  QDockWidget* modulesDockWidget;
};

#endif // !STUDIO_HPP

