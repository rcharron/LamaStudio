#include "MotifRenderer.hpp"
#include <QPainter>
#include <QDebug>
#include <QMouseEvent>
/*
QSize MotifRenderer::cellSize = QSize(20, 20);
bool MotifRenderer::antialiasing = false;

GrilleCell MotifRenderer::cell(QMap<int, Style>& styles, int i)
{
	if(i<=0)
		return GrilleCell();
	GrilleCell c;
	c.setColor(styles[i].color);
	return c;
}

QPixmap MotifRenderer::renderFast(Motif motif, QMap<int, Style> styles)
{
	QSize mSize = motif.size();
	int cw = cellSize.width();
	int ch = cellSize.height();
	int mw = motif.size().width();
	int mh = motif.size().height();
	QPixmap pixmap(QSize(cw*mw, ch*mh));
	pixmap.fill(Qt::white);
	QPainter painter(&pixmap);
	for (int i = 0; i < mw; i++)
	{
		for (int j = 0; j < mh; j++)
		{
			painter.save();
			painter.translate(i*cw, j*ch);
			GrilleCell c = cell(styles, motif(i, j));
			c.drawCell(painter, cellSize);
			painter.restore();
		}
	}
	return pixmap;
}

QPixmap MotifRenderer::render(Motif motif, QMap<int, Style> styles)
{
	QSize mSize = motif.size();
	int cw = cellSize.width();
	int ch = cellSize.height();
	int mw = motif.size().width();
	int mh = motif.size().height();
	QPixmap pixmap(QSize(cw*mw + cw, ch*mh + ch));
	pixmap.fill(Qt::white);
	QPainter painter(&pixmap);
	painter.translate(0.5*cw, 0.5*ch);
	for (int i = 0; i < mw; i++)
	{
		for (int j = 0; j < mh; j++)
		{
			painter.save();
			painter.translate(i*cw, j*ch);
			GrilleCell c = cell(styles, motif(i, j));
			c.drawCell(painter, cellSize);
			painter.restore();
		}
	}
	for (int i = 0; i < mw; i++)
	{
		for (int j = 0; j < mh; j++)
		{
			painter.save();
			painter.translate(i*cw, j*ch);
			GrilleCell c = cell(styles, motif(i, j));
			c.drawBorder(painter, cellSize);
			painter.restore();
		}
	}
	return pixmap;
}

QPixmap MotifRenderer::resize(QPixmap & pixmap, QSize newSize)
{
	return pixmap.scaled(newSize, Qt::KeepAspectRatio, Qt::SmoothTransformation);
}*/
