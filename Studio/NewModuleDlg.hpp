#ifndef NEWMODULEDLG_H
#define NEWMODULEDLG_H

#include <QDialog>
#include <QLineEdit>
#include <QDialogButtonBox>
#include <dimensions.hpp>

class NewModuleDlg : public QDialog
{
  Q_OBJECT
public:
  NewModuleDlg(QWidget* parent = nullptr);
  QString getName();
  Dimensions getSize();
private:
  QLineEdit* name;
  QLineEdit* sizeX;
  QLineEdit* sizeY;
  QDialogButtonBox* buttonBox;
};

#endif // NEWMODULEDLG_H
