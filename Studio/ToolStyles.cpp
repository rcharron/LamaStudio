#include "ToolStyles.hpp"
#include <SymbolsDrawer.hpp>
#include "ChooseSymbolDlg.hpp"
#include "QScrollArea"
#include <QHBoxLayout>
#include <QColorDialog>

ToolStylesLine::ToolStylesLine ( int id, QWidget* parent )
  : QWidget ( parent ), id ( id )
{
  QLabel* tId = new QLabel ( QString::number ( id ) + QChar ( ':' ), this );
  QLabel* tColor = new QLabel ( tr ( "Color" ), this );
  tSel = new QLabel ( " ", this );
  bColor = new QToolButton ( this );
  bColor->setAutoFillBackground ( true );
  bSymb = new QPushButton(this);
  bSymb->setFlat(true);
  bsColor = new QToolButton(this);
  bsColor->setAutoFillBackground(true);
  QHBoxLayout* layout = new QHBoxLayout ( this );
  layout->addWidget ( tSel );
  layout->addWidget ( tId );
  layout->addWidget ( tColor );
  layout->addWidget ( bColor );
  layout->addWidget ( bSymb );
  layout->addWidget ( bsColor );
  syncFrom ( Style() );
  connect ( bColor, &QToolButton::clicked, this, &ToolStylesLine::changeColor );
  connect ( bsColor, &QToolButton::clicked, this, &ToolStylesLine::changeSymbolColor );
  connect ( bSymb, &QToolButton::clicked, this, &ToolStylesLine::changeSymbol );
  layout->setMargin ( 0 );
}

void ToolStylesLine::syncFrom ( Style source )
{
  style = source;
  {
	  QPixmap px(20, 20);
	  QColor col = QColor::fromRgb(style.backgroundColor.r, style.backgroundColor.g, style.backgroundColor.b);
	  px.fill(col);
	  bColor->setIcon(px);
  }
  {
	  QPixmap px(20, 20);
	  QColor col = QColor::fromRgb(style.foregroundColor.r, style.foregroundColor.g, style.foregroundColor.b);
	  px.fill(col);
	  bsColor->setIcon(px);
  }
  SymbolsDrawer sdraw;
  sdraw.setSize(QSize(20,20));
  bSymb->setIcon(sdraw.draw(static_cast<SymbolsDrawer::Symbol>(source.symbol)));
}

Style ToolStylesLine::getStyle()
{
  return style;
}

void ToolStylesLine::setSelected ( bool val )
{
  if ( val ) tSel->setText ( ">" );
  else tSel->setText ( " " );
}

void ToolStylesLine::mousePressEvent ( QMouseEvent * event )
{
  Q_UNUSED(event);
  emit clicked ( id );
}


void ToolStylesLine::changeColor()
{
  QColor col = QColor::fromRgb ( style.backgroundColor.r,style.backgroundColor.g,style.backgroundColor.b );
  QColor color = QColorDialog::getColor ( col, this, tr ( "Choose cell color" ) );
  if ( !color.isValid() ) return;
  style.backgroundColor = Style::Color ( color.red(), color.green(), color.blue() );
  QPixmap px ( 20, 20 );
  px.fill ( color );
  bColor->setIcon ( px );
  emit styleChanged ( id );
}

void ToolStylesLine::changeSymbolColor()
{
	QColor col = QColor::fromRgb(style.foregroundColor.r, style.foregroundColor.g, style.foregroundColor.b);
	QColor color = QColorDialog::getColor(col, this, tr("Choose symbol color"));
	if (!color.isValid()) return;
	style.foregroundColor = Style::Color(color.red(), color.green(), color.blue());
	QPixmap px(20, 20);
	px.fill(color);
	bsColor->setIcon(px);
	emit styleChanged(id);
}

void ToolStylesLine::changeSymbol()
{
    ChooseSymbolDlg *dlg = new ChooseSymbolDlg(this);
    if ( dlg->exec() == QDialog::Accepted )
    {
        style.symbol = dlg->getSelected();
        SymbolsDrawer sdraw;
        sdraw.setSize(QSize(20,20));
        bSymb->setIcon(sdraw.draw(static_cast<SymbolsDrawer::Symbol>(style.symbol)));
        emit styleChanged(id);
    }
}

ToolStylesLineBackground::ToolStylesLineBackground ( QWidget* parent )
  : QWidget ( parent )
{
  QLabel* tId = new QLabel ( "0 :", this );
  QLabel* tColor = new QLabel ( tr ( "Default" ), this );
  tSel = new QLabel ( " ", this );
  QHBoxLayout* layout = new QHBoxLayout ( this );
  layout->addWidget ( tSel );
  layout->addWidget ( tId );
  layout->addWidget ( tColor );
  layout->setMargin ( 0 );
}

void ToolStylesLineBackground::mousePressEvent ( QMouseEvent* event )
{
  Q_UNUSED(event);
  emit clicked ( 0 );
}

void ToolStylesLineBackground::setSelected ( bool val )
{
  if ( val ) tSel->setText ( ">" );
  else tSel->setText ( " " );
}

ToolStyles::ToolStyles ( QWidget* parent )
  : QScrollArea ( parent )
{
  QWidget* areaWidget = new QWidget ( this );
  layout = new QGridLayout ( areaWidget );
  setWidget ( areaWidget );
  setWidgetResizable ( true );
  setVerticalScrollBarPolicy ( Qt::ScrollBarAsNeeded );
  setHorizontalScrollBarPolicy ( Qt::ScrollBarAlwaysOff );

  bAddStyle = new QPushButton ( tr ( "Add a style" ), this );

  styleDefault = new ToolStylesLineBackground ( this );
  layout->addWidget ( styleDefault, 0, 0 );
  layout->addWidget ( bAddStyle, 1, 0 );

  connect ( styleDefault, &ToolStylesLineBackground::clicked, this, &ToolStyles::select );
  connect ( bAddStyle, &QPushButton::clicked, this, &ToolStyles::addLine );
  addLine();
  select ( 0 );
}

Style ToolStyles::get ( int styleId )
{
  if ( styleId <= styles.size() )
    return styles[styleId - 1]->getStyle();
  return Style();
}

int ToolStyles::numberStyles()
{
  return lastStyle;
}

void ToolStyles::set ( int styleId, Style source )
{
  if ( styleId <= styles.size() )
    styles[styleId - 1]->syncFrom ( source );
}

int ToolStyles::getSelected()
{
  return selection;
}

void ToolStyles::setStyles ( std::map<int, Style> sty )
{
  for ( auto* swidget : styles )
    {
      layout->removeWidget ( swidget );
      swidget->close();
      swidget->deleteLater();
    }
  styles.clear();
  lastStyle = 0;
  for ( auto ste : sty )
    {
      while ( ste.first > lastStyle )
        addLine();
      styles[ste.first - 1]->syncFrom ( ste.second );
    }
  if ( lastStyle == 0 ) addLine();
  select ( 0 );
}

std::map<int, Style> ToolStyles::getStyles()
{
  std::map<int, Style> sty;
  for ( int i = 0; i < styles.size(); i++ )
    sty[i + 1] = styles[i]->getStyle();
  return sty;
}

void ToolStyles::clear()
{
  for ( auto* swidget : styles )
    {
      layout->removeWidget ( swidget );
      swidget->close();
      swidget->deleteLater();
    }
  styles.clear();
  lastStyle = 0;
  addLine();
  select(0);
}

void ToolStyles::select ( int id )
{
  if ( selection ==0 )
    styleDefault->setSelected ( false );
  else
    styles[selection-1]->setSelected ( false );

  if ( id > lastStyle || id <=0 )
    {
      selection = 0;
      styleDefault->setSelected ( true );
    }
  else
    {
      selection = id;
      styles[selection - 1]->setSelected ( true );
    }
  emit styleSelected ( selection );
}

void ToolStyles::addLine()
{
  lastStyle++;
  ToolStylesLine* line = new ToolStylesLine ( lastStyle );
  styles.push_back ( line );
  layout->addWidget ( line, lastStyle, 0 );
  layout->addWidget ( bAddStyle, lastStyle + 1, 0 );
  connect ( line, &ToolStylesLine::styleChanged, this, &ToolStyles::styleChanged );
  connect ( line, &ToolStylesLine::clicked, this, &ToolStyles::select );
}

