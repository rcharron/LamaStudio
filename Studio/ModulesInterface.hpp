#ifndef MODULESINTERFACE_H
#define MODULESINTERFACE_H

#include <QWidget>
#include <Module.hpp>
#include <QComboBox>
#include <QPushButton>
#include <QLineEdit>
#include <map>
#include <string>
#include <vector>
#include <ActionMng.hpp>
#include <Factory.hpp>

class ModulesInterface : public QWidget
{
  Q_OBJECT
public:
  ModulesInterface(ActionMng* mgr, Factory* factory, QWidget* parent = nullptr);
  Module* module();
  std::vector<Module*> listOfModules();
  void resetWith(std::vector<Module*> mods);
  void clear();
private slots:
  void doAddModule();
  void doRemoveModule();
  void doDuplicateModule();
  void moduleChanged(QString text);
  void moduleChanged(int indexCBox);
  void renameModule();
  void doResizeModule();
Q_SIGNALS:
  void moduleSelected(Module* module);
  void pleaseUpdateEditor();
private:
  ActionMng* mgr;
  Factory* factory;
  std::map<std::string, Module*> theModules;
  QComboBox* listeModules;
  QLineEdit* nameModule;
  QPushButton* addModule;
  QPushButton* deleteModule;
  QPushButton* resizeModule;
  QPushButton* duplicateModule;
};

#endif // MODULESINTERFACE_H
