#include "GrilleEditor.hpp"
#include "GrilleEditorTool.hpp"
#include "GrilleEditorToolNone.hpp"
#include "GrilleEditorToolPen.hpp"
#include "GrilleEditorToolRect.hpp"
#include "GrilleEditorToolFill.hpp"
#include "GrilleEditorToolStamp.hpp"
// #include <ActionResize.hpp>
// #include <ActionRect.hpp>
// #include <ActionPen.hpp>

GrilleEditor::GrilleEditor ( ActionMng *mgr, Factory *motifFactory, QWidget *parent ) :
  Grille ( motifFactory, parent ),
  mgr ( mgr ),
  selectedTool ( new GrilleEditorToolNone() ),
  selectedStyle ( 0 ),
  mouseWasPressed ( false )
{
}

void GrilleEditor::selecteStyle ( int style )
{
  selectedStyle = style;
  selectedTool->selectStyle ( style );
}

void GrilleEditor::clearStyles()
{
  styles.clear();
}

void GrilleEditor::selectTool ( GrilleEditor::Tool t )
{
  GrilleEditorTool *oldTool = selectedTool;
  switch ( t )
    {
    case Tool::None:
      selectedTool = new GrilleEditorToolNone();
      break;
    case Tool::Rect:
      selectedTool = new GrilleEditorToolRect ( mgr, selectedStyle );
      break;
    case Tool::Pen:
      selectedTool = new GrilleEditorToolPen ( mgr, selectedStyle );
      break;
    case Tool::Fill:
      selectedTool = new GrilleEditorToolFill ( mgr, selectedStyle );
      break;
    default:
      throw "Not implemented";
    }
  delete oldTool;
}

void GrilleEditor::selectStampTool(GrilleEditorToolStamp* stampTool)
{
  delete selectedTool;
  selectedTool = stampTool;
}


void GrilleEditor::paintMask ( QPainter &painter )
{
  std::vector<std::pair<Dimensions, int> > mask;
  if ( mouseWasPressed )
    {
      mask = selectedTool->mask();
    }
  else
    {
      mask = selectedTool->toolOverviewmask ( getCursorCell() );
    }
  for ( auto m:mask )
    {
      draw ( m.first,painter,m.second );
    }
}

void GrilleEditor::mousePressCell ( QPoint cellid, bool leftButton )
{
  if ( leftButton )
    {
      mouseWasPressed=true;
      selectedTool->startAt ( Dimensions ( cellid.x(),cellid.y() ) );
    }
}

void GrilleEditor::finalizeTool()
{
  mouseWasPressed = false;
  Action *act = selectedTool->toAction ( module->getLastAction() );
  if ( act!=nullptr )
    module->addAction ( act );
  selectedTool->clear();
  update();
}

void GrilleEditor::mouseReleaseCell ( QPoint cellid )
{
  finalizeTool();
}

void GrilleEditor::cursorEnterCell ( QPoint cellid, bool isPressed )
{
  if ( !isPressed&&mouseWasPressed )
    {
      finalizeTool();
    }
  else if ( isPressed )
    {
      selectedTool->cursorEnter ( Dimensions ( cellid.x(),cellid.y() ) );
      update();
    }
}
