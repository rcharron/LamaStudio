#include "Grille.hpp"
#include <QPainter>
#include <QColor>
#include <QDebug>
#include <QMouseEvent>
#include <QSvgGenerator>


Grille::Grille ( Factory* motifFactory, QWidget* parent )
  : QWidget ( parent ),
  motifFactory( motifFactory ),
  drawGrilleOffset(10,10),
   drawGrilleCellSize(21,21),
   module(nullptr),
  lastMotif(-1)
{
  setMouseTracking ( true );
  setBackgroundRole ( QPalette::Base );
  setAutoFillBackground ( true );

  cursorCell = QPoint(0, 0);
}

QPoint Grille::toCoordinate(QPoint Cursor)
{
  Cursor -= drawGrilleOffset;
  Cursor.setX(Cursor.x() / drawGrilleCellSize.width());
  Cursor.setY(Cursor.y() / drawGrilleCellSize.height());
  return Cursor;
}

QSize Grille::minimumSizeHint() const
{
  return QSize ( 100, 100 );
}

QSize Grille::sizeHint() const
{
  
  return QSize ( 400, 200 );
}

void Grille::setModule(Module* mod)
{
  module = mod;
  drawGrilleOffset = QPoint(10, 10);
  update();
}

void Grille::setStyles(std::map<int, Style> stys)
{
  styles = stys;
}

void Grille::addStyle(int id, Style sty)
{
  styles[id] = sty;
}


void Grille::paintEvent ( QPaintEvent *event )
{
  QPainter painter ( this );
  if(!module)return ;
  if(module->getLastAction()==nullptr)return ;
  if(lastMotif != module->getLastAction()->ID())
  renderMotif = motifFactory->computeMotif(module->getLastAction()->ID());
  lastMotif = module->getLastAction()->ID();
  
  painter.setRenderHint(QPainter::Antialiasing, false);
  painter.save();

  painter.translate(drawGrilleOffset);
  //painter.save();


  int cw = drawGrilleCellSize.width();
  int ch = drawGrilleCellSize.height();
  symbolDrawer.setSize(QSize(cw-1,ch-1));
  symbolDrawer.setPenWidth(2);

  for (auto sti : styles)
  {
	  symbolDrawer.setColor(QColor::fromRgb(sti.second.foregroundColor.r, sti.second.foregroundColor.g, sti.second.foregroundColor.b));
	  symbolDrawer.setBackgroundColor(QColor::fromRgb(sti.second.backgroundColor.r, sti.second.backgroundColor.g, sti.second.backgroundColor.b));
	  pixOfStyles[sti.second] = symbolDrawer.draw(static_cast<SymbolsDrawer::Symbol>(sti.second.symbol));
  }

  int nx = renderMotif.size().X();
  int ny = renderMotif.size().Y();

  
  int graphicalStartingCellx = -(drawGrilleOffset.x() - 1) / cw - 1;
  int graphicalStartingCelly = -(drawGrilleOffset.y() - 1) / ch - 1;
  graphicalStartingCellx = std::max(graphicalStartingCellx,0);
  graphicalStartingCelly = std::max(graphicalStartingCelly,0);
  int graphicalNumberCellx = size().width() / cw + 1;
  int graphicalNumberCelly = size().height() / ch + 1;
  graphicalNumberCellx = std::min(graphicalNumberCellx,nx-graphicalStartingCellx);
  graphicalNumberCelly = std::min(graphicalNumberCelly,ny-graphicalStartingCelly);

  for (int i = graphicalStartingCellx; i < graphicalStartingCellx + graphicalNumberCellx; i++)
  {
    for (int j = graphicalStartingCelly; j < graphicalStartingCelly + graphicalNumberCelly; j++)
    {
      draw(Dimensions(i,j),painter, renderMotif);
    }
  }
  
  paintMask(painter);
  
  QPen pen(Qt::black);
  pen.setWidth(0);
  painter.setPen(pen);
  for (int i = graphicalStartingCellx; i <= graphicalStartingCellx + graphicalNumberCellx; i++)
  {
    painter.drawLine(i*cw,graphicalStartingCelly*ch,i*cw,(graphicalStartingCelly+graphicalNumberCelly)*ch);
  }
  for (int j = graphicalStartingCelly; j <= graphicalStartingCelly + graphicalNumberCelly; j++)
  {
    painter.drawLine(graphicalStartingCellx*cw,j*ch,(graphicalStartingCellx+graphicalNumberCellx)*cw,j*ch);
  }
  painter.restore();

  painter.setRenderHint(QPainter::Antialiasing, false);
  painter.setPen(palette().dark().color());
  painter.setBrush(Qt::NoBrush);
  painter.drawRect(QRect(0, 0, width() - 1, height() - 1));
}

void Grille::paintMask(QPainter& painter)
{
}

void Grille::mouseMoveEvent ( QMouseEvent * event )
{
  if (moving)
  {
	if (event->buttons() == Qt::RightButton)
	{
		drawGrilleOffset = drawGrilleOffsetOr + event->pos() - movingCursorOr;
		update();
	}
	else
	  moving = false;
  }
  else
  {
	auto newCell = toCoordinate(event->pos());
	if (newCell != cursorCell)
	{
      cursorEnterCell(newCell, event->buttons() != Qt::NoButton);
      cursorCell = toCoordinate(event->pos());
      update();
	}
  }
}

void Grille::mousePressEvent ( QMouseEvent * event )
{
  auto cell = toCoordinate(event->pos());
  if(event->buttons() == Qt::MouseButton::RightButton)
  {
	moving = true;
	drawGrilleOffsetOr = drawGrilleOffset;
	movingCursorOr = event->pos();
  }
  else
	mousePressCell(cell, event->button() == Qt::MouseButton::LeftButton);
}

void Grille::mouseReleaseEvent ( QMouseEvent * event )
{
  if (moving)
  {
	moving = false;
  }
  else
  {
	auto cell = toCoordinate(event->pos());
	mouseReleaseCell(cell);
  }
}

void Grille::wheelEvent ( QWheelEvent * event )
{
  auto myDelta = event->angleDelta() / 120;
  drawGrilleCellSize += QSize(myDelta.y(), myDelta.y());
  if (drawGrilleCellSize.width() <= 5)drawGrilleCellSize.setWidth(5);
  if (drawGrilleCellSize.height() <= 5)drawGrilleCellSize.setHeight(5);
  cursorCell = toCoordinate(event->pos());
  update();
}

void Grille::draw(Dimensions at, QPainter& painter, Motif& motif)
{
  painter.save();
  painter.translate(QPoint(at.X()*drawGrilleCellSize.width() + 1, at.Y()*drawGrilleCellSize.height() + 1));
  draw(styles[motif(at)], painter);
  painter.restore();
}

void Grille::draw(Dimensions at, QPainter& painter, int styleId)
{
  painter.save();
  painter.translate(QPoint(at.X()*drawGrilleCellSize.width() + 1, at.Y()*drawGrilleCellSize.height() + 1));
  draw(styles[styleId], painter);
  painter.restore();
}


void Grille::draw(Style sty, QPainter& painter)
{
  painter.drawPixmap(0, 0, pixOfStyles[sty]);
}

QPixmap Grille::drawOnPixmap()
{
	int cw = drawGrilleCellSize.width();
	int ch = drawGrilleCellSize.height();
	QPixmap pix(cw*(renderMotif.size().X()+1), ch*(renderMotif.size().Y() + 1));
  pix.fill();
	QPainter painter(&pix);
	if (!module)return pix;
	if (module->getLastAction() == nullptr)return pix;
	if (lastMotif != module->getLastAction()->ID())
		renderMotif = motifFactory->computeMotif(module->getLastAction()->ID());
	lastMotif = module->getLastAction()->ID();

	painter.setRenderHint(QPainter::Antialiasing, false);
	painter.save();

	painter.translate(QPoint(0.5*cw,0.5*ch));

	symbolDrawer.setSize(QSize(drawGrilleCellSize.width() - 1, drawGrilleCellSize.height() - 1));
	symbolDrawer.setPenWidth(2);

	for (auto sti : styles)
	{
		symbolDrawer.setColor(QColor::fromRgb(sti.second.foregroundColor.r, sti.second.foregroundColor.g, sti.second.foregroundColor.b));
		symbolDrawer.setBackgroundColor(QColor::fromRgb(sti.second.backgroundColor.r, sti.second.backgroundColor.g, sti.second.backgroundColor.b));
		pixOfStyles[sti.second] = symbolDrawer.draw(static_cast<SymbolsDrawer::Symbol>(sti.second.symbol));
	}

	int nx = renderMotif.size().X();
	int ny = renderMotif.size().Y();

	for (int i = 0; i < nx; i++)
	{
		for (int j = 0; j < ny; j++)
		{
			draw(Dimensions(i, j), painter, renderMotif);
		}
	}

	paintMask(painter);

	QPen pen(Qt::black);
	pen.setWidth(0);
	painter.setPen(pen);
	for (int i = 0; i <= nx; i++)
	{
		painter.drawLine(i*cw, 0, i*cw, ny*ch);
	}
	for (int j = 0; j <= ny; j++)
	{
		painter.drawLine(0, j*ch, nx*cw, j*ch);
	}
	painter.restore();

	painter.setRenderHint(QPainter::Antialiasing, false);
	painter.setPen(palette().dark().color());
	painter.setBrush(Qt::NoBrush);
	return pix;
}

void Grille::drawSVG(QString filename)
{
  if (!module)return;
  if (module->getLastAction() == nullptr)return;
  QSvgGenerator generator;
  generator.setFileName(filename);
  int cw = drawGrilleCellSize.width();
	int ch = drawGrilleCellSize.height();
  QSize imgSize(cw*(renderMotif.size().X()+1), ch*(renderMotif.size().Y() + 1));
  generator.setSize(imgSize);
  generator.setViewBox(QRect(QPoint(0,0), imgSize));
  generator.setTitle(tr("Grille"));
  generator.setDescription(tr("SVG généré par LamaStudio"));
	QPainter painter;
  painter.begin(&generator);
  painter.fillRect(QRect(QPoint(0,0), imgSize), Qt::white);
	
	if (lastMotif != module->getLastAction()->ID())
		renderMotif = motifFactory->computeMotif(module->getLastAction()->ID());
	lastMotif = module->getLastAction()->ID();

	painter.setRenderHint(QPainter::Antialiasing, false);
	painter.save();

	painter.translate(QPoint(0.5*cw,0.5*ch));

	symbolDrawer.setSize(QSize(drawGrilleCellSize.width(), drawGrilleCellSize.height()));
	symbolDrawer.setPenWidth(2);

	int nx = renderMotif.size().X();
	int ny = renderMotif.size().Y();

	for (int i = 0; i < nx; i++)
	{
		for (int j = 0; j < ny; j++)
		{
      auto csty = styles[renderMotif(Dimensions(i, j))];
      symbolDrawer.setColor(QColor::fromRgb(csty.foregroundColor.r, csty.foregroundColor.g, csty.foregroundColor.b));
      symbolDrawer.setBackgroundColor(QColor::fromRgb(csty.backgroundColor.r, csty.backgroundColor.g, csty.backgroundColor.b));
      symbolDrawer.draw(painter, QPoint(i*drawGrilleCellSize.width(), j*drawGrilleCellSize.height()), static_cast<SymbolsDrawer::Symbol>(csty.symbol));
		}
	}

	paintMask(painter);

	QPen pen(Qt::black);
	pen.setWidth(0);
	painter.setPen(pen);
	for (int i = 0; i <= nx; i++)
	{
		painter.drawLine(i*cw, 0, i*cw, ny*ch);
	}
	for (int j = 0; j <= ny; j++)
	{
		painter.drawLine(0, j*ch, nx*cw, j*ch);
	}
	painter.restore();

	painter.setRenderHint(QPainter::Antialiasing, false);
	painter.setPen(palette().dark().color());
	painter.setBrush(Qt::NoBrush);
	painter.end();
}

void Grille::cursorEnterCell(QPoint cellid, bool isPressed)
{
}

void Grille::mousePressCell(QPoint cellid, bool leftButton)
{
}

void Grille::mouseReleaseCell(QPoint cellid)
{
}

Dimensions Grille::getCursorCell()
{
  return Dimensions(cursorCell.x(),cursorCell.y());
}

