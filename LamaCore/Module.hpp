#ifndef MODULE_H
#define MODULE_H

#include <vector>
#include <string>
#include <IO/json.hpp>

class Action;
class ActionMng;

class Module
{
public:
    Module(std::string name);
    Module(nlohmann::json& from, ActionMng* mgr);
    Module(ActionMng* mgr, std::string name, Action* pointsTo);
    nlohmann::json save();
    void rename(std::string newName);
    std::string getName();
    Action* getLastAction();
    void addAction(Action* action);
    bool undo();
    bool redo();
    bool canUndo();
    bool canRedo();
private:
    std::string name;
    std::vector<Action*> history;
    size_t historyUseId;
};

#endif // MODULE_H
