#include "LamaFile.hpp"
#include "json.hpp"
#include "Module.hpp"
#include "ActionMng.hpp"
#include "Factory.hpp"
#include <fstream>

using json = nlohmann::json;

LamaFile::LamaFile ( ActionMng* mgr, Factory* factory )
  : mgr ( mgr ),
  factory(factory)
{
}

std::vector<Module *> LamaFile::getModules()
{
  return modules;
}

void LamaFile::setModules ( std::vector<Module *> mods )
{
  modules = mods;
}

std::map<int, Style> LamaFile::getStyles()
{
  return styles;
}

void LamaFile::setStyles ( std::map<int, Style> stys )
{
  styles = stys;
}

bool LamaFile::save ( std::string filename )
{
  std::ofstream o ( filename );
  if ( !o ) return false;

  json j;
  j["mng"]=mgr->save();

  json jmods;
  size_t modid = 0;
  for ( auto mod:modules )
    jmods[modid++]=mod->save();
  j["modules"]=jmods;

  json jstyles;
  size_t styid=0;
  for ( auto sty:styles )
    {
      json js = sty.second;
      js["Id"]=sty.first;
      jstyles[styid++]=js;
    }
  j["styles"]=jstyles;

  o << std::setw ( 0 ) << j << std::endl;
  return true;
}

bool LamaFile::load ( std::string filename )
{
  std::ifstream i ( filename );
  if ( !i ) return false;
  mgr->clear();
  factory->clear();
  json j;
  i >> j;
  mgr->load ( j["mng"], factory );
  json jmods = j["modules"];
  modules.clear();
  for ( json jm:jmods )
    modules.push_back ( new Module ( jm,mgr ) );
  styles.clear();
  json jstyles=j["styles"];
  for ( json js: jstyles )
    styles[js["Id"]]=js;
  return true;
}

