#ifndef SCRIPTFILE_H
#define SCRIPTFILE_H

#include "../Style.hpp"
#include "../Actions/LuaNode.hpp"
#include "../Module.hpp"
#include "../Factory.hpp"

class ScriptFile
{
public:
  ScriptFile(ActionMng *mgr, Factory* factory);
  std::vector<Module*> getModules();
  std::map<int, Style> getStyles();
  bool load(std::string filename);
  
  void setModules(std::vector<Module*> mods);
  void setStyles(std::map<int, Style> stys);
  bool save(std::string filename);
private:
  ActionMng *mgr=nullptr;
  Factory* factory=nullptr;
  std::vector<Module*> modules;
  Palette styles;
};

#endif // SCRIPTFILE_H
