#include "ScriptFile.hpp"
#include <fstream>

ScriptFile::ScriptFile(ActionMng* mgr, Factory* factory) :
  mgr(mgr),
  factory(factory)
{
}

bool ScriptFile::load(std::string filename)
{
  mgr->clear();
  factory->clear();
  sol::state lua;
  loadStyleLuaClasses(lua);
  loadActionLuaClasses(lua,mgr,factory);
  lua.set_function("usePalette",[&](Palette p){styles=p;});
  lua.set_function("defineModule",[&](LuaNode node,std::string name)
  {
    Module* m = new Module(mgr,name,node.getAction());
    modules.push_back(m);
  });
  
  try{
    sol::protected_function_result res= lua.safe_script_file(filename);//safe_script(filename);
    return res.valid();
  }
  catch(...){return false;}
}

std::vector<Module *> ScriptFile::getModules()
{
  return modules;
}

std::map<int, Style> ScriptFile::getStyles()
{
  return styles.toMap();
}


void ScriptFile::setModules(std::vector<Module *> mods)
{
  modules=mods;
}

void ScriptFile::setStyles(std::map<int, Style> stys)
{
  styles=Palette(stys);
}

bool ScriptFile::save(std::string filename)
{
  std::ofstream os ( filename );
  if ( !os ) return false;
  os<<"-- Styles"<<std::endl;
  styles.writeLuaCode(os,"mainPal");
  os<<"usePalette(mainPal)"<<std::endl<<std::endl;
  
  os<<"-- Nodes"<<std::endl;
  std::stack<size_t> finals;
  for(auto m:modules)
    finals.push(m->getLastAction()->ID());
  mgr->writeLuaCode(os,finals);
  os<<std::endl;
  
  os<<"-- Modules"<<std::endl;
  for(auto m:modules)
  {
    os<<"defineModule("<<mgr->luaNodeNamer(m->getLastAction())<<",\""<<m->getName()<<"\")"<<std::endl;
  }
  return true;
}
