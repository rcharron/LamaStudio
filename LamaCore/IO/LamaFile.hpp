#ifndef LAMAFILE_H
#define LAMAFILE_H

#include <vector>
#include <string>
#include "Style.hpp"

class ActionMng;
class Module;
class Factory;

class LamaFile
{
public:
  LamaFile(ActionMng *mgr, Factory* factory);
  void setModules(std::vector<Module*> mods);
  std::vector<Module*> getModules();
  void setStyles(std::map<int, Style> stys);
  std::map<int, Style> getStyles();
  bool save(std::string filename);
  bool load(std::string filename);
private:
  ActionMng *mgr;
  Factory* factory;
  std::vector<Module*> modules;
  std::map<int, Style> styles;
};

#endif // LAMAFILE_H
