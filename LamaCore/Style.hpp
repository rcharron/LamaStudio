#ifndef STYLE_H
#define STYLE_H

#include "IO/json.hpp"
#include "sol/sol.hpp"
#include <iostream>
#include <map>
#include <string>

class Style
{
public:
  struct Color{
    Color(int r=0, int g=0, int b=0);
    int r;
    int g;
    int b;
    bool operator<(const Color& st) const;
    bool operator!=(const Color& st) const;
    bool operator==(const Color& st) const;
    bool isBlack();
    bool isWhite();
    void writeLuaCode(std::ostream& os);
  };
  Color backgroundColor;
  Color foregroundColor;
  int symbol;
  Style(Color backgroundColor, Color foregroundColor, int symbol);
  Style(Color backgroundColor, int symbol);
  Style(Color backgroundColor);
  Style();
  bool operator<(const Style& st) const;
  void writeLuaCode(std::ostream& os);
};

class Palette{
public:
  Palette();
  Palette(std::map<int, Style>);
  std::map<int, Style> toMap();
  Style& operator [](int id);
  void setStyle (int id, Style sty);
  Style getStyle(int id);
  void writeLuaCode(std::ostream& os, std::string paletteName);
private:
  std::map<int, Style> styles;
};

void loadStyleLuaClasses(sol::state& lua);

void to_json(nlohmann::json& j, const Style& style);
void from_json(const nlohmann::json& j, Style& style);

#endif // STYLE_H
