#ifndef MOTIF_HPP
#define MOTIF_HPP

#include <map>
#include "dimensions.hpp"


class Motif
{
public:
  void resizeMotif ( Dimensions newSize, bool expandRight, bool expandDown );
  int& operator() ( int i, int j );
  int& operator() ( Dimensions p );
  int& get ( int i, int j );
  int& get ( Dimensions p );
  Dimensions size();
//   QJsonObject save ( bool onlyVisible = true );
//   void load ( QJsonObject node );
  void clear();
protected:

  Dimensions grilleSize = Dimensions ( 1,1 );
  Dimensions grilleOffset;
  std::map<Dimensions, int> theGrille;
};


#endif // !MOTIF_HPP
