#ifndef DIMENSIONS_HPP
#define DIMENSIONS_HPP

#include <IO/json.hpp>

class Dimensions
{
public:
    Dimensions();
    Dimensions(int x, int y);
    void setX(int vx);
    void setY(int vy);
    int X() const;
    int Y() const;
    bool operator==(const Dimensions& other) const;
    bool operator!=(const Dimensions& other) const;
    bool operator<(const Dimensions& other) const;
    bool operator<=(const Dimensions& other) const;
    bool operator>(const Dimensions& other) const;
    bool operator>=(const Dimensions& other) const;
    Dimensions& operator+=(const Dimensions& other);
    Dimensions& operator-=(const Dimensions& other);
    Dimensions operator+(const Dimensions& other) const;
    Dimensions operator-(const Dimensions& other) const;
private:
    int x;
    int y;
};

void to_json(nlohmann::json& j, const Dimensions& d);
void from_json(const nlohmann::json& j, Dimensions& d);

#endif // DIMENSIONS_H
