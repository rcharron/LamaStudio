#include "IdFactory.hpp"
#include <iostream>

using namespace std;

IDSet::IDSet(IDSet* slower, size_t smax)
{
    smin=slower->smin;
    smid=slower->smax+1;
    this->smax=smax;
    childLeft=slower;
}


IDSet::IDSet(int singleton):
    smin(singleton),smax(singleton),smid(singleton),full(true)
{
}

IDSet::IDSet(size_t smin, size_t smax):
    smin(smin),smax(smax),smid((smax + smin + 1)/2)
{
}


IDSet::~IDSet()
{
    if(childRight)delete childRight;
    if(childLeft)delete childLeft;
}

bool IDSet::isFree()
{
    return !full;
}

size_t IDSet::getFree()
{
    if(full) return smax+1;
    if(smax==smin)
    {
        if(full) return smax+1;
        return smax;
    }
    if(childLeft==nullptr)return smin;
    size_t candidat = childLeft->getFree();
    if(candidat==smid)
    {
        if(childRight==nullptr)return candidat;
        return childRight->getFree();
    }
    return candidat;
}

size_t IDSet::getMaxValCapacity()
{
    return smax;
}



void IDSet::add(size_t element)
{
    if(element<smin)return;
    if(full)return;
    if(smin==smax)
    {
        if(smin==element)
            full=true;
    }
    else if(element<smid)
    {
        if(childLeft==nullptr)
        {
            childLeft=new IDSet(smin,smid-1);
            childLeft->add(element);
        }
        else
        {
            childLeft->add(element);
        }
                
    }
    else if(element<=smax)
    {
        if(childRight==nullptr)
        {
            childRight=new IDSet(smid,smax);
            childRight->add(element);
        }
        else
        {
            childRight->add(element);
        }
    }
    if((childLeft!=nullptr)&&(childRight!=nullptr))
    {
        if(childLeft->full&&childRight->full)
        {
            full = true;
            delete childLeft;
            delete childRight;
            childLeft = childRight = nullptr;
        }
    }
}

IDFactory::IDFactory()
{
    set=new IDSet(size_t(0),8);
}

IDFactory::~IDFactory()
{
    delete set;
}

void IDFactory::reset()
{
	delete set;
	set = new IDSet(size_t(0), 8);
}

size_t IDFactory::getFree()
{
    return set->getFree();
}

size_t IDFactory::getFreeAndAdd()
{
    size_t res=set->getFree();
    add(res);
    return res;
}

void IDFactory::add(size_t element)
{
    while(set->getMaxValCapacity()<element)
    {
        IDSet* newset = new IDSet(set,2*set->getMaxValCapacity());
        set=newset;
    }
    set->add(element);
}
