#include "Style.hpp"
#include <sstream>

Style::Color::Color ( int r, int g, int b ) :r ( r ),g ( g ),b ( b )
{
}

void Style::Color::writeLuaCode(std::ostream& os)
{
  os<<"Color.new("<<r<<", "<<g<<", "<<b<<')';
}

bool Style::Color::operator<(const Color & st) const
{
  if (r < st.r) return true;
  if (r > st.r) return false;
  if (g < st.g)return true;
  if (g > st.g)return false;
  return b < st.b;
}

bool Style::Color::operator!=(const Color & st) const
{
  return (r!=st.r)||(g!=st.g)||(b!=st.b);
}

bool Style::Color::operator==(const Color & st) const
{
  return (r==st.r)&&(g==st.g)&&(b==st.b);
}

bool Style::Color::isBlack()
{
  return r==0&&g==0&&b==0;
}

bool Style::Color::isWhite()
{
  return r==255&&g==255&&b==255;
}



Style::Style() :
  backgroundColor ( Style::Color ( 255,255,255 ) ),
  foregroundColor ( Style::Color ( 0,0,0 ) ),
  symbol ( 0 )
{
}

bool Style::operator<(const Style & st) const
{
  if (backgroundColor < st.backgroundColor) return true;
  if (backgroundColor != st.backgroundColor) return false;
  if (foregroundColor < st.foregroundColor) return true;
  if (foregroundColor != st.foregroundColor) return false;
  return symbol < st.symbol;
}

Style::Style ( Style::Color backgroundColor, Style::Color foregroundColor, int symbol ) :
  backgroundColor ( backgroundColor ),
  foregroundColor ( foregroundColor ),
  symbol ( symbol )
{
}

Style::Style(Style::Color backgroundColor, int symbol) : 
  backgroundColor(backgroundColor),
  foregroundColor(Style::Color(0,0,0)),
  symbol(symbol)
{
}

Style::Style(Style::Color backgroundColor) : 
  backgroundColor(backgroundColor),
  foregroundColor(Style::Color(0,0,0)),
  symbol(0)
{
}

void Style::writeLuaCode(std::ostream& os)
{
  int level=3;
  if(symbol==0){
    if(backgroundColor.isWhite())
      level=0;
    else if(foregroundColor.isBlack())
      level=1;
  }
  else
  {
    if(foregroundColor.isBlack())level=2;
  }
  os<<"Style.new(";
  if(level>0)backgroundColor.writeLuaCode(os);
  if(level>2){os<<", ";foregroundColor.writeLuaCode(os);}
  if(level>1)os<<", "<<symbol;
  os<<')';
}

Palette::Palette()
{
}

Palette::Palette(std::map<int, Style> data) :
  styles(data)
{
}

std::map<int, Style> Palette::toMap()
{
  return styles;
}

Style& Palette::operator[](int id)
{
  return styles[id];
}

void Palette::setStyle(int id, Style sty)
{
  styles[id]=sty;
}

Style Palette::getStyle(int id)
{
  return styles[id];
}

void Palette::writeLuaCode(std::ostream& os, std::string paletteName)
{
  os<<"local "<<paletteName<<" = Palette.new()"<<std::endl<<"do"<<std::endl;
  for(auto s:styles)
  {
    std::stringstream ss;
    ss<<"sty"<<s.first;
    std::string sn=ss.str();
    os<<"  local "<<sn<<" = ";
    s.second.writeLuaCode(os);
    os<<std::endl;
    os<<"  "<<paletteName<<":setStyle("<<s.first<<", "<<sn<<")"<<std::endl;
  }
  os<<"end"<<std::endl;
}

void loadStyleLuaClasses(sol::state& lua)
{
  lua.new_usertype<Style::Color>(
    "Color", sol::constructors<Style::Color(int,int,int)>(),
    "r", &Style::Color::r,
    "g", &Style::Color::g,
    "b", &Style::Color::b
  );
  lua.new_usertype<Style>(
    "Style", sol::constructors<Style(),Style(Style::Color),Style(Style::Color,int),Style(Style::Color,Style::Color,int)>(),
    "backgroundColor", &Style::backgroundColor,
    "foregroundColor", &Style::foregroundColor,
    "symbol", &Style::symbol
  );
  lua.new_usertype<Palette>(
    "Palette", sol::constructors<Palette()>(),
    "setStyle", &Palette::setStyle,
    "getStyle", &Palette::getStyle
  );
}

void to_json ( nlohmann::json& j, const Style& sty )
{
  j = nlohmann::json { {"br",sty.backgroundColor.r},
    {"fr",sty.foregroundColor.r},
    {"bg",sty.backgroundColor.g},
    {"fg",sty.foregroundColor.g},
    {"bb",sty.backgroundColor.b},
    {"fb",sty.foregroundColor.b},
    {"symbol",sty.symbol}
  };
}

void from_json ( const nlohmann::json& j, Style& sty )
{
  sty.backgroundColor.r = j.at ( "br" ).get<int>();
  sty.foregroundColor.r = j.at ( "fr" ).get<int>();
  sty.backgroundColor.g = j.at ( "bg" ).get<int>();
  sty.foregroundColor.g = j.at ( "fg" ).get<int>();
  sty.backgroundColor.b = j.at ( "bb" ).get<int>();
  sty.foregroundColor.b = j.at ( "fb" ).get<int>();
  sty.symbol = j.at ( "symbol" ).get<int>();
}
