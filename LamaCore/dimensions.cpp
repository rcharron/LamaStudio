#include "dimensions.hpp"

Dimensions::Dimensions() :x(0),y(0)
{}

Dimensions::Dimensions(int x, int y):x(x),y(y)
{}

void Dimensions::setX(int vx)
{
    x=vx;
}

void Dimensions::setY(int vy)
{
    y=vy;
}

int Dimensions::X() const
{
    return x;
}

int Dimensions::Y() const
{
    return y;
}

bool Dimensions::operator==(const Dimensions& other) const
{
    return (x==other.x)&&(y==other.y);
}

bool Dimensions::operator!=(const Dimensions& other) const
{
    return (x!=other.x)||(y!=other.y);
}

bool Dimensions::operator<(const Dimensions& other) const
{
    if(y<other.y)return true;
    if(y==other.y)return x<other.x;
    return false;
}

bool Dimensions::operator<=(const Dimensions& other) const
{
    if(y<other.y)return true;
    if(y==other.y)return x<=other.x;
    return false;
}

bool Dimensions::operator>(const Dimensions& other) const
{
    if(y>other.y)return true;
    if(y==other.y)return x>other.x;
    return false;
}

bool Dimensions::operator>=(const Dimensions& other) const
{
    if(y>other.y)return true;
    if(y==other.y)return x>=other.x;
    return false;
}

Dimensions Dimensions::operator+(const Dimensions& other) const
{
    return Dimensions(x+other.x,y+other.y);
}

Dimensions Dimensions::operator-(const Dimensions& other) const
{
    return Dimensions(x-other.x,y-other.y);
}

Dimensions & Dimensions::operator+=(const Dimensions& other)
{
    x+=other.x;
    y+=other.y;
    return *this;
}

Dimensions & Dimensions::operator-=(const Dimensions& other)
{
    x-=other.x;
    y-=other.y;
    return *this;
}

void to_json(nlohmann::json& j, const Dimensions& d)
{
    j = nlohmann::json { {"x",d.X()},
    {"y",d.Y()}
  };
}

void from_json(const nlohmann::json& j, Dimensions& d)
{
  d.setX(j.at ( "x" ).get<int>());
  d.setY(j.at ( "y" ).get<int>());
}
