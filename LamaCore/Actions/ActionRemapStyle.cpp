#include "ActionRemapStyle.hpp"
#include "dimensions.hpp"
#include "Motif.hpp"

void LuaActionRemapStyle::addMapping(int ori, int newStyle)
{
  remap[ori]=newStyle;
}

ActionRemapStyle::ActionRemapStyle(ActionMng* mng, Action* prev, LuaActionRemapStyle action) :
  Action(mng,prev),
  remap(action.remap)
{
}

ActionRemapStyle::ActionRemapStyle(ActionMng* mng, Action* prev, std::map<int, int> remap):
  Action(mng,prev),
  remap(remap)
{
}

ActionRemapStyle::ActionRemapStyle(ActionMng* mng, size_t id, Action* prev, nlohmann::json j):
  Action(mng,id,prev)
{
  nlohmann::json jm=j["remap"];
  for(nlohmann::json ja:jm)
    remap[ja[0].get<int>()]=ja[1].get<int>();
}

ActionType ActionRemapStyle::type()
{
  return ActionType::RemapStyle;
}

nlohmann::json ActionRemapStyle::save()
{
  nlohmann::json j;
  nlohmann::json jm;
  for(auto m:remap)
    jm.push_back(nlohmann::json::array({m.first, m.second}));
  j["remap"]=jm;
  return j;
}

void ActionRemapStyle::apply(Motif& motif)
{
  Dimensions sm = motif.size();
  for(int i=0;i<sm.X();i++)
    for(int j=0;j<sm.Y();j++)
      motif(i,j)=remap[motif(i,j)];
}

void ActionRemapStyle::writeLuaCode(std::ostream& os, std::string name)
{
  os<<name<<"=ActionRemapStyle.new()"<<std::endl;
  os<<"do"<<std::endl;
  for(auto k:remap)
    os<<"  "<<name<<":addMapping("<<k.first<<','<<k.second<<')'<<std::endl;
  os<<"end"<<std::endl;
}

void loadActionLuaRemapStyleClasses(sol::state& lua)
{
  lua.new_usertype<LuaActionRemapStyle>(
    "ActionRemapStyle", sol::constructors<LuaActionRemapStyle()>(),
    "addMapping", &LuaActionRemapStyle::addMapping
  );
}
