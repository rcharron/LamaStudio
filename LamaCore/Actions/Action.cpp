#include "Action.hpp"
#include "ActionMng.hpp"

Action::Action ( ActionMng* mng, Action* prev ) :
  mng ( mng ),
  prev ( prev ),
  id ( mng->getFreshID() )
{
  mng->Register ( this );
}

Action::Action ( ActionMng* mng, size_t id, Action* prev ) :
  mng ( mng ),
  prev ( prev ),
  id ( id )
{
  mng->Register ( this );
}

Action::~Action()
{
}


size_t Action::ID()
{
  return id;
}

Action * Action::Prev()
{
  return prev;
}

std::set<size_t> Action::depends()
{
  std::set<size_t> res;
  if(prev)res.insert(prev->id);
  return res;
}
