#ifndef ACTIONREMAPSTYLE_H
#define ACTIONREMAPSTYLE_H

#include "Action.hpp"
#include "ActionType.hpp"
#include <map>

class LuaActionRemapStyle
{
public:
  void addMapping(int ori, int newStyle);
  std::map<int,int> remap;
};

class ActionRemapStyle : public Action
{
public:
  ActionRemapStyle ( ActionMng* mng, Action* prev, LuaActionRemapStyle action );
  ActionRemapStyle ( ActionMng* mng, Action* prev, std::map<int,int> remap );
  ActionRemapStyle ( ActionMng* mng, size_t id, Action* prev, nlohmann::json j );
  virtual ActionType type();
  virtual nlohmann::json save();
  virtual void apply(Motif& );
  virtual void writeLuaCode(std::ostream& os, std::string name);
private:
  std::map<int,int> remap;
};

void loadActionLuaRemapStyleClasses(sol::state& lua);

#endif // ACTIONREMAPSTYLE_H
