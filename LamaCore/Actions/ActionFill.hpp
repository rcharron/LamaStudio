#ifndef ACTIONFILL_H
#define ACTIONFILL_H

#include "dimensions.hpp"
#include "Action.hpp"

class LuaActionFill
{
public:
  LuaActionFill(int oriX, int oriY, int style);
  int oriX;
  int oriY;
  int style;
};

class ActionFill : public Action
{
public:
  ActionFill(ActionMng* mng, Action* prev, LuaActionFill luaClass);
  ActionFill(ActionMng* mng, Action* prev, Dimensions from, int style);
  ActionFill(ActionMng* mng, size_t id, Action* prev, nlohmann::json j);
  virtual void apply(Motif& );
  virtual nlohmann::json save ();
  virtual ActionType type();
  virtual void writeLuaCode(std::ostream& os, std::string name);
private:
  Dimensions from;
  int style;
};

void loadActionLuaFillClasses(sol::state& lua);

#endif // ACTIONFILL_H
