#ifndef ACTION_H
#define ACTION_H

#include <set>
#include "stddef.h"
#include "IO/json.hpp"
#include "ActionType.hpp"
#include "../sol/sol.hpp"

class ActionMng;
class Motif;

class Action
{
public:
  Action ( ActionMng* mng, Action* prev );
  Action ( ActionMng* mng, size_t id, Action* prev );
  virtual ~Action();
  virtual void apply ( Motif& ) = 0;
  size_t ID();
  Action* Prev();
  virtual nlohmann::json save () = 0;
  virtual ActionType type() = 0;
  virtual void writeLuaCode(std::ostream& os, std::string name) = 0;
  virtual std::set<size_t> depends();
protected:
  ActionMng* mng=nullptr;
  Action* prev=nullptr;
  size_t id=-1;
};

#endif // ACTION_H
