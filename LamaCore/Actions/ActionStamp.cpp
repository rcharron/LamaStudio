#include "ActionStamp.hpp"
#include "Factory.hpp"
#include "ActionMng.hpp"
#include "LuaNode.hpp"

LuaActionStamp::LuaActionStamp(int ptX, int ptY, LuaNode* stampID) :
  ptX(ptX),
  ptY(ptY),
  stampID(stampID)
{
}

ActionStamp::ActionStamp(ActionMng* mng, Action* prev, Factory* factory, LuaActionStamp action) :
  Action(mng, prev),
  factory(factory),
  where(action.ptX, action.ptY),
  stampID(action.stampID->getAction()->ID())
{
}

ActionStamp::ActionStamp ( ActionMng *mng, Action *prev, Factory *factory, Dimensions where, size_t stampID ) :
  Action ( mng, prev ),
  factory ( factory ),
  where ( where ),
  stampID ( stampID )
{
}

ActionStamp::ActionStamp ( ActionMng *mng, size_t id, Action *prev, Factory *factory, nlohmann::json j ) :
  Action ( mng,id,prev ),
  factory ( factory )
{
  where=j["where"];
  stampID=j["stampID"];
}

std::set<size_t> ActionStamp::depends()
{
  auto res = Action::depends();
  res.insert(stampID);
  return res;
}

ActionType ActionStamp::type()
{
  return ActionType::Stamp;
}

void ActionStamp::writeLuaCode(std::ostream& os, std::string name)
{
  os<<name<<"=ActionStamp.new("<<where.X()<<','<<where.Y()<<','<<mng->luaNodeNamer(mng->get(stampID))<<')'<<std::endl;
}


nlohmann::json ActionStamp::save()
{
  nlohmann::json j;
  j["where"]=where;
  j["stampID"]=stampID;
  return j;
}

void ActionStamp::apply ( Motif &m )
{
  //Dimensions ms = m.size();
  Motif stampM=factory->computeMotif ( stampID );
  Dimensions ss = stampM.size();
  for ( int i=0; i<ss.X(); i++ )
    for ( int j=0; j<ss.Y(); j++ )
      if(stampM ( i,j )>0)
        m ( i+where.X(),j+where.Y() ) =stampM ( i,j );
}

void loadActionLuaStampClasses(sol::state& lua)
{
    lua.new_usertype<LuaActionStamp>(
    "ActionStamp", sol::constructors<LuaActionStamp(int,int,LuaNode*)>(),
    "ptX", &LuaActionStamp::ptX,
    "ptY", &LuaActionStamp::ptY,
    "stampID", &LuaActionStamp::stampID
  );
}
