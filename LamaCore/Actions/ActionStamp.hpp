#ifndef ACTIONSTAMP_H
#define ACTIONSTAMP_H

#include "Action.hpp"
#include "dimensions.hpp"

class Factory;
class LuaNode;

class LuaActionStamp
{
public:
  LuaActionStamp(int ptX, int ptY, LuaNode* stampID);
  int ptX;
  int ptY;
  LuaNode* stampID;
};

class ActionStamp : public Action
{
public:
  ActionStamp ( ActionMng *mng, Action *prev, Factory* factory, LuaActionStamp action);
  ActionStamp ( ActionMng *mng, Action *prev, Factory* factory, Dimensions where, size_t stampID);
  ActionStamp ( ActionMng *mng, size_t id, Action *prev, Factory* factory, nlohmann::json j );
  virtual ActionType type();
  virtual nlohmann::json save();
  virtual void apply ( Motif & );
  virtual void writeLuaCode(std::ostream& os, std::string name);
  virtual std::set<size_t> depends() override;
private:
  Factory* factory;
  Dimensions where;
  size_t stampID;
};

void loadActionLuaStampClasses(sol::state& lua);

#endif // ACTIONSTAMP_H
