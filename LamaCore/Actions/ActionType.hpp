#ifndef ACTION_TYPE_H
#define ACTION_TYPE_H

enum class ActionType
{
  Fill = 0,
  Pen = 1,
  Rect = 2,
  Resize = 3,
  Stamp = 4,
  RemapStyle = 5,
  IsoTransfo = 6
};

#endif
