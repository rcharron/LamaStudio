#include "ActionIsoTransform.hpp"
#include "Motif.hpp"

LuaActionIsoTransform::LuaActionIsoTransform(int type) :
    type(type)
{}

ActionIsoTransform::ActionIsoTransform(ActionMng *mng, Action *prev, LuaActionIsoTransform luaClass)
    : Action(mng, prev),
    transf(static_cast<Type>(luaClass.type))
{
}

ActionIsoTransform::ActionIsoTransform(ActionMng *mng, Action *prev, ActionIsoTransform::Type transf)
    : Action(mng, prev),
      transf(transf)
{
}

ActionIsoTransform::ActionIsoTransform(ActionMng *mng, size_t id, Action *prev, nlohmann::json j)
    : Action(mng, id, prev)
{
    transf=static_cast<Type>(static_cast<int>(j["type"]));
}

void ActionIsoTransform::apply(Motif & motif)
{
    Dimensions d = motif.size();
    int w=d.X();
    int h=d.Y();
    int wm=w/2;
    int hm=h/2;
    switch (transf) {
    case RotateLeft:
        {
        Motif cpyOr=motif;
        for(int i=0;i<w;i++)
            for(int j=0;j<h;j++)
                motif(j,w-1-i)=cpyOr(i,j);
        motif.resizeMotif(Dimensions(h,w),true,true);
        }
        break;
    case RotateRight:
        {
            Motif cpyOr=motif;
            for(int i=0;i<w;i++)
                for(int j=0;j<h;j++)
                    motif(h-1-j,i)=cpyOr(i,j);
            motif.resizeMotif(Dimensions(h,w),true,true);
        }
        break;
    case SymH:
        for(int i=0;i<w;i++)
            for(int j=0;j<hm;j++)
            {
            motif(i,j)+=motif(i,h-1-j);
            motif(i,h-1-j)=motif(i,j)-motif(i,h-1-j);
            motif(i,j)-=motif(i,h-1-j);
            }
        break;
    case SymV:
        for(int i=0;i<wm;i++)
            for(int j=0;j<h;j++)
            {
            motif(i,j)+=motif(w-1-i,j);
            motif(w-1-i,j)=motif(i,j)-motif(w-1-i,j);
            motif(i,j)-=motif(w-1-i,j);
            }
        break;
    }
}

nlohmann::json ActionIsoTransform::save()
{
    nlohmann::json j;
    j["type"] = static_cast<int>(transf);
    return j;
}

ActionType ActionIsoTransform::type()
{
    return  ActionType::IsoTransfo;
}

void ActionIsoTransform::writeLuaCode(std::ostream &os, std::string name)
{
    os<<name<<"=LuaActionIsoTransform.new("<<static_cast<int>(transf)<<')'<<std::endl;
}

void loadActionLuaIsoTransformClasses(sol::state &lua)
{
    lua.new_usertype<LuaActionIsoTransform>(
      "LuaActionIsoTransform", sol::constructors<LuaActionIsoTransform(int)>(),
      "type", &LuaActionIsoTransform::type
    );
}
