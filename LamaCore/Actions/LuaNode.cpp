#include "LuaNode.hpp"

LuaNode::LuaNode(ActionMng* mgr, Factory* factory) :
  mng(mgr),
  factory(factory)
{
}

LuaNode LuaNode::applyFill(LuaActionFill action)
{
  LuaNode res(mng, factory);
  res.actionObj = new ActionFill(mng,actionObj,action);
  res.nID = res.actionObj->ID();
  return res;
}

LuaNode LuaNode::applyPen(LuaActionPen action)
{
  LuaNode res(mng, factory);
  res.actionObj = new ActionPen(mng,actionObj,action);
  res.nID = res.actionObj->ID();
  return res;
}

LuaNode LuaNode::applyRect(LuaActionRect action)
{
  LuaNode res(mng, factory);
  res.actionObj = new ActionRect(mng,actionObj,action);
  res.nID = res.actionObj->ID();
  return res;
}

LuaNode LuaNode::applyRemapStyle(LuaActionRemapStyle action)
{
  LuaNode res(mng, factory);
  res.actionObj = new ActionRemapStyle(mng,actionObj,action);
  res.nID = res.actionObj->ID();
  return res;
}

LuaNode LuaNode::applyResize(LuaActionResize action)
{
  LuaNode res(mng, factory);
  res.actionObj = new ActionResize(mng,actionObj,action);
  res.nID = res.actionObj->ID();
  return res;
}

LuaNode LuaNode::applyStamp(LuaActionStamp action)
{
  LuaNode res(mng, factory);
  res.actionObj = new ActionStamp(mng,actionObj,factory,action);
  res.nID = res.actionObj->ID();
  return res;
}

LuaNode LuaNode::applyIsoTransform(LuaActionIsoTransform action)
{
    LuaNode res(mng, factory);
    res.actionObj = new ActionIsoTransform(mng,actionObj,action);
    res.nID = res.actionObj->ID();
    return res;
}

Action * LuaNode::getAction()
{
  return actionObj;
}

void loadActionLuaClasses(sol::state& lua, ActionMng* mgr, Factory* factory)
{
  loadActionLuaFillClasses(lua);
  loadActionLuaPenClasses(lua);
  loadActionLuaRectClasses(lua);
  loadActionLuaRemapStyleClasses(lua);
  loadActionLuaResizeClasses(lua);
  loadActionLuaStampClasses(lua);
  loadActionLuaIsoTransformClasses(lua);
  
  lua.new_usertype<LuaNode>(
    "noCtr",sol::constructors<>(),
    "apply",sol::overload(&LuaNode::applyFill ,
                          &LuaNode::applyPen,
                          &LuaNode::applyRect,
                          &LuaNode::applyRemapStyle,
                          &LuaNode::applyResize,
                          &LuaNode::applyStamp,
                          &LuaNode::applyIsoTransform));
  
  
  lua.set_function("CreateRootNode",[mgr,factory]()->LuaNode{ return LuaNode(mgr, factory);});
}
