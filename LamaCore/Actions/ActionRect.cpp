#include "ActionRect.hpp"
#include "ActionMng.hpp"
#include "Motif.hpp"

LuaActionRect::LuaActionRect(int oriX, int oriY, int sizeX, int sizeY, int style) :
  oriX(oriX),
  oriY(oriY),
  sizeX(sizeX),
  sizeY(sizeY),
  style(style)
{
}

ActionRect::ActionRect(ActionMng* mng, Action* prev, LuaActionRect action) : 
  Action(mng,prev),
  pos(action.oriX,action.oriY),
  size(action.sizeX,action.sizeY),
  style(action.style)
{
}

ActionRect::ActionRect(ActionMng* mng, size_t id, Action* prev, nlohmann::json j)
  : Action(mng,id,prev)
{
  style = j["style"];
  pos = j["pos"];
  size = j["size"];
}

ActionRect::ActionRect(ActionMng* mng, Action* prev, Dimensions pos, Dimensions size, int style)
  : Action(mng,prev),
  pos(pos),
  size(size),
  style(style)
{
}

void ActionRect::apply(Motif& m)
{
  for(int i=0;i<size.X();i++)
    for(int j=0;j<size.Y();j++)
      m(pos.X()+i,pos.Y()+j)=style;
}

ActionType ActionRect::type()
{
  return ActionType::Rect;
}

void ActionRect::writeLuaCode(std::ostream& os, std::string name)
{
  os<<name<<"=ActionRect.new("<<pos.X()<<','<<pos.Y()<<','<<size.X()<<','<<size.Y()<<','<<style<<')'<<std::endl;
}

nlohmann::json ActionRect::save()
{
  nlohmann::json j;
  j["style"] = style;
  j["pos"] = pos;
  j["size"] = size;
  return j;
}

void loadActionLuaRectClasses(sol::state& lua)
{
  lua.new_usertype<LuaActionRect>(
    "ActionRect", sol::constructors<LuaActionRect(int,int,int,int,int)>(),
    "oriX", &LuaActionRect::oriX,
    "oriY", &LuaActionRect::oriY,
    "sizeX", &LuaActionRect::sizeX,
    "sizeY", &LuaActionRect::sizeY,
    "style", &LuaActionRect::style
  );
}
