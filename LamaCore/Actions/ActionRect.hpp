#ifndef ACTIONRECT_H
#define ACTIONRECT_H

#include "Action.hpp"
#include "dimensions.hpp"

class LuaActionRect
{
public:
  LuaActionRect(int oriX, int oriY, int sizeX, int sizeY, int style);
  int oriX;
  int oriY;
  int sizeX;
  int sizeY;
  int style;
};

class ActionRect : public Action
{
public:
  ActionRect ( ActionMng* mng, Action* prev, LuaActionRect action);
  ActionRect ( ActionMng* mng, Action* prev, Dimensions pos, Dimensions size, int style );
  ActionRect ( ActionMng* mng, size_t id, Action* prev, nlohmann::json j );
  virtual void apply ( Motif& );
  virtual nlohmann::json save ();
  virtual ActionType type();
  virtual void writeLuaCode(std::ostream& os, std::string name);
private:
  Dimensions pos;
  Dimensions size;
  int style;
};

void loadActionLuaRectClasses(sol::state& lua);

#endif // ACTIONRECT_H
