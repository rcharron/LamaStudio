#include "ActionPen.hpp"
#include "ActionMng.hpp"
#include "Motif.hpp"

LuaActionPen::LuaActionPen(std::vector<int> path, int style) :
  path(path),
  style(style)
{
}

LuaActionPen::LuaActionPen(int style) :
  style(style)
{
}

LuaActionPen::LuaActionPen(int x, int y, int style) :
  style(style)
{
  path.push_back(x);
  path.push_back(y);
}

void LuaActionPen::addPath(int x, int y)
{
  path.push_back(x);
  path.push_back(y);
}

ActionPen::ActionPen(ActionMng* mng, Action* prev, LuaActionPen action) :
  Action(mng,prev),
  style(action.style)
{
  size_t t=action.path.size()/2;
  for(size_t i=0;i<t;i++)
    path.push_back(Dimensions(action.path[i*2],action.path[1+i*2]));
}

ActionPen::ActionPen(ActionMng* mng, size_t id, Action* prev, nlohmann::json j)
  : Action(mng,id,prev)
{
  style = j["style"];
  path.clear();
  if(!j["path"].is_array())return;
  for(Dimensions d:j["path"])
    path.push_back(d);
}

ActionPen::ActionPen(ActionMng* mng, Action* prev, std::vector<Dimensions> path, int style)
  : Action(mng,prev),
  path(path),
  style(style)
{
}

void ActionPen::apply(Motif& m)
{
  for(Dimensions cell: path)
    m(cell)=style;
}


ActionType ActionPen::type()
{
  return ActionType::Pen;
}

nlohmann::json ActionPen::save()
{
  nlohmann::json j;
  j["style"] = style;
  for(Dimensions d:path)
  {
    j["path"].push_back(d);
  }
  return j;
}

void ActionPen::writeLuaCode(std::ostream& os, std::string name)
{
  if(path.size()==1)
  {
    os<<name<<"=ActionPen.new("<<path[0].X()<<','<<path[0].Y()<<','<<style<<')'<<std::endl;
  }
  else
  {
    os<<name<<"=ActionPen.new("<<style<<')'<<std::endl;
    os<<"do"<<std::endl;
    for(auto s:path)
      os<<"  "<<name<<":addPath("<<s.X()<<','<<s.Y()<<')'<<std::endl;
    os<<"end"<<std::endl;
  }
}

void loadActionLuaPenClasses(sol::state& lua)
{
  lua.new_usertype<LuaActionPen>(
    "ActionPen", sol::constructors<LuaActionPen(std::vector<int>,int),LuaActionPen(int),LuaActionPen(int,int,int)>(),
    "addPath", &LuaActionPen::addPath,
    "style", &LuaActionPen::style
  );
}
