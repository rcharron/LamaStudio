#ifndef ACTIONISOTRANSFORM_HPP
#define ACTIONISOTRANSFORM_HPP

#include "Action.hpp"

class LuaActionIsoTransform
{
public:
  LuaActionIsoTransform(int type);
  int type;
};

class ActionIsoTransform : public Action
{
public:
    enum Type{RotateLeft = 0, RotateRight = 1, SymH = 2, SymV = 3};
    ActionIsoTransform(ActionMng* mng, Action* prev, LuaActionIsoTransform luaClass);
    ActionIsoTransform(ActionMng* mng, Action* prev, Type transf);
    ActionIsoTransform(ActionMng* mng, size_t id, Action* prev, nlohmann::json j);
    virtual void apply(Motif& );
    virtual nlohmann::json save ();
    virtual ActionType type();
    virtual void writeLuaCode(std::ostream& os, std::string name);
  private:
    Type transf;
};

void loadActionLuaIsoTransformClasses(sol::state& lua);

#endif // ACTIONISOTRANSFORM_HPP
