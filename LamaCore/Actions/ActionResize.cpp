#include "ActionResize.hpp"
#include "ActionMng.hpp"
#include "Motif.hpp"

LuaActionResize::LuaActionResize(int newSizeX, int newSizeY, bool expandRight, bool expandDown) :
  newSizeX(newSizeX),
  newSizeY(newSizeY),
  expandRight(expandRight),
  expandDown(expandDown)
{
}

LuaActionResize::LuaActionResize(int newSizeX, int newSizeY) :
  newSizeX(newSizeX),
  newSizeY(newSizeY)
{
}

ActionResize::ActionResize(ActionMng* mng, Action* prev, LuaActionResize action) :
  Action(mng,prev),
  newSize(action.newSizeX,action.newSizeY),
  expandRight(action.expandRight),
  expandDown(action.expandDown)
{
}

ActionResize::ActionResize(ActionMng* mng, size_t id, Action* prev, nlohmann::json j)
  : Action(mng,id,prev)
{
  expandRight = j["expandRight"];
  expandDown = j["expandDown"];
  newSize=(j["newSize"]);
}

ActionResize::ActionResize(ActionMng* mng, Action* prev, Dimensions newSize, bool expandRight, bool expandDown)
  : Action(mng,prev),
  newSize(newSize),
  expandRight(expandRight),
  expandDown(expandDown)
{
}

void ActionResize::apply(Motif& m)
{
  m.resizeMotif(newSize,expandRight,expandDown);
}

ActionType ActionResize::type()
{
  return ActionType::Resize;
}

void ActionResize::writeLuaCode(std::ostream& os, std::string name)
{
  os<<name<<"=ActionResize.new("<<newSize.X()<<','<<newSize.Y();
  if(!(expandRight&&expandDown))os<<','<<(expandRight?"true":"false")<<','<<(expandDown?"true":"false");
  os<<')'<<std::endl;
}

nlohmann::json ActionResize::save()
{
  nlohmann::json j;
  j["expandRight"] = expandRight;
  j["expandDown"] = expandDown;
  j["newSize"] = newSize;
  return j;
}

void loadActionLuaResizeClasses(sol::state& lua)
{
  lua.new_usertype<LuaActionResize>(
    "ActionResize", sol::constructors<LuaActionResize(int,int,bool,bool),LuaActionResize(int,int)>(),
    "sizeX", &LuaActionResize::newSizeX,
    "sizeY", &LuaActionResize::newSizeY,
    "expandRight", &LuaActionResize::expandRight,
    "expandDown", &LuaActionResize::expandDown
  );
}
