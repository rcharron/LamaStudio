#ifndef LUANODE_H
#define LUANODE_H

#include "../sol/sol.hpp"
#include "../ActionMng.hpp"
#include "ActionFill.hpp"
#include "ActionPen.hpp"
#include "ActionRect.hpp"
#include "ActionRemapStyle.hpp"
#include "ActionResize.hpp"
#include "ActionStamp.hpp"
#include "ActionIsoTransform.hpp"

class LuaNode
{
public:
  LuaNode(ActionMng* mgr, Factory* factory);
  LuaNode applyFill(LuaActionFill action);
  LuaNode applyPen(LuaActionPen action);
  LuaNode applyRect(LuaActionRect action);
  LuaNode applyRemapStyle(LuaActionRemapStyle action);
  LuaNode applyResize(LuaActionResize action);
  LuaNode applyStamp(LuaActionStamp action);
  LuaNode applyIsoTransform(LuaActionIsoTransform action);
  Action* getAction();
  
private:
  size_t nID;
  ActionMng* mng=nullptr;
  Factory* factory=nullptr;
  Action* actionObj=nullptr;//For the manager, and previous action
};

void loadActionLuaClasses(sol::state& lua, ActionMng* mgr, Factory* factory);

#endif // LUANODE_H
