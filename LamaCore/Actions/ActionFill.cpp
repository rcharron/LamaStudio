#include "ActionFill.hpp"
#include "Motif.hpp"
#include <stack>

ActionFill::ActionFill(ActionMng* mng, Action* prev, LuaActionFill luaClass)
  : Action(mng, prev),
  from(luaClass.oriX,luaClass.oriY),
  style(luaClass.style)
{
}


ActionFill::ActionFill(ActionMng* mng, Action* prev, Dimensions from, int style)
  : Action(mng, prev),
  from(from),
  style(style)
{
}

ActionFill::ActionFill(ActionMng* mng, size_t id, Action* prev, nlohmann::json j)
  : Action(mng, id, prev)
{
  style = j["style"];
  from.setX(j["fromX"]);
  from.setY(j["fromY"]);
}

void ActionFill::apply(Motif& motif)
{
  int srcCol=motif(from);
  if(srcCol==style)return;
  std::stack<Dimensions> stack;
  stack.push(from);
  while(!stack.empty())
  {
    Dimensions cell = stack.top();
    stack.pop();
    if(motif(cell)!=srcCol)continue;
    motif(cell)=style;
    Dimensions nei;
    if(cell.X()>0)
    {
      nei=Dimensions(cell.X()-1,cell.Y());
      stack.push(nei);
    }
    if(cell.X()+1<motif.size().X())
    {
      nei=Dimensions(cell.X()+1,cell.Y());
      stack.push(nei);
    }
    if(cell.Y()>0)
    {
      nei=Dimensions(cell.X(),cell.Y()-1);
      stack.push(nei);
    }
    if(cell.Y()+1<motif.size().Y())
    {
      nei=Dimensions(cell.X(),cell.Y()+1);
      stack.push(nei);
    }
  }
}

nlohmann::json ActionFill::save()
{
  nlohmann::json j;
  j["style"] = style;
  j["fromX"] = from.X();
  j["fromY"] = from.Y();
  return j;
}

ActionType ActionFill::type()
{
  return ActionType::Fill;
}

void ActionFill::writeLuaCode(std::ostream& os, std::string name)
{
  os<<name<<"=ActionFill.new("<<from.X()<<','<<from.Y()<<','<<style<<')'<<std::endl;
}


LuaActionFill::LuaActionFill(int oriX, int oriY, int style):
  oriX(oriX),
  oriY(oriY),
  style(style)
{
}

void loadActionLuaFillClasses(sol::state& lua)
{
  lua.new_usertype<LuaActionFill>(
    "ActionFill", sol::constructors<LuaActionFill(int,int,int)>(),
    "oriX", &LuaActionFill::oriX,
    "oriY", &LuaActionFill::oriY,
    "style", &LuaActionFill::style
  );
}
