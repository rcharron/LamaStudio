#ifndef ACTIONRESIZE_H
#define ACTIONRESIZE_H

#include "dimensions.hpp"
#include "Action.hpp"

class LuaActionResize
{
public:
  LuaActionResize(int newSizeX, int newSizeY, bool expandRight, bool expandDown);
  LuaActionResize(int newSizeX, int newSizeY);
  int newSizeX;
  int newSizeY;
  bool expandRight=true;
  bool expandDown=true;
};

class ActionResize : public Action
{
public:
  ActionResize ( ActionMng* mng, Action* prev, LuaActionResize action );
  ActionResize ( ActionMng* mng, Action* prev, Dimensions newSize, bool expandRight, bool expandDown );
  ActionResize ( ActionMng* mng, size_t id, Action* prev, nlohmann::json j );
  virtual void apply ( Motif& );
  virtual nlohmann::json save ();
  virtual ActionType type();
  virtual void writeLuaCode(std::ostream& os, std::string name);
private:
  Dimensions newSize;
  bool expandRight;
  bool expandDown;
};

void loadActionLuaResizeClasses(sol::state& lua);

#endif // ACTIONRESIZE_H
