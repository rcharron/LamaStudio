#ifndef ACTIONPEN_H
#define ACTIONPEN_H

#include "dimensions.hpp"
#include "Action.hpp"
#include <vector>

class LuaActionPen
{
public:
  LuaActionPen(int style);
  LuaActionPen(std::vector<int> path, int style);
  LuaActionPen(int x, int y, int style);
  void addPath(int x, int y);
  std::vector<int> path;
  int style;
};

class ActionPen : public Action
{
public:
  ActionPen ( ActionMng* mng, Action* prev, LuaActionPen action);
  ActionPen ( ActionMng* mng, Action* prev, std::vector<Dimensions> path, int style );
  ActionPen ( ActionMng* mng, size_t id, Action* prev, nlohmann::json j );
  virtual void apply ( Motif& );
  virtual nlohmann::json save ();
  virtual ActionType type();
  virtual void writeLuaCode(std::ostream& os, std::string name);
private:
  std::vector<Dimensions> path;
  int style;
};

void loadActionLuaPenClasses(sol::state& lua);

#endif // ACTIONPEN_H
