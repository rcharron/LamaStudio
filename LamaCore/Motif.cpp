#include "Motif.hpp"

void Motif::resizeMotif ( Dimensions newSize, bool expandRight, bool expandDown )
{
  if ( !expandDown )
    {
      int delta = newSize.Y() - grilleSize.Y();
      grilleOffset.setY ( grilleOffset.Y() - delta );
    }
  if ( !expandRight )
    {
      int delta = newSize.X() - grilleSize.X();
      grilleOffset.setX ( grilleOffset.X() - delta );
    }
  grilleSize = newSize;
}

int & Motif::operator() ( int i, int j )
{
  return theGrille[Dimensions ( i, j ) + grilleOffset];
}

int & Motif::operator() ( Dimensions p )
{
  return theGrille[p + grilleOffset];
}

int & Motif::get ( int i, int j )
{
  return theGrille[Dimensions ( i, j ) + grilleOffset];
}

int & Motif::get ( Dimensions p )
{
  return theGrille[p + grilleOffset];
}

Dimensions Motif::size()
{
  return grilleSize;
}

// QJsonObject Motif::save ( bool onlyVisible )
// {
//   QJsonObject res;
//   res["Grid"] = onlyVisible;
//   res["size"] = QJsonArray ( { grilleSize.width(), grilleSize.height() } );
//   if ( onlyVisible )
//     {
//       QJsonArray data;
//       for ( int i = 0; i < grilleSize.width(); i++ ) for ( int j = 0; j < grilleSize.height(); j++ ) data.push_back ( get ( i, j ) );
//       res["data"] = data;
//     }
//   else
//     {
//       res["offset"] = QJsonArray ( { grilleOffset.x(), grilleOffset.y() } );
//       QJsonArray data;
//       for ( auto elt : theGrille )
//         {
//           QJsonObject j;
//           j["i"] = elt.first.x();
//           j["j"] = elt.first.y();
//           j["val"] = elt.second;
//           data.push_back ( j );
//         }
//       res["data"] = data;
//     }
//   return res;
// }
// 
// void Motif::load ( QJsonObject node )
// {
//   bool onlyVisible = node["Grid"].toBool();
//   grilleSize.setWidth ( node["size"].toArray() [0].toInt() );
//   grilleSize.setHeight ( node["size"].toArray() [1].toInt() );
//   if ( onlyVisible )
//     {
//       grilleOffset = QPoint ( 0, 0 );
//       QJsonArray data = node["data"].toArray();
//       for ( int i = 0; i < grilleSize.width(); i++ )
//         for ( int j = 0; j < grilleSize.height(); j++ )
//           theGrille[QPoint ( i,j )] = data[i*grilleSize.height()+j].toInt();
//     }
//   else
//     {
//       theGrille.clear();
//       grilleOffset.setX ( node["offset"].toArray() [0].toInt() );
//       grilleOffset.setY ( node["offset"].toArray() [1].toInt() );
//       QJsonArray data = node["data"].toArray();
//       for ( auto elt : data )
//         {
//           QJsonObject j = elt.toObject();
//           QPoint p ( j["i"].toInt(), j["j"].toInt() );
//           theGrille[p] = j["val"].toInt();
//         }
//     }
// }

void Motif::clear()
{
  theGrille.clear();
}
