#include "Factory.hpp"
#include "ActionMng.hpp"
#include "Actions/Action.hpp"
#include <stack>


Factory::Factory(ActionMng* mng) :
  mng(mng)
{
}

Factory::~Factory()
{
  clear();
}

void Factory::clear()
{
  renders.clear();
}

Motif Factory::computeMotif(size_t id)
{
  if(renders.find(id)!=renders.end())
    return renders[id];
  int depth=0;
  std::stack<Action*> stack;
  stack.push(mng->get(id));
  Motif mwork;
  while(true)
  {
    depth++;
    Action* da = stack.top();
    if(da==nullptr)break;
    if(renders.find(id)!=renders.end())
    {
      mwork=renders[id];
      break;
    }
    
    stack.push(da->Prev());
  }
  if(stack.top()==nullptr)stack.pop();
  while(!stack.empty())
  {
    stack.top()->apply(mwork);
    stack.pop();
  }
  if(depth>=cacheDepth)
    renders[id]=mwork;
  return mwork;
}
