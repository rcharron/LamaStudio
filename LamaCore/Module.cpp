#include "Module.hpp"
#include "Actions/Action.hpp"
#include "ActionMng.hpp"
#include <stack>

Module::Module ( std::string name ) :
  name ( name ),
  historyUseId ( 0 )
{
}

Module::Module(nlohmann::json& from, ActionMng* mgr)
{
  using nlohmann::json;
  name = from["name"] ;
  if(!from["lastActionId"].is_null())
  {
    size_t lastActionId=from["lastActionId"];
    std::stack<Action*> act;
    act.push(mgr->get(lastActionId));
    while(true)
    {
      Action* prev = act.top()->Prev();
      if(prev)act.push(prev);
      else break;
    }
    while(!act.empty())
    {
      history.push_back(act.top());
      act.pop();
    }
  }
  historyUseId = from["historyUseId"];
}

Module::Module(ActionMng* mgr, std::string name, Action* pointsTo)
{
  this->name=name;
  if(pointsTo!=nullptr)
  {
    std::stack<Action*> act;
    act.push(pointsTo);
    while(true)
    {
      Action* prev = act.top()->Prev();
      if(prev)act.push(prev);
      else break;
    }
    historyUseId = act.size();
    while(!act.empty())
    {
      history.push_back(act.top());
      act.pop();
    }
  }
}


nlohmann::json Module::save()
{
  nlohmann::json j;
  j["name"] = name;
  if(!history.empty())j["lastActionId"] = history.back()->ID();
  j["historyUseId"] = historyUseId;
  return j;
}


void Module::rename ( std::string newName )
{
  name=newName;
}

std::string Module::getName()
{
  return name;
}

bool Module::undo()
{
  if ( historyUseId==0 ) return false;
  historyUseId--;
  return true;
}

bool Module::redo()
{
  if ( historyUseId==history.size() ) return false;
  historyUseId++;
  return true;
}

bool Module::canUndo()
{
  return historyUseId>0 ;
}

bool Module::canRedo()
{
  return historyUseId<history.size();
}

void Module::addAction ( Action* action )
{
  if ( historyUseId!=history.size() ) history.resize ( historyUseId );
  history.push_back ( action );
  historyUseId++;
}

Action * Module::getLastAction()
{
  if ( history.empty() ||historyUseId==0 ) return nullptr;
  return history[historyUseId-1];
}
