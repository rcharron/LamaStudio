#include "ActionMng.hpp"
#include "Actions/Action.hpp"
#include "Actions/ActionType.hpp"
#include "Actions/ActionResize.hpp"
#include "Actions/ActionPen.hpp"
#include "Actions/ActionRect.hpp"
#include "Actions/ActionFill.hpp"
#include "Actions/ActionStamp.hpp"
#include "Actions/ActionRemapStyle.hpp"
#include "Actions/ActionIsoTransform.hpp"
#include "Factory.hpp"

ActionMng::~ActionMng()
{
  for ( auto a:listActions )
    {
      if ( a.second!=nullptr )
        delete a.second;
    }
}

size_t ActionMng::getFreshID()
{
  return idFactory.getFreeAndAdd();
}

Action *ActionMng::get ( size_t id )
{
  return listActions[id];
}

void ActionMng::Register ( Action *act )
{
  if ( listActions[act->ID()]!=nullptr )
    throw "Already attributed";
  listActions[act->ID()]=act;
}

nlohmann::json ActionMng::save()
{
  nlohmann::json j;
  for ( auto ac:listActions )
    {
      auto jac = ac.second->save();
      jac["actionType"]=ac.second->type();
      if ( ac.second->Prev() !=nullptr ) jac["actionPrev"]=ac.second->Prev()->ID();
      j[ac.first]=jac;
    }
  return j;
}

std::string ActionMng::luaNodeNamer(Action* act)
{
  if(act==nullptr)return "tree[\"root\"]"; 
  std::stringstream ss;
  ss<<"tree["<<act->ID()<<']';
  return ss.str();
}

std::string ActionMng::luaActionNamer(Action* act)
{
  std::stringstream ss;
  ss<<"acts["<<act->ID()<<']';
  return ss.str();
}

void ActionMng::writeLuaCode(std::ostream& os)
{
  os<<"local tree={}"<<std::endl;
  os<<"local acts={}"<<std::endl;
  os<<luaNodeNamer(nullptr)<<"=CreateRootNode()"<<std::endl;
  
  for ( auto ac:listActions )
  {
    Action* act=ac.second;
    std::string an=luaActionNamer(act);
    act->writeLuaCode(os,an);
    os<<luaNodeNamer(act)<<"="<<luaNodeNamer(act->Prev())<<":apply("<<an<<')'<<std::endl;
  }
}

void ActionMng::writeLuaCode(std::ostream& os, std::stack<size_t> finals)
{
  os<<"local tree={}"<<std::endl;
  os<<"local acts={}"<<std::endl;
  os<<luaNodeNamer(nullptr)<<"=CreateRootNode()"<<std::endl;
  std::map<size_t,bool> treated;
  
  while(!finals.empty())
  {
    size_t head=finals.top();
    if(treated[head])
    {
      finals.pop();
      continue;
    }
    auto depends = get(head)->depends();
    bool newChilds=false;
    for(size_t c:depends)
    {
      if(!treated[c])
      {
        newChilds=true;
        finals.push(c);
      }
    }
    if(!newChilds)
    {
      finals.pop();
      treated[head]=true;
      Action* act=get(head);
      std::string an=luaActionNamer(act);
      act->writeLuaCode(os,an);
      os<<luaNodeNamer(act)<<"="<<luaNodeNamer(act->Prev())<<":apply("<<an<<')'<<std::endl;
    }
  }
}

void ActionMng::clear()
{
  for ( auto a:listActions )
    if ( a.second ) delete a.second;
  listActions.clear();
  idFactory.reset();
}


void ActionMng::load ( nlohmann::json j, Factory *factory )
{
  clear();
  size_t id=0;
  for ( auto jac:j )
    {
      ActionType at=jac["actionType"];
      Action *prev = nullptr;
      if ( !jac["actionPrev"].is_null() )
        prev = get ( jac["actionPrev"] );
      switch ( at )
        {
        case ActionType::Resize:
          new ActionResize ( this, id, prev, jac );
          break;
        case ActionType::Pen:
          new ActionPen ( this, id, prev, jac );
          break;
        case ActionType::Rect:
          new ActionRect ( this, id, prev, jac );
          break;
        case ActionType::Fill:
          new ActionFill ( this, id, prev, jac );
          break;
        case ActionType::Stamp:
          new ActionStamp ( this, id, prev, factory, jac );
          break;
        case ActionType::RemapStyle:
          new ActionRemapStyle( this, id, prev, jac );
          break;
      case ActionType::IsoTransfo:
          new ActionIsoTransform( this, id, prev, jac );
          break;
        default:
          throw "Not implemented";
        }
      idFactory.add ( id );
      id++;
    }
}

