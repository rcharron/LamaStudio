#ifndef FACTORY_H
#define FACTORY_H

#include <map>
#include "Motif.hpp"

class ActionMng;

class Factory
{
public:
  Factory(ActionMng* mng);
  ~Factory();
  void clear();
  Motif computeMotif(size_t id);
private:
  ActionMng* mng;
  std::map<size_t, Motif> renders;
  const int cacheDepth = 10;
};

#endif // FACTORY_H
