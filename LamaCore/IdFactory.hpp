#ifndef IDFACTORY_HPP
#define IDFACTORY_HPP
#include <stddef.h>

class IDSet
{
public:
    IDSet(IDSet* slower, size_t smax);
    IDSet(size_t smin, size_t smax);
    IDSet(int singleton);
    ~IDSet();
    bool isFree();
    void add(size_t element);
    size_t getFree();//max+1 if not available
    size_t getMaxValCapacity();
private:
    size_t smin = 0;
    size_t smax = 1;
    size_t smid;
    bool full = false;
    IDSet* childRight = nullptr;
    IDSet* childLeft = nullptr;
};

class IDFactory
{
public:
    IDFactory();
    ~IDFactory();
	void reset();
    void add(size_t element);
    size_t getFree();
    size_t getFreeAndAdd();
private:
    IDSet* set=nullptr;
};

#endif // IDFACTORY_H
