#ifndef ACTITONMNG_H
#define ACTITONMNG_H

#include "IdFactory.hpp"
#include "IO/json.hpp"
#include <map>
#include <iostream>
#include <stack>

class Action;
class Factory;

class ActionMng
{
public:
    ~ActionMng();
    void Register(Action* act); // An action is self registering
    Action* get(size_t id);
    size_t getFreshID();
    nlohmann::json save();
    void load(nlohmann::json j, Factory* factory);
    void writeLuaCode(std::ostream& os);
    void clear();
    std::string luaNodeNamer(Action* act);
    void writeLuaCode(std::ostream& os,std::stack<size_t> finals);
private:
  std::string luaActionNamer(Action* act);
    IDFactory idFactory;
    std::map<size_t, Action*> listActions;
};

#endif // ACTITONMNG_H
